package kr.co.hbilab.app;

import java.util.List;

//20150527
public interface Dao {
    public List<DeptDTO> selectAll();
    public DeptDTO selectOne(int no);
}
