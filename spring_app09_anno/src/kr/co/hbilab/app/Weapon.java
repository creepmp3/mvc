package kr.co.hbilab.app;

public interface Weapon{
    public void use();
    public void reuse();
    public void drop();
}
