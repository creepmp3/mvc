/**
 * 2015. 6. 8.
 * @author KDY
 */
package kr.co.hbilab.web;

import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class MyForm {
    private String name;
    private String gender;
    private String hiddenField;
    private String password;
    private String select;
    private List menuList;
    private String textarea;
    private String checkbox;
    private CommonsMultipartFile file;
    
    public MyForm() {
    }
    

    public MyForm(String name, String gender, String hiddenField, String password,
            String select, List menuList, String textarea, String checkbox,
            CommonsMultipartFile file) {
        super();
        this.name = name;
        this.gender = gender;
        this.hiddenField = hiddenField;
        this.password = password;
        this.select = select;
        this.menuList = menuList;
        this.textarea = textarea;
        this.checkbox = checkbox;
        this.file = file;
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHiddenField() {
        return hiddenField;
    }

    public void setHiddenField(String hiddenField) {
        this.hiddenField = hiddenField;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public List getMenuList() {
        return menuList;
    }

    public void setMenuList(List menuList) {
        this.menuList = menuList;
    }

    public String getTextarea() {
        return textarea;
    }

    public void setTextarea(String textarea) {
        this.textarea = textarea;
    }

    public String getCheckbox() {
        return checkbox;
    }

    public void setCheckbox(String checkbox) {
        this.checkbox = checkbox;
    }

    public CommonsMultipartFile getFile() {
        return file;
    }

    public void setFile(CommonsMultipartFile file) {
        this.file = file;
    }

    
    
}
