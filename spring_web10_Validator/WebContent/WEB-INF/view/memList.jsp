<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/view/header.jsp"></jsp:include>
    <table>
        <tr>
            <th>ID</th>
            <th>PW</th>
            <th>EMAIL</th>
        </tr>
	    <c:forEach items="${list }" var="dto">
	    <tr>
	        <td><c:out value="${dto.id }"/></td>
	        <td><c:out value="${dto.pw }"/></td>
	        <td><c:out value="${dto.email }"/></td>
	    </tr>
	    </c:forEach>
    </table>
</body>
</html>