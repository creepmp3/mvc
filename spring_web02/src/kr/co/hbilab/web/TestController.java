package kr.co.hbilab.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * 2015. 5. 28.
 * @author user
 * 
 */
public class TestController implements Controller{

    @Override
    public ModelAndView handleRequest(HttpServletRequest req, HttpServletResponse resp)
            throws Exception {
        ModelAndView model = new ModelAndView();
        model.addObject("msg", "대한민국 만세!!");
        model.setViewName("end");
        return model;
    }
    
}
