package project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

class JPanel04 extends JPanel {
	Vector<String> column = new Vector<String>();
	DefaultTableModel model;
	Vector<String> userRow;
	JTable jtable;
	JScrollPane jsp;
	
	
	public JPanel04() {
		
		setLayout(new BorderLayout());
		
		column.addElement("제목");
		column.addElement("게시일");
		column.addElement("작성자");
		
		model = new DefaultTableModel(column,0);
		jtable = new JTable(model);
		jsp = new JScrollPane(jtable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		jtable.getColumn("제목").setPreferredWidth(200);
		userRow = new Vector<String>();
		userRow.addElement("공지");
		userRow.addElement("2015-03-24");
		userRow.addElement("작성자");
		model.addRow(userRow);
		
		add(jsp);
		
		
		
	}
}
