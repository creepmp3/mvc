package project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Date;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

class JPanel03 extends JPanel implements ActionListener {
	
	JPanel jpjp;
	
	JButton jbtn77, jbtn88, jbtn99;
	JLabel view_lb = new JLabel();
	JLabel title_lb = new JLabel("파일 검색", JLabel.CENTER);
	JLabel result_lb = new JLabel("RESUTLS");
	JTable jtable, view_jt;
	
	Container con;
	
	Vector field_vc = new Vector();
	Vector data_vc = new Vector();
	Vector condition_vc = new Vector();
	JComboBox condition_jcb = new JComboBox(condition_vc);
	JScrollPane view_jsp;
	JScrollPane condition_jsp = new JScrollPane(condition_jcb);
	JButton search_bt = new JButton("검색");
	JTextField search_tf = new JTextField(15);
	JButton clear_bt = new JButton("CLEAR");
	JButton end_bt = new JButton("END");
	
	public JPanel03() {
		/*
		 * ****************** 자료실 S *********************
		 * 
		 * String[][] data = { { "1_1", "1_2", "1_3", "1_4", "1_5" } , { "2_1",
		 * "2_2", "2_3", "2_4", "2_5" } , { "3_1", "3_2", "3_3", "3_4", "3_5" }
		 * , { "4_1", "4_2", "4_3", "4_4", "4_5" } }; String[] data_col = {
		 * "1번", "2번", "3번", "4번", "5번" }; jtable = new JTable(data, data_col);
		 * 
		 * jtable.setBounds(0, 100, 332, 435);
		 * 
		 * add(jtable);
		 * 
		 * 
		 * ****************** 자료실 E *********************
		 */
		init();
		start();
		setSize(300, 300);
		;
		setVisible(false);
		
	}
	
	public void init() {
		setLayout(new BorderLayout(10, 10));
		JPanel jp = new JPanel(new BorderLayout());
		title_lb.setFont(new Font("Sans-Serif", Font.BOLD, 14));
		jp.add("North", title_lb);
		JPanel jp1 = new JPanel(new GridLayout(3, 1));
		jp1.setBorder(new BevelBorder(BevelBorder.RAISED));
		jp1.setBackground(Color.white);
		JPanel jp2 = new JPanel(new BorderLayout());
		jp2.setBorder(new TitledBorder("파일검색"));
		JPanel jp3 = new JPanel(new GridLayout(2, 1));
		search_tf.setBorder(new TitledBorder("검색조건"));
		jp3.add(search_tf);
		condition_jsp.setBorder(new TitledBorder("검색범위"));
		jp3.add(condition_jsp);
		jp2.add("Center", jp3);
		JPanel jp4 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		jp4.add(search_bt);
		jp2.add("South", jp4);
		jp1.add(jp2);
		jp1.add(new JLabel());
		jp1.add(new JLabel());
		jp.add("Center", jp1);
		jp.setPreferredSize(new Dimension(100, 600));
		add("West", jp);
		
		jpjp = new JPanel(new BorderLayout(10, 10));
		JPanel jpjp1 = new JPanel(new BorderLayout(5, 5));
		result_lb.setFont(new Font("Sans-Serif", Font.BOLD, 14));
		jpjp1.add("West", result_lb);
		view_lb.setBorder(new BevelBorder(BevelBorder.LOWERED));
		jpjp1.setPreferredSize(new Dimension(550, 70));
		JPanel jpjpjp = new JPanel(new BorderLayout());
		jpjpjp.setBackground(Color.white);
		jpjpjp.add("Center", view_lb);
		jpjp1.add("Center", jpjpjp);
		field_vc.add("파일명");
		field_vc.add("파일경로");
		field_vc.add("파일크기");
		// field_vc.add("최종수정일");
		// field_vc.add("파일종류");
		view_jt = new JTable(data_vc, field_vc);
		view_jt.setFont(new Font("Consolas", Font.PLAIN, 9));
		view_jsp = new JScrollPane(view_jt);
		jpjp.add("Center", view_jsp);
		JPanel jpjp2 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		jpjp2.add(clear_bt);
		jpjp2.add(end_bt);
		jpjp.add("South", jpjp2);
		add("Center", jpjp);
		
		File[] root = File.listRoots();
		condition_vc.add("전체범위");
		for (int i = 0; i < root.length; i++) {
			condition_vc.add(root[i].toString());
		}
		condition_jcb.updateUI();
	}
	
	public void start() {
		search_bt.addActionListener(this);
		search_tf.addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		
		String data = search_tf.getText();
		if (obj == search_tf || obj == search_bt) {
			if (data == null || data.trim().length() == 0)
				return;
			
			data = data.trim();
			
			int condition = 0;
			
			char ch = data.charAt(0);
			if (ch == '*')
				condition = 1;
			
			char ch1 = data.charAt(data.length() - 1);
			if (ch1 == '*')
				condition = 2;
			if (ch == '*' && ch1 == '*')
				condition = 3;
			
			String str = (String) condition_jcb.getSelectedItem();
			if (str.trim().equals("전체범위")) {
				File[] root = File.listRoots();
				for (int i = 1; i < root.length; i++) {
					new MyThread(root[i].toString(), data, condition);
				}
			} else {
				new MyThread(str.trim(), data, condition);
			}
		} else if (obj == clear_bt) {
			data_vc.clear();
			jpjp.remove(view_jsp);
			view_jt = new JTable(data_vc, field_vc);
			view_jsp = new JScrollPane(view_jt);
			jpjp.add("Center", view_jsp);
			jpjp.validate();
		} else if (obj == end_bt) {
			System.exit(0);
		}
	}
	
	private void updateTable(Vector imsi) {
		data_vc.add(imsi);
		jpjp.remove(view_jsp);
		;
		view_jt = new JTable(data_vc, field_vc);
		view_jsp = new JScrollPane(view_jt);
		jpjp.add("Center", view_jsp);
		jpjp.validate();
	}
	
	class MyThread1 extends Thread {
		private String path;
		
		public MyThread1(String path) {
			this.path = path;
			this.start();
		}
		
		public void run() {
			view_lb.setText(path);
		}
	}
	
	class MyThread extends Thread {
		private String path, data;
		private int condition;
		
		public MyThread(String path, String data, int contion) {
			this.path = path;
			this.data = data;
			this.condition = condition;
			this.start();
		}
		
		public void run() {
			this.searchDirectory();
		}
		
		private void searchDirectory() {
			File[] files = new File(path).listFiles();
			
			if (files == null)
				return;
			for (int i = 0; i < files.length; i++) {
				new MyThread1(files[i].getPath());
				if (condition == 0) {
					if (files[i].getName().equals(data)) {
						Vector imsi = new Vector();
						imsi.add(files[i].getName());
						imsi.add(files[i].getParent());
						imsi.add(String.valueOf(files[i].length()));
						imsi.add(new Date(files[i].lastModified()).toString());
						imsi.add(files[i].isDirectory() ? "폴더" : "파일");
						updateTable(imsi);
					}
				} else if (condition == 1) {
					if (files[i].getName().endsWith(data.substring(1))) {
						Vector imsi = new Vector();
						imsi.add(files[i].getName());
						imsi.add(files[i].getParent());
						imsi.add(String.valueOf(files[i].length()));
						imsi.add(new Date(files[i].lastModified()).toString());
						imsi.add(files[i].isDirectory() ? "폴더" : "파일");
						updateTable(imsi);
					}
				} else if (condition == 2) {
					if (files[i].getName().endsWith(data.substring(1))) {
						Vector imsi = new Vector();
						imsi.add(files[i].getName());
						imsi.add(files[i].getParent());
						imsi.add(String.valueOf(files[i].length()));
						imsi.add(new Date(files[i].lastModified()).toString());
						imsi.add(files[i].isDirectory() ? "폴더" : "파일");
						updateTable(imsi);
					}
				} else if (condition == 3) {
					if (files[i].getName().indexOf(data.substring(1, data.trim().length() - 1)) != -1) {
						Vector imsi = new Vector();
						imsi.add(files[i].getName());
						imsi.add(files[i].getParent());
						imsi.add(String.valueOf(files[i].length()));
						imsi.add(new Date(files[i].lastModified()).toString());
						imsi.add(files[i].isDirectory() ? "폴더" : "파일");
						updateTable(imsi);
					}
				}
			}
			
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					new MyThread(path + File.separator + files[i].getName(), data, condition);
				}
			}
			
		}
	}
}
