package project;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GoodzzyChatClient extends JFrame implements ActionListener, Runnable, KeyListener {
	Socket s;
	BufferedReader br;
	PrintWriter pw;
	String ip;
	
	JScrollPane jsp;
	JTextArea jta;
	JLabel jlbIp;
	JTextField jtfIp, jtfTxt;
	JButton jbtnLogin, jbtnSend;
	
	static String user_namev = "";
	String my_name = "";
	
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@192.168.0.95:1521:orcl";
	String user = "scott";
	String password = "tiger";
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	
	public GoodzzyChatClient(String title, String user_namev) {
		super(title);
		this.user_namev = user_namev;
		setLayout(null);
		setBounds(50, 50, 400, 600);
		
		jta = new JTextArea();
		jsp = new JScrollPane(jta, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jtfTxt = new JTextField();
		jbtnSend = new JButton("전송");
		
		jsp.setBounds(20, 20, 355, 400);
		jtfTxt.setBounds(20, 450, 295, 50);
		jbtnSend.setBounds(315, 450, 60, 50);
		
		add(jtfTxt);
		add(jbtnSend);
		add(jsp);
		
		jbtnSend.addActionListener(this);
		jtfTxt.addKeyListener(this);
		
		try {
			
			ip = InetAddress.getLocalHost().getHostAddress();
			System.out.println(ip);
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, password);
			StringBuffer sb = new StringBuffer();
			
			sb.append("\n SELECT name_v FROM dept_user WHERE ip_v = ? ");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, ip);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				my_name = rs.getString("name_v");
				rs.close();
			}
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} finally {
			try {
				if (rs != null) rs.close();
				if (pstmt != null) pstmt.close();
				if (conn != null) conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		Thread th = new Thread(this);
		th.start();
		
		// 창 종료
		//setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setVisible(true);
		jtfTxt.requestFocus();
	}
	
	public static void main(String[] args) {
		new GoodzzyChatClient("대화중", user_namev);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		String data = jtfTxt.getText();
		
		pw.println(data);
		pw.flush();
		
		jtfTxt.setText("");
		jtfTxt.requestFocus();
		
	}
	
	@Override
	public void run() {
		try {
			String ip = "192.168.0.95";
			s = new Socket(ip, 4000);
			
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s.getOutputStream())));
			
			String msg = null;
			while (true) {
				msg = br.readLine();
				String sender = msg.substring(0, msg.indexOf("]")-1);
				String nomal_msg = "";
				String w_msg = "";
				
				if (msg.indexOf("/c") > -1) {
					jta.setText("");
				} else {
					jta.append(msg + "\n");
				}
				JScrollBar jsb = jsp.getVerticalScrollBar();
				int max = jsb.getMaximum();
				jsb.setValue(max);
				
			}
		} catch (UnknownHostException e) {
			System.out.println("알 수 없는 호스트");
		} catch (IOException e) {
			System.out.println("접속 실패 다시 확인하세요");
		}
		
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		
		if (code == 10) {
			String data = jtfTxt.getText();
			
			pw.println(data);
			pw.flush();
			
			jtfTxt.setText("");
			jtfTxt.requestFocus();
		}
		
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
