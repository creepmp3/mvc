package project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ChatServer extends JFrame implements ActionListener{
	ArrayList<GMServer> list = new ArrayList<GMServer>();
	ServerSocket ss;
	BufferedReader br;
	PrintWriter pw;
	JTextArea jta;
	JButton jbtn;
	JPanel jp1, jp2;
	JScrollPane jsp;
	
	public ChatServer(String title){
		super(title);
		setBounds(30, 30, 400, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	
		
		jta = new JTextArea(25, 30);
		jbtn = new JButton("EXIT");
		jp1 = new JPanel();
		jp2 = new JPanel();
		jsp = new JScrollPane(jta, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		add(jp1, "Center");
		add(jp2, "South");
		//jp1.add(jta);
		jp2.add(jbtn);
		jp1.add(jsp);
		
		
		jbtn.addActionListener(this);
		setVisible(true);
		vchatting();
		
	}
	private void vchatting(){
		// 채팅시작
		try {
			ss = new ServerSocket(4000);
			while(true){
				Socket client = ss.accept();
				GMServer gm = new GMServer(client);
				list.add(gm);
				gm.start();
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		ChatServer cs = new ChatServer("채팅서버 ver0.01");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("종료합니다");
		System.exit(0);
		
	}// 액션끝
	// innerClass
	public class GMServer extends Thread{
		Socket client ;
		PrintWriter pw;
		BufferedReader br;
		String ip;
		
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@192.168.0.95:1521:orcl"; 
		String user = "scott";
		String password = "tiger";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String name;

		GMServer(Socket client){
			this.client = client; 

			try {
				ip = client.getInetAddress().getHostAddress();
				System.out.println(ip + " : 서버에 접속됨");
				br = new BufferedReader(new InputStreamReader(client.getInputStream()));
				pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(client.getOutputStream())));
			
	            Class.forName(driver);
	            conn = DriverManager.getConnection(url, user, password);
	            StringBuffer sb = new StringBuffer();
	            sb.append("SELECT NAME_V ");
	            sb.append("FROM DEPT_USER ");
	            sb.append("WHERE IP_V = ? ");
	            pstmt = conn.prepareStatement(sb.toString());
	            pstmt.setString(1, ip);
	            rs = pstmt.executeQuery();
	            
	            
	            while (rs.next()) {

	               name = rs.getString("NAME_V");
	               
	               jta.append("SYSTEM : [ "+name+" ] 님이 대화에 참여하였습니다\n");
	               pw.println("SYSTEM : [ "+name+" ] 님이 대화에 참여하였습니다");
	               broadcast("SYSTEM : [ "+name+" ] 님이 대화에 참여하였습니다");
	               pw.flush();
	            }
	            if(ip== null) System.out.println(name+"=null");
	         } catch (SQLException e1) {
	            e1.printStackTrace();
	         } catch (ClassNotFoundException e1) {
	            e1.printStackTrace();
	         }catch(IOException e){
	        	 System.out.println("IO");
	         }
				
		}
		
		@Override
		public void run() {
			
			try {
				while(true){
					String msg = null;
					msg = br.readLine();
					jta.append("[ "+name+" ] : "+msg+"\n");
					broadcast("[ "+name+" ] :"+msg);
					JScrollBar jsb = jsp.getVerticalScrollBar();
					int max = jsb.getMaximum();
					jsb.setValue(max);
				}
			} catch (IOException e) {
				jta.append("SYSTEM : [ "+name+" ] 님이 퇴장하셨습니다\n");
				pw.println("SYSTEM : [ "+name+" ] 님이 퇴장하셨습니다\n");
				broadcast("SYSTEM : [ "+name+" ] 님이 퇴장하셨습니다\n");
				pw.flush();
				list.remove(this);
			}
				
		}
		private void broadcast(String string) {
				for (GMServer s : list){
					s.pw.println(string);
					s.pw.flush();
				}

		}
	}
	
}
