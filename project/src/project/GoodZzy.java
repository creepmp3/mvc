package project;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.metal.MetalTabbedPaneUI;

/*
 CREATE TABLE DEPT_USER (
 USER_NO_N    NUMBER       PRIMARY KEY NOT NULL
 , NAME_V       VARCHAR2(20) NOT NULL
 , PASSWORD_V   VARCHAR2(20) NOT NULL
 , BIRTHDAY_V   VARCHAR2(10) 
 , IP_V         VARCHAR2(15) 
 , DEPT_NO_N    NUMBER      
 , DEPT_NAME_V  VARCHAR2(20)      
 , POSITION_V   VARCHAR2(10) 
 , HIREDATE_V   VARCHAR2(10) 
 , STATUS       VARCHAR2(20) 
 , STATUS_MSG   VARCHAR2(128)
 , REG_D        DATE         NOT NULL
 )

 CREATE SEQUENCE DEPT_USER_SEQ
 START WITH 1
 INCREMENT BY 1
 NOMAXVALUE
 NOMINVALUE

 CREATE TABLE DEPT_NAME (
 DEPT_NO_N    NUMBER       PRIMARY KEY NOT NULL
 , DEPT_NAME_V  VARCHAR2(50) NOT NULL
 , DEPT_LOC     VARCHAR2(10) 
 , REG_D        DATE         NOT NULL
 )

 CREATE SEQUENCE DEPT_NAME_SEQ
 START WITH 1
 INCREMENT BY 1
 NOMAXVALUE
 NOMINVALUE 

 CREATE TABLE DEPT_COMMUTE (
 COMMUTE_NO_N         NUMBER         PRIMARY KEY NOT NULL
 , USER_NO_N            NUMBER         NOT NULL
 , WORK_IN_D            DATE     
 , WORK_OUT_D           DATE  
 , MSG_V                VARCHAR2(128)   
 , REG_D                DATE           NOT NULL
 )

 CREATE SEQUENCE DEPT_COMMUTE_SEQ
 START WITH 1
 INCREMENT BY 1
 NOMAXVALUE
 NOMINVALUE


CREATE TABLE USER_MSG2(
   MSG_NO_N      NUMBER   PRIMARY KEY NOT NULL
 , SENDER        VARCHAR2(20)
 , RECEIVER      VARCHAR2(20)
 , IP_V          VARCHAR2(15)
 , READ_CHK_C    CHAR(1)
 , READ_REG_D    DATE
 , REG_D         DATE
 )
 
 
CREATE SEQUENCE USER_MSG2_SEQ
START WITH 1
INCREMENT BY 1
NOMAXVALUE
NOMINVALUE
 
 */

public class GoodZzy extends JFrame implements ActionListener, MouseListener, Runnable {
	JPanel jp1, jp2, jp3;
	JPanel cl1, cl2, cl3, jp_main, jp_msg, jp_file, jp_notice;
	CardLayout layout;
	
	JButton jbtn1, jbtn2, jbtnNotice;
	
	// 상단 메뉴
	JMenuBar jmb;
	JMenu jm;
	JMenuItem jmi1, jmi2, jmiLogout;
	JMenuItem jpm1, jpm2, jpm3;
	
	JList jl;
	JScrollPane jsp;
	
	// 로그인
	JLabel jlbLogin_name, jlbLogin_pw, jlb_bg;
	JTextField jtfLogin_name;
	JButton jbtnLogin, jbtnJoin;
	JPasswordField jtfLogin_pw;
	
	// 회원가입
	JLabel jlbJoin_name, jlbJoin_pw, jlbJoin_birthday, jlbJoin_deptname, jlbJoin_hiredate, jlbJoin_position, jlb_gender;
	JTextField jtfJoin_name, jtfJoin_birthday, jtfJoin_deptname, jtfJoin_hiredate, jtfJoin_position;
	JPasswordField jtfJoin_pw;
	JComboBox jcb_deptname;
	JButton jbtnJoin_ok, jbtnJoin_cancel;
	JRadioButton jr_gender_m, jr_gender_f;
	
	JButton jbtnWorkIn, jbtnWorkOut;
	
	// 부서목록
	JRootPane jrp;
	Container con;
	JTree jtree;
	
	JTextArea jtaSend, jta2, jta3, jtaTop;
	JScrollPane jsp2;
	JTextField jtf, jtf2;
	JPanel jpPop, jpPop2;
	JPopupMenu jpop_menu;
	
	JTable jtable;
	
	JTabbedPane jtab;
	
	Image imgOn, imgOff;
	Image logo_img;
	ImageIcon bg_img;
	
	Object jlValue;
	
	BufferedImage img;
	
	Socket s;
	BufferedReader br;
	PrintWriter pw;
	
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@192.168.0.95:1521:orcl";
	String user = "scott";
	String password = "tiger";
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	
	int userNo = 0;
	String ipNo = "";
	int deptNo = 0;
	
	
	public JPanel01 jpanel01 = null;
	public JPanel02 jpanel02 = null;
	public JPanel03 jpanel03 = null;
	public JPanel04 jpanel04 = null;
	
	public GoodZzy(String title) {
		setTitle(title);
		setBounds(30, 30, 350, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		layout = new CardLayout();
		setLayout(layout);
		
		/* ******************* 상단메뉴바 S ****************** */
		
		jp1 = new JPanel();
		jmi1 = new JMenuItem("FILE");
		jmiLogout = new JMenuItem("로그아웃");
		jmi2 = new JMenuItem("종료");
		jm = new JMenu("FILE");
		jmb = new JMenuBar();
		
		jbtn1 = new JButton("목록1");
		jbtn2 = new JButton("목록2");
		
		jmi2.addActionListener(this);
		jmiLogout.addActionListener(this);
		
		jm.add(jmi1);
		jm.addSeparator();
		jm.add(jmiLogout);
		jm.add(jmi2);
		jmb.add(jm);
		
		jp1.add(jmb);
		
		/* ******************* 상단메뉴바 E ****************** */
		
		/* ******************* LoginPanel S ****************** */
		
		cl1 = new JPanel();
		
		Color c1 = new Color(255, 170, 0);
		cl1.setLayout(null);
		cl1.setBackground(c1);
		
		logo_img = getToolkit().getImage("src/project/images/logo1.png");
		bg_img = new ImageIcon("src/project/login_bg.jpg");
		jlb_bg = new JLabel(bg_img);
		jlbLogin_name = new JLabel("이　름");
		jlbLogin_pw = new JLabel("암　호");
		jtfLogin_name = new JTextField(30);
		jtfLogin_pw = new JPasswordField(30);
		jbtnLogin = new JButton("로그인");
		jbtnJoin = new JButton("회원가입");
		
		jlbLogin_name.setFont(new Font("돋움", Font.LAYOUT_RIGHT_TO_LEFT, 14));
		jlbLogin_name.setForeground(new Color(202,224,188));
		jlbLogin_pw.setForeground(new Color(202,224,188));
		jlbLogin_pw.setFont(new Font("돋움", Font.LAYOUT_RIGHT_TO_LEFT, 14));
		jlbLogin_name.setBounds(60, 320, 60, 50);
		jtfLogin_name.setBounds(150, 338, 140, 20);
		jlbLogin_pw.setBounds(60, 370, 60, 50);
		jtfLogin_pw.setBounds(150, 386, 140, 20);
		jbtnLogin.setBounds(50, 450, 100, 40);
		jbtnJoin.setBounds(190, 450, 100, 40);
		jlb_bg.setSize(350, 600);
		
		jbtnLogin.addActionListener(this);
		jbtnJoin.addActionListener(this);
		
		jtfLogin_name.setText("김도연");
		jtfLogin_pw.setText("1234");
		
		cl1.add(jlbLogin_name);
		cl1.add(jtfLogin_name);
		cl1.add(jlbLogin_pw);
		cl1.add(jtfLogin_pw);
		cl1.add(jtfLogin_pw);
		cl1.add(jbtnLogin);
		cl1.add(jbtnJoin);
		cl1.add(jlb_bg);
		
		/* ******************* LoginPanel E ****************** */
		
		/* ******************* 회원가입 Panel S ****************** */
		
		jlbJoin_name = new JLabel("이름");
		jlbJoin_pw = new JLabel("암호");
		jlbJoin_birthday = new JLabel("생년월일");
		jlb_gender = new JLabel("성별");
		jlbJoin_deptname = new JLabel("부서");
		jlbJoin_hiredate = new JLabel("입사일");
		jlbJoin_position = new JLabel("직급");
		
		jtfJoin_name = new JTextField(20);
		jtfJoin_pw = new JPasswordField(20);
		jtfJoin_birthday = new JTextField(20);
		jr_gender_m = new JRadioButton("남자");
		jr_gender_f = new JRadioButton("여자");
		jcb_deptname = new JComboBox();
		jtfJoin_hiredate = new JTextField(20);
		jtfJoin_position = new JTextField(20);
		jbtnJoin_ok = new JButton("등록");
		jbtnJoin_cancel = new JButton("취소");
		
		cl2 = new JPanel();
		cl2.setLayout(null);
		Color c2 = new Color(119,187,242);
		cl2.setBackground(c2);
		
		jlbJoin_name.setBounds(50, 72, 60, 50);
		jtfJoin_name.setBounds(150, 88, 140, 20);
		jlbJoin_pw.setBounds(50, 122, 60, 50);
		jtfJoin_pw.setBounds(150, 138, 140, 20);
		jlbJoin_birthday.setBounds(50, 172, 60, 50);
		jtfJoin_birthday.setBounds(150, 188, 140, 20);
		jlb_gender.setBounds(50, 225, 60, 50);
		jr_gender_m.setBounds(150, 240, 60, 20);
		jr_gender_f.setBounds(230, 240, 60, 20);
		jr_gender_m.setBackground(c2);
		jr_gender_f.setBackground(c2);
		jlbJoin_hiredate.setBounds(50, 272, 60, 50);
		jtfJoin_hiredate.setBounds(150, 288, 140, 20);
		jlbJoin_position.setBounds(50, 322, 60, 50);
		jtfJoin_position.setBounds(150, 338, 140, 20);
		jlbJoin_deptname.setBounds(50, 372, 60, 50);
		// jtfJoin_deptname.setBounds(150, 238, 140, 20);
		jcb_deptname.setBounds(150, 388, 140, 20);
		
		jbtnJoin_ok.setBounds(50, 438, 100, 30);
		jbtnJoin_cancel.setBounds(180, 438, 100, 30);
		
		jbtnJoin_ok.addActionListener(this);
		jbtnJoin_cancel.addActionListener(this);
		jr_gender_m.addActionListener(this);
		jr_gender_f.addActionListener(this);
		
		cl2.add(jlbJoin_name);
		cl2.add(jtfJoin_name);
		cl2.add(jlbJoin_pw);
		cl2.add(jtfJoin_pw);
		cl2.add(jlbJoin_birthday);
		cl2.add(jtfJoin_birthday);
		cl2.add(jlbJoin_deptname);
		cl2.add(jcb_deptname);
		cl2.add(jlbJoin_hiredate);
		cl2.add(jtfJoin_hiredate);
		cl2.add(jlbJoin_position);
		cl2.add(jtfJoin_position);
		cl2.add(jbtnJoin_ok);
		cl2.add(jbtnJoin_cancel);
		cl2.add(jlb_gender);
		cl2.add(jr_gender_m);
		cl2.add(jr_gender_f);
		
		/* ******************* 회원가입 Panel E ****************** */
		
		jbtn1.setBounds(10, 30, 30, 30);
		
		// jp2.add(jbtn1);
		// jp2.add(jbtn2);
		
		// jp3.add(jl);
		
		// cl2.add(jp1, "list");
		setJMenuBar(jmb);
		
		add(cl1, "LoginPanel");
		add(cl2, "JoinPanel");
		
		layout.show(getContentPane(), "LoginPanel");
		// add(jp3, "South");
		setResizable(false);
		setVisible(true);
		
		Thread th = new Thread();
		th.start();
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		Thread th = new Thread(this);
		th.start();
		if (obj == jmi2) {
			System.out.println("종료");
			System.exit(0);
		} else if (obj == jmiLogout) {
			logo_img = getToolkit().getImage("src/project/images/logo1.png");
			repaint();
			layout.show(getContentPane(), "LoginPanel");
		} else if (obj == jpm1) {
			/*
			 * jtf.setText((String)jlValue); jta.requestFocus(true);
			 * 
			 * int result = JOptionPane.showConfirmDialog(null, jpPop, "Test",
			 * JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			 * 
			 * if(result == JOptionPane.OK_OPTION) { System.out.println("OK");
			 * }else{ System.out.println("Cancelled"); }
			 */
		} else if (obj == jpm2) {
			System.out.println(jlValue);
			
		} else if (obj == jpm3) {
			System.out.println(jlValue);
		/*	
		}else if(jr_gender_m.isSelected()){	
			//jr_gender_f.setSelected(false);
			//jr_gender_m.setSelected(true);
		}else if(jr_gender_f.isSelected()){
			//jr_gender_m.setSelected(false);
			//jr_gender_f.setSelected(true);
			*/
		} else if (obj == jbtnLogin) {
			try {
				Class.forName(driver);
				conn = DriverManager.getConnection(url, user, password);
				
				StringBuffer sql = new StringBuffer();
				sql.append("\n SELECT IP_V, USER_NO_N, DEPT_NO_N FROM DEPT_USER WHERE NAME_V = ? AND PASSWORD_V = ? ");
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setString(1, jtfLogin_name.getText());
				pstmt.setString(2, jtfLogin_pw.getText());
				rs = pstmt.executeQuery();
				
				int n = 0;
				if (rs.next()) {
					
					userNo = rs.getInt("user_no_n");
					ipNo = rs.getString("IP_V");
					deptNo = rs.getInt("dept_no_n");
					
					/* ******************** 탭버튼 S ********************* */
					JTabbedPane jtab = new JTabbedPane(){
						public void updateUI() {
							setUI(new ColoredTabbedPaneUI());
						}
					};
					/*
					 * jpanel01 = new JPanel01(){ public void
					 * paintComponent(Graphics g){ imgOn =
					 * Toolkit.getDefaultToolkit
					 * ().getImage("src/images/tab_on.png"); g.drawImage(imgOn,
					 * 0, 0, null); }
					 * 
					 * };
					 */
					
					
					cl3 = new JPanel(){
						 public void paintComponent(Graphics g) {
			                g.drawImage(bg_img.getImage(), 0, 0, null);
			                setOpaque(false);
			                super.paintComponent(g);
			            }
					};
					cl3.setLayout(null);
					jpanel01 = new JPanel01(userNo, deptNo);
					jpanel02 = new JPanel02(ipNo, userNo);
					jpanel03 = new JPanel03();
					jpanel04 = new JPanel04();
					Color orange = new Color(255, 170, 0);
					Color btn_color = new Color(88,152,152);
					Color tabColor = new Color(83,116,133);
					jtab.addTab("목록", jpanel01);
					jtab.addTab("메세지", jpanel02);
					jtab.addTab("자료실", jpanel03);
					jtab.addTab("공지사항", jpanel04);
					// jtab.setIconAt(0, new
					// ImageIcon("src/images/tab_on.png"));
					jtab.setBackground(tabColor);
					jtab.setForeground(Color.DARK_GRAY);
					jtab.setSelectedIndex(0);
					jtab.addChangeListener(new ChangeListener() {
						public void stateChanged(ChangeEvent e) {
							int i = jtab.getSelectedIndex();
					        ((ColoredTabbedPaneUI) jtab.getUI()).setSelectedTabBackground(btn_color);
					        jtab.revalidate();
					        jtab.repaint();
					    }
					});
					((ColoredTabbedPaneUI) jtab.getUI()).setSelectedTabBackground(btn_color);
					
					
					/*
					 * jbtnNotice.setFont(new Font("돋움", Font.PLAIN, 11));
					 * jbtnNotice.setBounds(1, 90, 78, 30);
					 * jbtnWorkIn.setBounds(1, 475, 78, 30);
					 * jbtnWorkOut.setBounds(1, 505, 78, 30);
					 * 
					 * cl3.add(jbtnNotice); cl3.add(jbtnWorkIn);
					 * cl3.add(jbtnWorkOut);
					 */
					jtab.setBounds(7, 5, 330, 535);
					cl3.add(jtab);
					cl3.setBackground(new Color(92,128,142));
					
					add(cl3, "ListPanel");
					//cl3.setBackground(new Color(255, 170, 0));
					/* ******************** 탭버튼 E ********************* */
					
					logo_img = null; // 로고 이미지 null로 초기화
					layout.show(getContentPane(), "ListPanel");
					
				} else {
					int result = JOptionPane.showConfirmDialog(null, "로그인 정보가 맞지 않습니다\n회원가입 창으로 이동하시겠습니까?", "로그인실패", 2);
					if (result == JOptionPane.OK_OPTION) {
						layout.show(getContentPane(), "JoinPanel");
					}
				}
				
			} catch (SQLException e1) {
				e1.printStackTrace();
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} finally {
				try {
					if (rs != null) rs.close();
					if (pstmt != null) pstmt.close();
					if (conn != null) conn.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			
		} else if (obj == jbtnJoin) {
			try {
				Class.forName(driver);
				conn = DriverManager.getConnection(url, user, password);
				StringBuffer sql = new StringBuffer();
				
				sql.append(" SELECT dept_name_v FROM dept_name ");
				pstmt = conn.prepareStatement(sql.toString());
				rs = pstmt.executeQuery();
				DefaultComboBoxModel dNames = new DefaultComboBoxModel();
				while (rs.next()) {
					dNames.addElement(rs.getString("dept_name_v"));
					jcb_deptname.setModel(dNames);
				}
				// 초기 개발3팀으로 셋팅
				jcb_deptname.setSelectedIndex(2);
				
			} catch (SQLException e1) {
				e1.printStackTrace();
			} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} finally {
				try {
					if (rs != null) rs.close();
					if (pstmt != null) pstmt.close();
					if (conn != null) conn.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			layout.show(getContentPane(), "JoinPanel");
		} else if (obj == jbtnJoin_cancel) {
			logo_img = getToolkit().getImage("src/project/images/logo1.png");
			repaint();
			layout.show(getContentPane(), "LoginPanel");
		} else if (obj == jbtnJoin_ok) {
			try {
				Class.forName(driver);
				conn = DriverManager.getConnection(url, user, password);
				StringBuffer sql = new StringBuffer();
				sql.append(" SELECT dept_no_n from dept_name WHERE dept_name_v = ? ");
				pstmt = conn.prepareStatement(sql.toString());
				String jcvdName = (String) jcb_deptname.getModel().getSelectedItem();
				pstmt.setString(1, jcvdName);
				rs = pstmt.executeQuery();
				rs.next();
				
				int dept_no_n = rs.getInt("dept_no_n");

				String name = jtfJoin_name.getText();
				String password = jtfJoin_pw.getText();
				String birthday = jtfJoin_birthday.getText();
				String ip = "";
				try {
					ip = InetAddress.getLocalHost().getHostAddress();
				} catch (UnknownHostException e2) {
					e2.printStackTrace();
				}
				
				String position = jtfJoin_position.getText();
				String hiredate = jtfJoin_hiredate.getText();
				String genderc = "";
				if(jr_gender_m.isSelected()){
					genderc = "남자";
					jr_gender_m.setSelected(false);
				}else if(jr_gender_f.isSelected()){
					genderc = "여자";
					jr_gender_f.setSelected(false);
				}
				rs.close();
				pstmt.close();
				
				sql.setLength(0);
				sql.append("\n INSERT INTO DEPT_USER (                           ");
				sql.append("\n                        USER_NO_N                  ");
				sql.append("\n                      , NAME_V                     ");
				sql.append("\n                      , PASSWORD_V                 ");
				sql.append("\n                      , BIRTHDAY_V                 ");
				sql.append("\n                      , IP_V                       ");
				sql.append("\n                      , DEPT_NO_N                  ");
				sql.append("\n                      , POSITION_V                 ");
				sql.append("\n                      , HIREDATE_V                 ");
				sql.append("\n                      , REG_D                      ");
				sql.append("\n                      , GENDER_C                   ");
				sql.append("\n                       )VALUES(                    ");
				sql.append("\n                             DEPT_USER_SEQ.NEXTVAL "); // USER_NO_N
				sql.append("\n                            , ?                    "); // NAME_V
				sql.append("\n                            , ?                    "); // PASSWORD_V
				sql.append("\n                            , ?                    "); // BIRTHDAY_V
				sql.append("\n                            , ?                    "); // IP_V
				sql.append("\n                            , ?                    "); // DEPT_NO_N
				sql.append("\n                            , ?                    "); // POSITION_V
				sql.append("\n                            , ?                    "); // HIREDATE_V
				sql.append("\n                            , SYSDATE              "); // REG_D
				sql.append("\n                            , ?                    "); // GENDER_C
				sql.append("\n                             )                     ");
				
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setString(1, name);
				pstmt.setString(2, password);
				pstmt.setString(3, birthday);
				pstmt.setString(4, ip);
				pstmt.setInt(5, dept_no_n);
				pstmt.setString(6, position);
				pstmt.setString(7, hiredate);
				pstmt.setString(8, genderc);
				// System.out.println("sql:"+sql.toString());
				
				
				int result = 0;
				result = pstmt.executeUpdate();
				
				if (result > 0) {
					JOptionPane.showMessageDialog(null, "등록이 완료 되었습니다");
				} else {
					JOptionPane.showMessageDialog(null, "등록이 실패 하였습니다");
				}
				logo_img = getToolkit().getImage("src/project/images/logo1.png");
				repaint();
				layout.show(getContentPane(), "LoginPanel");
				
			} catch (SQLException e1) {
				System.out.println("SQL 에러");
				e1.printStackTrace();
			} catch (ClassNotFoundException e1) {
				System.out.println("클래스를 찾을수 없습니다.");
			} finally {
				try {
					if (rs != null) rs.close();
					if (pstmt != null) pstmt.close();
					if (conn != null) conn.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		} else if (obj == jpm1) {
			// jtf.setText(TreeData.toString());
			jpop_menu.setVisible(false);
			jtaSend.setText("");
			
			int sendResult = JOptionPane.showConfirmDialog(null, jpPop, "쪽지보내기", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
			
			if (sendResult == JOptionPane.OK_OPTION) {
				System.out.println("OK");
				String ip_v = jtaSend.getText();
				jtaSend.setText("");
				pw.println(ip_v + ":" + jtaSend.getText());
				pw.flush();
				
			} else {
				System.out.println("Cancelled");
			}
			/*
			 * }else if(obj==jbtn_file){ layout.show(getContentPane(),
			 * "jp_file");
			 */
		}
		
	} // actionPerformed End
	
	
	@Override
	public void mouseClicked(MouseEvent e) {
		Object obj = e.getSource();
		int x = e.getXOnScreen();
		int y = e.getYOnScreen();
		int btnNum = e.getButton();
		
		// System.out.println(obj);
		// System.out.println(jlValue);
		// System.out.println("x : " + x + "\ny : "+y);
		// System.out.println("btnNum : "+btnNum);
		
		if (btnNum == 3) {
			jpop_menu.setLocation(x, y);
			jpop_menu.setVisible(true);
			
		} else if (btnNum == 1) {
			jpop_menu.setVisible(false);
			
		} else if (obj == jpm2) {
			System.out.println("jpm2");
			
		} else if (obj == jpm3) {
			System.out.println("jpm3");
			
		} else {
			
		}
		
	}
	
	@Override
	public void run() {
		/*
		try {
			System.out.println("Goodzzy run");
			s = new Socket("localhost", 5000);
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s.getOutputStream())));
			System.out.println("aaa"+br);
			String data = null;
			while (true) {
				data = br.readLine();
				jta2.append(data + "\n");
				System.out.println("[msg]" + data);
				String user = "";
				String msg = "";
				if (msg != null) {
					user = data.substring(0, data.indexOf(":"));
					msg = data.substring(data.indexOf(":") + 1, data.length());
					jta2.setText(msg);
					
					int sendResult = JOptionPane
							.showConfirmDialog(null, jpPop2, user + "님이 쪽지를 보냈습니다", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
					
					if (sendResult == JOptionPane.OK_OPTION) {
						System.out.println("OK");
						
					} else {
						System.out.println("Cancelled");
					}
				}
				
				JScrollBar jsb = jsp2.getVerticalScrollBar();
				int max = jsb.getMaximum();
				jsb.setValue(max);
			}
			
		} catch (UnknownHostException e) {
			System.out.println("알수없는 호스트");
		} catch (IOException e) {
			System.out.println("접속실패 다시 확인하세요");
		}
		*/
	} // run() End


	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.drawImage(logo_img, 30, 80, this);
	}
	
	@Override
	public void mouseEntered(MouseEvent arg0) {
	}
	
	@Override
	public void mouseExited(MouseEvent arg0) {
	}
	
	@Override
	public void mousePressed(MouseEvent arg0) {
	}
	
	@Override
	public void mouseReleased(MouseEvent arg0) {
	}
	
	public static void main(String[] args) {
		new GoodZzy("구찌 ver1.18");
	}
}

class ColoredTabbedPaneUI extends MetalTabbedPaneUI {
    public void setSelectedTabBackground(Color color) {
    	selectColor = color;
    }

    protected void paintFocusIndicator(Graphics g, int tabPlacement,
        Rectangle[] rects, int tabIndex, Rectangle iconRect,
        Rectangle textRect, boolean isSelected) {
    }
  }
