package project;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

class JPanel02 extends JPanel implements MouseListener, ActionListener {
	JButton jbtn44, jbtn55, jbtn66;
	JButton jbtn_Reply;
	
	JTextArea jta2;
	JScrollPane jsp;
	
	Socket s;
	BufferedReader br;
	PrintWriter pw;
	JPanel jp1;
	JList jlist;
	
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@192.168.0.95:1521:orcl";
	String user = "scott";
	String password = "tiger";
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	
	StringBuffer sql;
	String name;
	String ip = "";
	int userNo = 0;
	String receiver = "";
	
	public JPanel02(String ip, int userNo) {
		this.ip = ip;
		this.userNo = userNo;
		System.out.println(ip);
		setLayout(null);
		
		jbtn44 = new JButton("버튼4");
		jbtn55 = new JButton("버튼5");
		jbtn_Reply = new JButton("답장");
		jbtn_Reply.setBounds(230, 460, 80, 33);
		jbtn_Reply.addActionListener(this);
		
		StringBuffer sql = new StringBuffer();
		
		DefaultListModel listModel = new DefaultListModel();

		jlist = new JList();
		
		jlist.setBounds(0, 0, 300, 300);
		jlist.setModel(listModel);
		add(jlist);
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, password);
			
			sql = new StringBuffer();
			sql.append("\n SELECT COUNT(*) OVER() TOTALCOUNT, read_chk_c, ip_v, sender, msg FROM user_msg2 ");
			sql.append("\n  WHERE IP_V = ? ");
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setString(1, ip);
			
			rs = pstmt.executeQuery();
			listModel.addElement("쪽지 보관함");
			int n = 0;
			Object elements;
			while (rs.next()) {
				int chk = rs.getInt("read_chk_c");
				elements = (rs.getString("sender") + "> " + rs.getString("msg")+(chk==0?" (읽음)":""));
				if(chk==1) jlist.setFont(new Font("Helvetica Neue", Font.PLAIN, 12));
				listModel.addElement(elements);
				n++;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}finally{
			try {
				if(rs != null) rs.close();
				if(pstmt != null) pstmt.close();
				if(conn != null) conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		jta2 = new JTextArea();
		jsp = new JScrollPane(jta2, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		jbtn44.setBounds(10, 10, 80, 30);
		jbtn55.setBounds(100, 10, 80, 30);
		
		jlist.setBounds(0, 0, 350, 450);
		jlist.addMouseListener(this);
		//jlist.setFont(new Font("Helvetica Neue", Font.BOLD, 12));

		add(jlist);
		add(jbtn_Reply);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			sendMsg();
		}
		
	}

	private void sendMsg() {
		String msg = (String) jlist.getSelectedValue();
		receiver = msg.substring(0, msg.indexOf(">"));
		
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, password);
			sql = new StringBuffer();
			
			sql.append(" UPDATE user_msg2 SET read_chk_c = '0', read_reg_d = SYSDATE WHERE ip_v = ? ");
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setString(1, ip);
			pstmt.executeUpdate();
			
			pstmt.close();
			sql.setLength(0);
			
			sql.append("\n SELECT name_v, user_no_n, ip_v FROM dept_user WHERE name_v = ? ");
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setString(1, receiver);
			rs = pstmt.executeQuery();
			
			if(rs.next()){
				//userNo = rs.getInt("user_no_n");
				name = rs.getString("name_v");
				ip = rs.getString("ip_v");
			}
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		ChatScreen cs = new ChatScreen(name, userNo);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		
		if(obj==jbtn_Reply){
			sendMsg();
		}
	}
	
}