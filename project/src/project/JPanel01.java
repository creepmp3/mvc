package project;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;

class JPanel01 extends JPanel implements ActionListener, MouseListener, PopupMenuListener, Runnable {
	
	JButton jbtn11, jbtn22, jbtn33;
	
	JTextArea jtaSend, jta2, jta3;
	JScrollPane jsp, jsp2;
	JTextField jtf, jtf_ip, jtf2;
	JPanel jpPop, jpPop2;
	JPopupMenu jpop;
	
	// 상단 메뉴
	JMenuBar jmb;
	JMenu jm;
	JMenuItem jmi1, jmi2, jmiLogout;
	JMenuItem jpm1, jpm2, jpm3;
	JButton jbtn_workIn, jbtn_workOut, jbtn_file, jbtn_logout, jbtn_status, jbtn_status_msg, jbtn_status_msg_ok1, jbtn_status_msg_ok2, jbtn_notice;
	JComboBox jcb_status;
	JTextField jtf_status_msg;
	
	// 부서목록
	DefaultMutableTreeNode TreeData;
	JTree tree;
	
	// 정보보기
	JPanel jp_info_Pop;
	JLabel jlb_info_name, jlb_info_deptname, jlb_info_userno, jlb_info_birthday, jlb_info_position, jlb_info_hiredate, jlb_info_status_msg;
	
	// 지각
	JPanel jpPop_commute;
	JScrollPane jsp_commute;
	JTextArea jta_commute;
	JButton jbtn_commute_ok, jbtn_commute_cancel;
	
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@192.168.0.95:1521:orcl";
	String user = "scott";
	String password = "tiger";
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	
	Socket s;
	BufferedReader br;
	PrintWriter pw;
	
	int userNo = 0;
	int deptNo = 0;
	String ip_v = "";
	String user_namev = "";
	String status_msg = "";
	
	Color btn_color = new Color(10, 60, 83);
	
	public JPanel01(int userNo, int deptNo) {
		setLayout(null);
		// setBackground(new Color(255, 170, 0));
		this.userNo = userNo;
		this.deptNo = deptNo;
		// 상단 버튼 부분
		Color btn_color = new Color(10, 60, 83);
		// jbtn_status = new JButton();
		
		jbtn_status_msg = new JButton();
		
		jcb_status = new JComboBox();
		
		jbtn_logout = new JButton("로그아웃");
		jbtn_workIn = new JButton("출근");
		jbtn_workOut = new JButton("퇴근");
		jbtn_file = new JButton("");
		jbtn_notice = new JButton();
		jtf_status_msg = new JTextField(20);
		jbtn_status_msg_ok1 = new JButton("수정");
		jbtn_status_msg_ok2 = new JButton("수정");
		/*
		 * jbtn_status.setBounds(2, 0, 234, 24); jbtn_status.setFont(new
		 * Font("돋움", Font.PLAIN, 11));
		 * jbtn_status.setHorizontalAlignment(JButton.LEFT);
		 * jbtn_status.setBackground(Color.LIGHT_GRAY);
		 * jbtn_status.setBorderPainted(false);
		 */
		jbtn_status_msg.setBounds(2, 24, 178, 24);
		jbtn_status_msg.setFont(new Font("돋움", Font.PLAIN, 12));
		jbtn_status_msg.setBackground(Color.LIGHT_GRAY);
		jbtn_status_msg.setBorderPainted(false);
		jbtn_status_msg.setHorizontalAlignment(JButton.LEFT);
		jbtn_status_msg_ok1.setBounds(180, 25, 56, 22);
		jbtn_status_msg_ok1.setFont(new Font("돋움", Font.PLAIN, 11));
		
		jtf_status_msg.setBounds(2, 24, 178, 24);
		jbtn_status_msg_ok2.setBounds(180, 25, 56, 22);
		jbtn_status_msg_ok2.setFont(new Font("돋움", Font.PLAIN, 11));
		
		jbtn_logout.setBounds(238, 0, 86, 48);
		
		jcb_status.setBounds(2, 0, 234, 24);
		
		jbtn_workIn.setBounds(2, 50, 60, 50);
		jbtn_workOut.setBounds(62, 50, 60, 50);
		jbtn_file.setBounds(122, 50, 60, 50);
		jbtn_notice.setBounds(182, 50, 142, 50);
		jbtn_notice.setFont(new Font("돋움", Font.PLAIN, 11));
		jbtn_notice.setEnabled(false);
		
		jsp = new JScrollPane(tree, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		jbtn_status_msg_ok1.addActionListener(this);
		jbtn_status_msg_ok2.addActionListener(this);
		jbtn_workIn.addActionListener(this);
		jbtn_workOut.addActionListener(this);
		jbtn_logout.addActionListener(this);
		jbtn_file.addActionListener(this);
		
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
		GregorianCalendar gc = new GregorianCalendar();
		Date d = gc.getTime();
		long nowTime = Long.parseLong(sdf.format(d));
		long attendanceTime = Long.parseLong("090000");
		
		
		// add(jbtn_status);
		add(jbtn_status_msg);
		add(jbtn_status_msg_ok1);
		add(jcb_status);
		add(jbtn_logout);
		add(jbtn_workIn);
		add(jbtn_workOut);
		add(jbtn_file);
		add(jbtn_notice);
		// jbtn_status.addActionListener(this);
		jbtn_status_msg.addActionListener(this);
		jcb_status.addActionListener(this);
		
		/* ******************* JTree S ********************* */
		
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, password);
			
			StringBuffer sql = new StringBuffer();
			sql.append("\n SELECT name_v, STATUS, STATUS_MSG, DEPT_NO_N FROM DEPT_USER WHERE USER_NO_N = ? ");
			
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setInt(1, userNo);
			rs = pstmt.executeQuery();
			rs.next();
			String status = rs.getString("status");
			user_namev = rs.getString("name_v");
			status_msg = rs.getString("status_msg");
			jbtn_status_msg.setText(status_msg);
			deptNo = rs.getInt("dept_no_n");
			// jbtn_status.setText(status);
			DefaultComboBoxModel status_cb = new DefaultComboBoxModel();
			status_cb.addElement("로그인");
			status_cb.addElement("로그아웃");
			status_cb.addElement("자리비움");
			status_cb.addElement("외근");
			jcb_status.setModel(status_cb);
			jcb_status.setSelectedItem(status != null ? status : "로그인");
			
			rs.close();
			
			sql.setLength(0);
			sql.append("\n SELECT COUNT(*) OVER() TOTALCOUNT FROM DEPT_USER WHERE DEPT_NO_N = ? ");
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setInt(1, deptNo);
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				
				rs.next();
				int totalcount = rs.getInt("totalcount");
				String[] jtree_str = new String[totalcount];
				
				pstmt.close();
				
				sql = new StringBuffer();
				sql.append("\n SELECT u.name_v , d.dept_name_v, u.ip_v, u.status, u.status_msg, u.gender_c  ");
				sql.append("\n   FROM dept_user u , dept_name d         ");
				sql.append("\n  WHERE u.dept_no_n = d.dept_no_n         ");
				sql.append("\n    AND u.dept_no_n = ?                   ");
				
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setInt(1, deptNo);
				rs = pstmt.executeQuery();
				
				String deptname = "";
				
				int n = 0;
				int gender = 0;
				String name = "";
				String genderc = "";
				while (rs.next()) {
					name = rs.getString("name_v");
					deptname = rs.getString("dept_name_v");
					jtree_str[n] = name + " (" + rs.getString("ip_v") + ")" + " " + (rs.getString("status_msg")==null?"":rs.getString("status_msg"));
					genderc = rs.getString("gender_c");
					n++;
				}
				
				// 부서명을 담는다
				DefaultMutableTreeNode root = new DefaultMutableTreeNode(deptname);
				// 부서명을 담은 TreeNode를 기본 TreeModel에 담는다 DefaultTreeModel
				DefaultTreeModel dtm = new DefaultTreeModel(root);
				
				for (int i = 0; i < jtree_str.length; i++) {
					// 하위 TreeNode를 배열크기만큼 생성하면서 부서명을 담은 TreeNode에 담는다
					DefaultMutableTreeNode dmtn1 = new DefaultMutableTreeNode(jtree_str[i]);
					root.add(dmtn1);
				}
				
				/*
				 * String iconType = ""; if(genderc.equals("남자")){ iconType =
				 * "1.png"; }else{ iconType = "2.png"; }
				 */
				
				Object iconObj = LookAndFeel.makeIcon(JPanel01.class, "1.png");
				UIManager.put("Tree.leafIcon", iconObj);
				
				tree = new JTree(dtm);
				tree.setBounds(3, 103, 321, 393);
				// tree.setBackground(new Color(255, 170, 0));
				tree.expandRow(0); // 1번 노드를 확장시킴
				tree.addSelectionRow(0); // 0번째를 선택 상태로 출력
				tree.addMouseListener(this);
				
				Color c1 = new Color(255, 170, 0);
				DefaultTreeCellRenderer cellRenderer = (DefaultTreeCellRenderer) tree.getCellRenderer();
				// cellRenderer.setBackgroundNonSelectionColor(c1);
				// cellRenderer.setBackgroundSelectionColor(new Color(0, 0,
				// 128));
				// cellRenderer.setBorderSelectionColor(Color.black);
				// cellRenderer.setTextNonSelectionColor(Color.black);
				
				add(tree);
				
				pstmt.close();
				
				sql.setLength(0);
				sql.append("\n SELECT *                                                      ");
				sql.append("\n   FROM (SELECT                                                ");
				sql.append("\n            ROW_NUMBER() OVER(ORDER BY COMMUTE_NO_N DESC) rnum ");
				sql.append("\n          , commute_no_n                                       ");
				sql.append("\n          , user_no_n                                          ");
				// sql.append("\n          , msg_v                                              ");
				sql.append("\n          , TO_CHAR(work_in_d, 'YYYY-MM-DD')      workind      ");
				sql.append("\n          , TO_CHAR(work_out_d, 'YYYY-MM-DD')     workoutd     ");
				sql.append("\n          , TO_CHAR(reg_d, 'YYYY-MM-DD HH:mm:ss') reg          ");
				sql.append("\n          , TO_CHAR(sysdate, 'YYYY-MM-DD') today               ");
				sql.append("\n          , TO_CHAR(reg_d, 'YYYY-MM-DD') regd                  ");
				sql.append("\n           FROM dept_commute                                   ");
				sql.append("\n          WHERE user_no_n = ?                                  ");
				sql.append("\n        )                                                      ");
				sql.append("\n  WHERE today = regd                                           ");
				// sql.append("\n    AND rnum = 1                                               ");
				
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setInt(1, userNo);
				rs = pstmt.executeQuery();
				
				if (rs.next()) {
					// String commute_msg = rs.getString("msg_v");
					String regd = rs.getString("regd");
					String workind = rs.getString("workind");
					String workoutd = rs.getString("workoutd");
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-mm-dd");
					String month = "" + (cal.get(Calendar.MONTH) + 1);
					if (month.length() == 1)
						month = "0" + month;
					
					String today = cal.get(Calendar.YEAR) + "-" + month + "-" + cal.get(Calendar.DATE);
					if (regd != null) {
						// Calendar 객체의 오늘날짜와 쿼리의 등록된 날짜가 같은지 비교
						if (today.equals(regd)) {
							if (workind != null) {
								jbtn_workIn.setEnabled(false);
								jbtn_workIn.setBackground(Color.LIGHT_GRAY);
							}
							if (workoutd != null) {
								jbtn_workOut.setEnabled(false);
								jbtn_workOut.setBackground(Color.LIGHT_GRAY);
							}
						}
					}
					// jbtn_status_msg.setText(status_msg);
					
					// 지금시간이 09시 보다 작다면 퇴근 버튼 비활성
					if (nowTime < attendanceTime) {
						
						jbtn_workOut.setEnabled(false);
					}
					
				}// if End
				
			} // if End
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		/* ******************* JTree E ********************* */
		
		/* ******************* 메뉴팝업 S ********************* */
		
		jpm1 = new JMenuItem("쪽지보내기");
		jpm2 = new JMenuItem("대화하기");
		jpm3 = new JMenuItem("정보보기");
		
		jpop = new JPopupMenu();
		
		jpm1.addActionListener(this);
		jpm2.addActionListener(this);
		jpm3.addActionListener(this);
		
		jpop.add(jpm1);
		jpop.add(jpm2);
		jpop.addSeparator();
		jpop.add(jpm3);
		jpop.setSize(50, 100);
		jpop.setBorder(new BevelBorder(BevelBorder.RAISED));
		// jpop_menu.addPopupMenuListener(this);
		
		add(jpop);
		
		/* ******************* 메뉴팝업 E ********************* */
		
		/* ******************* 쪽지보내기 팝업 S ************************ */
		jpPop = new JPanel();
		
		jtaSend = new JTextArea(10, 30);
		jsp = new JScrollPane(jtaSend, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jtf = new JTextField(10);
		jtf_ip = new JTextField(10);
		
		jtf.setLocation(0, 0);
		
		jpPop.add(jtf);
		jpPop.add(jtf_ip);
		jpPop.add(jsp);
		
		jpPop2 = new JPanel(new BorderLayout());
		
		jta2 = new JTextArea(10, 30);
		jsp2 = new JScrollPane(jta2, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jtf2 = new JTextField(10);
		
		jtf2.setLocation(0, 0);
		
		// jpPop2.add(jtf2);
		jpPop2.add(jsp2);
		
		/* ******************* 쪽지보내기 팝업 E ************************ */
		
		/* ******************* 지각사유 팝업 S ************************ */
		
		jpPop_commute = new JPanel(new BorderLayout());
		
		jta_commute = new JTextArea(10, 30);
		jsp_commute = new JScrollPane(jta_commute, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		jpPop_commute.add(jsp_commute);
		
		/* ******************* 지각사유 팝업 E ************************ */
		
		/*
		 * add(jbtn11); add(jbtn22);
		 */

		Thread th = new Thread();
		th.start();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		Thread th = new Thread(this);
		th.start();
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, password);
			
			if (obj == jpm1) {
				jpop.setVisible(false);
				/*
				user_namev = TreeData.toString();
				user_namev = user_namev.substring(0, user_namev.indexOf(" "));
				jtf.setText(user_namev);
				
				StringBuffer sql = new StringBuffer();
				sql.append(" SELECT IP_V FROM dept_user WHERE name_v = ? ");
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setString(1, user_namev);
				System.out.println("user_namev:" + user_namev);
				rs = pstmt.executeQuery();
				rs.next();
				ip_v = rs.getString("ip_v");
				jtaSend.setText("");
				jtf_ip.setText(ip_v);
				
				jpop.setVisible(false);
				
				int sendResult = JOptionPane.showConfirmDialog(null, jpPop, "쪽지보내기", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
				
				if (sendResult == JOptionPane.OK_OPTION) {
					System.out.println(jtaSend.getText());
					
					try {
						pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s.getOutputStream())));
						pw.println(ip_v + ":" + jtaSend.getText());
						pw.flush();
					} catch (UnknownHostException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				
				*/
				
				String abc = TreeData.toString().substring(0, TreeData.toString().indexOf(" "));
				ChatScreen cs = new ChatScreen(abc, userNo);
				
			// 2015.03.25 이은선
			}else if (obj == jpm2){
				jpop.setVisible(false);
				GoodzzyChatClient gcc = new GoodzzyChatClient("대화중", user_namev);	
			} else if (obj == jpm3) {
				jpop.setVisible(false);
				String tree_name = TreeData.toString();
				StringBuffer sql = new StringBuffer();
				/*
				 * sql.append("\n SELECT              ");
				 * sql.append("\n      USER_NO_N      ");
				 * sql.append("\n    , DEPT_NAME_V    ");
				 * sql.append("\n    , BIRTHDAY_V     ");
				 * sql.append("\n    , HIREDATE_V     ");
				 * sql.append("\n    , POSITION_V     ");
				 * sql.append("\n    , STATUS_MSG     ");
				 * sql.append("\n  FROM DEPT_USER     ");
				 * sql.append("\n WHERE NAME_V = ? ");
				 */
				sql.append("\n SELECT                           ");
				sql.append("\n       u.user_no_n                ");
				sql.append("\n     , d.dept_no_n                ");
				sql.append("\n     , d.dept_name_v              ");
				sql.append("\n     , u.birthday_v               ");
				sql.append("\n     , u.hiredate_v               ");
				sql.append("\n     , u.position_v               ");
				sql.append("\n     , u.status_msg               ");
				sql.append("\n   FROM dept_user u               ");
				sql.append("\n   LEFT JOIN dept_name d          ");
				sql.append("\n     ON u.dept_no_n = d.dept_no_n ");
				sql.append("\n  WHERE u.name_v = ?              ");
				tree_name = tree_name.substring(0, tree_name.indexOf(" "));
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setString(1, tree_name);
				rs = pstmt.executeQuery();
				int n = 0;
				if (rs.next()) {
					userNo = rs.getInt("user_no_n");
					String deptname = rs.getString("dept_name_v");
					String birthday = rs.getString("birthday_v");
					String hiredate = rs.getString("hiredate_v");
					String position = rs.getString("position_v");
					String statusmsg = rs.getString("status_msg");
					
					/* ******************* 정보보기 팝업 S ************************ */
					jp_info_Pop = new JPanel();
					// jp_info_Pop.setLayout(null);
					
					jlb_info_name = new JLabel();
					jlb_info_deptname = new JLabel();
					jlb_info_userno = new JLabel();
					jlb_info_birthday = new JLabel();
					jlb_info_position = new JLabel();
					jlb_info_hiredate = new JLabel();
					// jlb_info_status_msg = new JLabel();
					
					jlb_info_name.setBounds(0, 0, 60, 30);
					jlb_info_deptname.setBounds(0, 30, 60, 30);
					/*
					 * jp_info_Pop.add(jlb_info_name);
					 * jp_info_Pop.add(jlb_info_deptname);
					 * jp_info_Pop.add(jlb_info_userno);
					 * jp_info_Pop.add(jlb_info_birthday);
					 * jp_info_Pop.add(jlb_info_position);
					 * jp_info_Pop.add(jlb_info_hiredate);
					 */
					// jp_info_Pop.add(jlb_info_status_msg);
					
					JTextArea jta_info = new JTextArea();
					jta_info.setBackground(new Color(238, 238, 238));
					jta_info.setFocusable(false);
					jta_info.append("사원번호 : " + String.valueOf(userNo));
					jta_info.append("\n이름 : " + tree_name);
					jta_info.append("\n부서 : " + deptname);
					jta_info.append("\n생일 : " + birthday);
					jta_info.append("\n입사일 : " + hiredate);
					jta_info.append("\n직급 : " + position);
					
					jp_info_Pop.add(jta_info);
					
					/* ******************* 정보보기 팝업 E ************************ */
					
					jlb_info_name.setText("이름 : " + tree_name);
					jlb_info_deptname.setText("부서 : " + deptname);
					jlb_info_userno.setText("유저번호 : " + String.valueOf(userNo));
					jlb_info_birthday.setText("생일 : " + birthday);
					jlb_info_hiredate.setText("입사일 : " + hiredate);
					jlb_info_position.setText("직급 : " + position);
					// jlb_info_status_msg.setText(statusmsg);
				}
				JOptionPane.showConfirmDialog(null, jp_info_Pop, "정보보기", JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION);
				
			} else if (obj == jbtn_workIn) {
				
				SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
				GregorianCalendar gc = new GregorianCalendar();
				Date d = gc.getTime();
				long nowTime = Long.parseLong(sdf.format(d));
				// long time = Long.parseLong("089999");
				long attendanceTime = Long.parseLong("090000");
				
				// 지금시간이 09시를 넘었다면 지각사유 (현재는 09시보다 작으면 지각처리됨)
				// nowTime<attendanceTime
				if (nowTime > attendanceTime) {
					
					int result = JOptionPane.showConfirmDialog(null, "지각하셨습니다 \n사유를 입력하시겠습니까?", "지각", JOptionPane.YES_NO_OPTION);
					if (result == JOptionPane.YES_OPTION) {
						int sendResult = JOptionPane.showConfirmDialog(null, jpPop_commute, "지각사유", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
						
						if (sendResult == JOptionPane.OK_OPTION) {
							
							StringBuffer sql = new StringBuffer();
							sql.append("\n INSERT INTO DEPT_COMMUTE (                            ");
							sql.append("\n                           COMMUTE_NO_N                ");
							sql.append("\n                         , USER_NO_N                   ");
							sql.append("\n                         , WORK_IN_D                   ");
							sql.append("\n                         , MSG_V                       ");
							sql.append("\n                         , REG_D                       ");
							sql.append("\n                          )VALUES(                     ");
							sql.append("\n                              DEPT_COMMUTE_SEQ.NEXTVAL "); // COMMUTE_NO_N
							sql.append("\n                            , ?                        "); // USER_NO_N
							sql.append("\n                            , SYSDATE                  "); // WORK_IN_D
							sql.append("\n                            , ?                        "); // MSG_V
							sql.append("\n                            , SYSDATE                  "); // REG_D
							sql.append("\n                             )                         ");
							
							pstmt = conn.prepareStatement(sql.toString());
							pstmt.setInt(1, userNo);
							pstmt.setString(2, jta_commute.getText());
							
							int result2 = 0;
							result2 = pstmt.executeUpdate();
							
							if (result2 > 0) {
								JOptionPane.showMessageDialog(null, "등록이 완료 되었습니다");
								jbtn_workIn.setBackground(Color.LIGHT_GRAY);
								jbtn_workIn.setEnabled(false);
							} else {
								JOptionPane.showMessageDialog(null, "등록이 실패 하였습니다");
							}
						}
					}
					
				} else {
					
					StringBuffer sql = new StringBuffer();
					sql.append("\n INSERT INTO DEPT_COMMUTE (                            ");
					sql.append("\n                           COMMUTE_NO_N                ");
					sql.append("\n                         , USER_NO_N                   ");
					sql.append("\n                         , WORK_IN_D                   ");
					sql.append("\n                         , REG_D                       ");
					sql.append("\n                          )VALUES(                     ");
					sql.append("\n                              DEPT_COMMUTE_SEQ.NEXTVAL "); // COMMUTE_NO_N
					sql.append("\n                            , ?                        "); // USER_NO_N
					sql.append("\n                            , SYSDATE                  "); // WORK_IN_D
					sql.append("\n                            , SYSDATE                  "); // REG_D
					sql.append("\n                             )                         ");
					
					pstmt = conn.prepareStatement(sql.toString());
					pstmt.setInt(1, userNo);
					
					int result2 = 0;
					result2 = pstmt.executeUpdate();
					
					if (result2 > 0) {
						JOptionPane.showMessageDialog(null, "출근체크가 완료 되었습니다");
						jbtn_workIn.setEnabled(false);
					} else {
						JOptionPane.showMessageDialog(null, "출근체크가 실패 하였습니다");
					}
					
				}
				
			} else if (obj == jbtn_workOut) {
				StringBuffer sql = new StringBuffer();
				
				sql.append("\n SELECT *                                                      ");
				sql.append("\n   FROM (SELECT                                                ");
				sql.append("\n            ROW_NUMBER() OVER(ORDER BY COMMUTE_NO_N DESC) rnum ");
				sql.append("\n          , commute_no_n                                       ");
				sql.append("\n          , user_no_n                                          ");
				// sql.append("\n          , msg_v                                              ");
				sql.append("\n          , TO_CHAR(work_in_d, 'YYYY-MM-DD')      workind      ");
				sql.append("\n          , TO_CHAR(sysdate, 'YYYY-MM-DD') today               ");
				sql.append("\n          , TO_CHAR(reg_d, 'YYYY-MM-DD') regd                  ");
				sql.append("\n           FROM dept_commute                                   ");
				sql.append("\n          WHERE user_no_n = ?                                  ");
				sql.append("\n        )                                                      ");
				sql.append("\n  WHERE today = regd                                           ");
				sql.append("\n    AND rnum = 1                                               ");
				
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setInt(1, userNo);
				rs = pstmt.executeQuery();
				
				if (rs.next()) {
					
					// String commute_msg = rs.getString("msg_v");
					String regd = rs.getString("regd");
					String workind = rs.getString("workind");
					
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-mm-dd");
					String month = "" + (cal.get(Calendar.MONTH) + 1);
					if (month.length() == 1)
						month = "0" + month;
					
					String today = cal.get(Calendar.YEAR) + "-" + month + "-" + cal.get(Calendar.DATE);
					
					pstmt.close();
					
					if (regd != null) {
						// Calendar 객체의 오늘날짜와 쿼리의 등록된 날짜가 같은지 비교
						if (today.equals(regd)) {
							if (workind != null) {
								sql.setLength(0);
								sql.append("\n UPDATE DEPT_COMMUTE         ");
								sql.append("\n    SET WORK_OUT_D = SYSDATE ");
								sql.append("\n  WHERE USER_NO_N = ?        ");
								
								pstmt = conn.prepareStatement(sql.toString());
								pstmt.setInt(1, userNo);
								
								int result2 = 0;
								result2 = pstmt.executeUpdate();
								
								if (result2 > 0) {
									JOptionPane.showMessageDialog(null, "퇴근체크가 완료 되었습니다");
									jbtn_workOut.setBackground(Color.LIGHT_GRAY);
									jbtn_workOut.setEnabled(false);
								} else {
									JOptionPane.showMessageDialog(null, "퇴근체크에 실패 하였습니다");
								}
							}
						}
					}
				} else {
					JOptionPane.showMessageDialog(null, "출근부터 하시졈");
				}
				
			} else if (obj == jbtn_status_msg) {
				jbtn_status_msg.setVisible(false);
				jbtn_status_msg_ok1.setVisible(false);
				add(jtf_status_msg);
				add(jbtn_status_msg_ok2);
				jtf_status_msg.setVisible(true);
				jbtn_status_msg_ok2.setVisible(true);
				
				StringBuffer sql = new StringBuffer();
				sql.append("\n SELECT STATUS_MSG FROM DEPT_USER WHERE USER_NO_N = ? ");
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setInt(1, userNo);
				rs = pstmt.executeQuery();
				
				if (rs.next()) {
					jtf_status_msg.setText(rs.getString("status_msg"));
				}
				
				/*
				 * int sendResult = JOptionPane.showConfirmDialog(null,
				 * jpPop_status, "지각사유", JOptionPane.OK_CANCEL_OPTION,
				 * JOptionPane.PLAIN_MESSAGE);
				 * 
				 * if (sendResult == JOptionPane.OK_OPTION) {
				 */
			} else if (obj == jbtn_status_msg_ok2) {
				StringBuffer sql = new StringBuffer();
				String msgText = jtf_status_msg.getText();
				sql.append("\n UPDATE DEPT_USER SET status_msg = ? ");
				sql.append("\n  WHERE USER_NO_N = ?                ");
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setString(1, msgText);
				pstmt.setInt(2, userNo);
				int msgUpdate = 0;
				msgUpdate = pstmt.executeUpdate();
				if (msgUpdate != 0) {
					JOptionPane.showMessageDialog(null, "상태정보 변경에 성공 하였습니다");
					
					jtf_status_msg.setVisible(false);
					jbtn_status_msg_ok2.setVisible(false);
					jbtn_status_msg.setVisible(true);
					jbtn_status_msg_ok1.setVisible(true);
					jbtn_status_msg.setText(msgText);
					
				} else {
					JOptionPane.showMessageDialog(null, "상태정보 변경에 실패 하였습니다");
				}
			} else if (obj == jbtn_logout) {
				System.exit(0);
			} else if (obj == jcb_status) {
				String cmd = (String) jcb_status.getModel().getSelectedItem();
				
				StringBuffer sql = new StringBuffer();
				sql.append("\n UPDATE DEPT_USER SET status = ? ");
				sql.append("\n  WHERE USER_NO_N = ?            ");
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setString(1, cmd);
				pstmt.setInt(2, userNo);
				int result3 = 0;
				result3 = pstmt.executeUpdate();
			} else if (obj == jbtn_status_msg) {
				System.out.println(jbtn_status_msg.getText());
			}
			
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		Object obj = e.getSource();
		int x = e.getXOnScreen();
		int y = e.getYOnScreen();
		int btnNum = e.getButton();
		
		// System.out.println(obj);
		// System.out.println(jlValue);
		// System.out.println("x : " + x + "\ny : "+y);
		// System.out.println("btnNum : "+btnNum);
		// System.out.println(tree.getLeadSelectionRow());
		// System.out.println(tree.getLeadSelectionPath());
		
		TreeData = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		// System.out.println(TreeData.getUserObject());
		if (TreeData != null && btnNum == 3) {
			jpop.setLocation(x, y);
			jpop.setVisible(true);
			
		} else if (btnNum == 1 || obj == tree) {
			jpop.setVisible(false);
			
		} else if (obj == jpm2) {
			System.out.println("jpm2");
			
		} else if (obj == jpm3) {
			System.out.println("jpm3");
			
		} 
		if (e.getClickCount() == 2) {
			String abc = TreeData.toString().substring(0, TreeData.toString().indexOf(" "));
			ChatScreen cs = new ChatScreen(abc, userNo);
			System.out.println("send Messege");
		}
		
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
	}
	
	@Override
	public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
	}
	
	@Override
	public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
	}
	
	@Override
	public void popupMenuCanceled(PopupMenuEvent e) {
	}
	
	@Override
	public void run() {
		try {
			s = new Socket("192.168.0.95", 5000);
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s.getOutputStream())));
			
			String data = null;
			
			while (true) {
				data = br.readLine();
				jta2.append(data + "\n");
				String user_msg = "";
				String msg = "";
				user_msg = data.substring(0, data.indexOf(":"));
				msg = data.substring(data.indexOf(":") + 1, data.length());
				jta2.setText(msg);
				
				/*
				try {
					Class.forName(driver);
					DriverManager.getConnection(url, user, password);
					
					StringBuffer sql = new StringBuffer();
					sql.append(" SELECT ip_v, user_no_n, name_v FROM DEPT_USER WHERE user_no_n = ? ");
					pstmt = conn.prepareStatement(sql.toString());
					pstmt.setInt(1, userNo);
					rs = pstmt.executeQuery();
					rs.next();
					String str = rs.getString("name_v");
					String ip = InetAddress.getLocalHost().getHostAddress();
					//int user_ip = rs.getInt("ip_v");
					System.out.println(str);
					System.out.println(userNo);
					int sendResult = JOptionPane
							.showConfirmDialog(null, jpPop2, user + "님이 쪽지를 보냈습니다", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
					
					if (sendResult == JOptionPane.OK_OPTION) {
						System.out.println("OK jpPop2");
					} else {
						System.out.println("Cancelled");
					}
					
					// 수직 스크롤바 객체를 얻어옴
					JScrollBar jsb = jsp2.getVerticalScrollBar();
					// 수직 스크롤바 최대값을 while 돌때마다 설정
					int max = jsb.getMaximum();
					jsb.setValue(max);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				*/
			}
			
		} catch (UnknownHostException e) {
			System.out.println("알수없는 호스트");
		} catch (IOException e) {
			System.out.println("접속실패 다시 확인하세요");
		}
	} // run() End
	
	
} // JPanel01 End

