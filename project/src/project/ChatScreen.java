package project;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ChatScreen extends JFrame implements ActionListener, Runnable {
   JPanel jp1, jp2, jp3;
   JScrollPane jsp;
   public JTextArea jta;
   CardLayout layout;
   JTextField jtfTxt;
   JButton jbtnSend, jbtnFile;

   Socket s;

   BufferedReader br;
   PrintWriter pw;

   String driver = "oracle.jdbc.driver.OracleDriver";
   String url = "jdbc:oracle:thin:@192.168.0.95:1521:orcl";
   String user = "scott";
   String password = "tiger";

   Connection conn = null;
   PreparedStatement pstmt = null;
   ResultSet rs = null;
   
   String abc = "";
   int userNo = 0;
   String send_name = "";
   
   public ChatScreen(String str, int userNo) {
	   this.userNo = userNo;
      // 채팅창GUI
      abc = str;
      System.out.println(abc);
      System.out.println(userNo);
      layout = new CardLayout();
      setLayout(layout);
      setBounds(50, 50, 450, 350);
      layout = new CardLayout();
      jta = new JTextArea();
      jsp = new JScrollPane(jta, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      jtfTxt = new JTextField(20);
      jbtnSend = new JButton("Send");// 채팅GUI END
      jbtnFile = new JButton("File");

      jp2 = new JPanel();
      jp3 = new JPanel();

      jp2.setLayout(new BorderLayout());

      // jp3.add(jtfTxt);
      jp3.add(jbtnSend);
      jp3.add(jbtnFile);
      jp2.add(jsp, "Center");
      jp2.add(jp3, "South");

      Thread th = new Thread(this);
      th.start();
      jbtnSend.addActionListener(this);
      jbtnFile.addActionListener(this);
      add(jp2);
      setVisible(true);

   }

   /*public static void main(String[] args) {
      new ChatScreen(str);
   }*/

   @Override
   public void actionPerformed(ActionEvent e) {
      Object obj = e.getSource();
      String data = jta.getText();
      
      //send키를 눌렀을 떄
      if (obj == jbtnSend) {
         pw.println(data);
         pw.flush();
         jta.setText("");//jta clear

         String name = null;
         int deptno = 0;
         String ip = null;
         try {
            
            ip = InetAddress.getLocalHost().getHostAddress();
            System.out.println(ip);
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, password);
            StringBuffer sb = new StringBuffer();
            
            System.out.println(userNo);
            sb.append("\n SELECT name_v FROM dept_user WHERE user_no_n = ? ");
            pstmt = conn.prepareStatement(sb.toString());
            pstmt.setInt(1, userNo);
            rs = pstmt.executeQuery();
            if(rs.next()){
            	send_name = rs.getString("name_v");
            	rs.close();
            }
            sb.setLength(0);
            
            sb.append("SELECT NAME_V, IP_V  ");
            sb.append("FROM DEPT_USER ");
            sb.append("WHERE NAME_V = ? ");
            pstmt = conn.prepareStatement(sb.toString());
            pstmt.setString(1, abc );
            
            rs = pstmt.executeQuery();
         
            while (rs.next()) {

               name = rs.getString("NAME_V");//이름을 매개변수로 받은 이름으로 찾은 DEPT 내의 이름
               ip = rs.getString("IP_V");//RECEIVER의 IP
               
               //System.out.println(name);

            }
            
         } catch (SQLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         } catch (ClassNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         } catch (UnknownHostException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
         }
         try {

            StringBuffer sql = new StringBuffer();
            
            sql.append("INSERT INTO USER_MSG2(                              ");
            sql.append("                MSG_NO_N                            ");
            sql.append("              , IP_V                                ");
            sql.append("              , SENDER                              ");
            sql.append("              , RECEIVER                            ");
            sql.append("              , MSG                                 ");
            sql.append("              , READ_CHK_C                          ");
            sql.append("              , REG_D                               ");
            sql.append("                     )VALUES(                       ");
            sql.append("                             USER_MSG2_SEQ.NEXTVAL  "); // MSG_NO_N
            sql.append("                           , ?                      "); // IP_V
            sql.append("                           , ?                      "); // SENDER
            sql.append("                           , ?                      "); // RECEIVER
            sql.append("                           , ?                      "); // MSG
            sql.append("                           , '1'                    "); // READ_CHK_C
            sql.append("                           , SYSDATE                "); // REG_D
            sql.append("                           )                        "); 
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setString(1, ip);//RECEIVER
            pstmt.setString(2, send_name);//sender
            pstmt.setString(3, abc);// receiver
            pstmt.setString(4, data);//data,    db에 메세지 저장.
            rs = pstmt.executeQuery();
            

         } catch (SQLException e1) {

            

         

            // String str = jtfTxt.getText();
            // jta.setText("user : " + str);
         }
         // 현재 프레임 닫기
         dispose();
      } else if (obj == jbtnFile) {
         JFileChooser jfc = new JFileChooser();
         int choice = jfc.showOpenDialog(this);
         if (choice == JFileChooser.APPROVE_OPTION) {
            File file = jfc.getSelectedFile();
            String filePath = jfc.getSelectedFile().getPath();
            System.out.println(filePath + "  Sending");

         }

      }

   }

   @Override
   public void run() {
      // System.out.println("ip_v:"+ip_v+"\nJPanel01의 run");
      try {
         s = new Socket("192.168.0.95", 5000);
         br = new BufferedReader(new InputStreamReader(s.getInputStream()));

         pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
               s.getOutputStream())));

         String data = null;
         // while (true) {

         data = br.readLine();
         jta.append(data + "\n");
         // System.out.println("[msg]" + data);
         // String user = "";
         // String msg = "";
         // if (msg != null) {
         // user = data.substring(0, data.indexOf(":"));
         // msg = data.substring(data.indexOf(":") + 1, data.length());
         // jta.setText(msg);

         /*
          * int sendResult = JOptionPane .showConfirmDialog(null, jpPop2,
          * user + "님이 쪽지를 보냈습니다", JOptionPane.OK_CANCEL_OPTION,
          * JOptionPane.PLAIN_MESSAGE);
          * 
          * if (sendResult == JOptionPane.OK_OPTION) {
          * System.out.println("OK");
          * 
          * } else { System.out.println("Cancelled"); }}
          */

         // 수직 스크롤바 객체를 얻어옴
         JScrollBar jsb = jsp.getVerticalScrollBar();
         // 수직 스크롤바 최대값을 while 돌때마다 설정
         int max = jsb.getMaximum();
         jsb.setValue(max);
         // }

      } catch (UnknownHostException e) {
         System.out.println("알수없는 호스트");
      } catch (IOException e) {
         System.out.println("접속실패 다시 확인하세요");
      }
   } // run() End
}