package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// 20150416
// connection 객체를 생성해주는 객체
// SingleTon 객체로 만들기(new 할수없도록) 몇개를 만들어도 한번만 실행되도록
public class MakeConnection {

	private final static String url = "jdbc:oracle:thin:@192.168.0.80:1521:orcl";
	private final static String driver = "oracle.jdbc.driver.OracleDriver";
	private String user = "scott";
	private String password = "tiger";

	private Connection conn = null;
	private StringBuffer sql = new StringBuffer();
	private PreparedStatement pstmt = null;
	private ResultSet rs = null;

	private static MakeConnection mc = new MakeConnection();

	private MakeConnection(){
		try {
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			System.out.println("드라이버 로딩 실패");
		} catch (SQLException e) {
			System.out.println("DB 연결 실패");
		}
	}
	public Connection getConnection(){
		return conn;
	}

	// 외부에서 접근할수 있도록
	public static MakeConnection getInstance(){
		return mc;
	}
}
