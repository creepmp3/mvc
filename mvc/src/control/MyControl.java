/**
* Comment  : 
* @version : 1.0
* @author  : Kim Doyeon
* @date    : 2015. 5. 12.
*/
package control;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/myController.do")
public class MyControl extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }
    
    private void process(HttpServletRequest req, HttpServletResponse resp){
        try {
            req.setCharacterEncoding("UTF-8");
            String cmd = req.getParameter("cmd");
            String url = "";
            String msg = "";
            if(cmd==null || cmd.equals("hello")){
                msg = "Hello world! ^^ ";
                req.setAttribute("msg", msg);
                url = "hello2.jsp";
            }else if(cmd.equals("ip")){
                msg = "당신의 IP는?";
                String ip = req.getRemoteAddr();
                req.setAttribute("msg", msg);
                req.setAttribute("ip", ip);
                url = "ip.jsp";
            }else{
                msg = "안녕하세요 ^^ ";
                req.setAttribute("msg", msg);
                url = "hello.jsp";
            }
            RequestDispatcher dis = req.getRequestDispatcher(url);
            dis.forward(req, resp);
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch(ServletException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
       
    }
}
