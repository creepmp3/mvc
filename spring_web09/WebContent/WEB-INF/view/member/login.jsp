<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<jsp:include page="/WEB-INF/view/common/header.jsp"></jsp:include>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script type="text/javascript">
function validate(){
    if($("#id").val() == ""){
        alert("아이디를 입력하세요");
        $("#id").focus();
        return false;
    }
    if($("#pw").val() == ""){
        alert("암호를 입력하세요");
        $("#pw").focus();
        return false;
    }
    return true;
}
</script>
<style type="text/css">
table {
     border-collapse:collapse;
     vertical-align: middle;
}
table tr th, table tr td {
     border-collapse:collapse;
     border:1px solid black;
}
table tr th {
    text-align:right;
    background-color: #dedede;
}
table tr:last-child {
    text-align:right;
    border:0px solid black;
}
input[type='submit']{
    height:47px;
}
</style>
    
    <form action="loginOk" id="frm" method="post" onsubmit="return validate()">
        <table>
            <tr>
                <th>ID</th>
                <td><input type="text" name="id" id="id" placeholder="아이디" tabindex="1"/></td>
                <td rowspan="2">
                   <input type="submit" value="로그인" tabindex="3"/>
                </td>
            </tr>
            <tr>
                <th>PW</th>
                <td><input type="text" name="pw" id="pw" placeholder="비밀번호" tabindex="2"/></td>
            </tr>
        </table>
    </form>
    <br/>
    <span id="msg">${msg }</span>
</body>
</html>