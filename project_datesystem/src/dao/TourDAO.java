package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import db.MakeConnection;

public class TourDAO {
    Connection conn = null;
    PreparedStatement pstmt = null;
    StringBuffer sql = new StringBuffer();
    ResultSet rs = null;

    String tourUploadPath = "/project_datesystem/upload/tour/";

    public TourDAO() {
        conn = MakeConnection.getInstance().getConnection();
    }

    public int insertOne(TourVO vo) {
        int result = 0;
        sql.setLength(0);
        sql.append("\n INSERT INTO t_tour(tour_no_n                  ");
        sql.append("\n                  , tour_title_v               ");
        sql.append("\n                  , tour_content_v             ");
        sql.append("\n                  , tour_imgpath_1_v           ");
        sql.append("\n                  , tour_imgpath_2_v           ");
        sql.append("\n                  , tour_imgpath_3_v           ");
        sql.append("\n                  , tour_imgpath_4_v           ");
        sql.append("\n                  , tour_imgpath_5_v           ");
        sql.append("\n                  , tour_imgpath_6_v           ");
        sql.append("\n                  , tour_imgpath_7_v           ");
        sql.append("\n                  , tour_imgpath_8_v           ");
        sql.append("\n                  , tour_tel_v                 ");
        sql.append("\n                  , tour_open_time_v           ");
        sql.append("\n                  , tour_close_time_v          ");
        sql.append("\n                  , tour_price_v               ");
        sql.append("\n                  , tour_holiday_v             ");
        sql.append("\n                  , tour_type_v                ");
        sql.append("\n                  , tour_loc_v                 ");
        sql.append("\n                  , tour_use_yn_v              ");
        sql.append("\n                  , tour_reg_d                 ");
        sql.append("\n                   )VALUES(                    ");
        sql.append("\n                           tour_no_seq.nextval "); // tour_no_n
        sql.append("\n                         , ?                   "); // tour_title_v
        sql.append("\n                         , ?                   "); // tour_content_v
        sql.append("\n                         , ?                   "); // tour_imgpath_1_v
        sql.append("\n                         , ?                   "); // tour_imgpath_2_v
        sql.append("\n                         , ?                   "); // tour_imgpath_3_v
        sql.append("\n                         , ?                   "); // tour_imgpath_4_v
        sql.append("\n                         , ?                   "); // tour_imgpath_5_v
        sql.append("\n                         , ?                   "); // tour_imgpath_6_v
        sql.append("\n                         , ?                   "); // tour_imgpath_7_v
        sql.append("\n                         , ?                   "); // tour_imgpath_8_v
        sql.append("\n                         , ?                   "); // tour_tel_v
        sql.append("\n                         , ?                   "); // tour_open_time_v
        sql.append("\n                         , ?                   "); // tour_close_time_v
        sql.append("\n                         , ?                   "); // tour_price_v
        sql.append("\n                         , ?                   "); // tour_holiday_v
        sql.append("\n                         , ?                   "); // tour_type_v
        sql.append("\n                         , ?                   "); // tour_loc_v
        sql.append("\n                         , ?                   "); // tour_use_yn_v
        sql.append("\n                         , SYSDATE             "); // tour_reg_d
        sql.append("\n                           )                   ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            int i = 1;
            pstmt.setString(i++, vo.getTour_title_v());
            pstmt.setString(i++, vo.getTour_content_v());
            pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_1_v());
            pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_2_v());
            pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_3_v());
            pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_4_v());
            pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_5_v());
            pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_6_v());
            pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_7_v());
            pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_8_v());
            pstmt.setString(i++, vo.getTour_tel_v());
            pstmt.setString(i++, vo.getTour_open_time_v());
            pstmt.setString(i++, vo.getTour_close_time_v());
            pstmt.setString(i++, vo.getTour_price_v());
            pstmt.setString(i++, vo.getTour_holiday_v());
            pstmt.setString(i++, vo.getTour_type_v());
            pstmt.setString(i++, vo.getTour_loc_v());
            pstmt.setString(i++, vo.getTour_use_yn_v());

            result = pstmt.executeUpdate();

            if (result > 0) {
                System.out.println("등록성공");
            } else {
                System.out.println("등록실패");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<TourVO> selectAll(String searchOption, String searchKeyword){
        ArrayList<TourVO> list = new ArrayList<TourVO>();
        sql.setLength(0);
        sql.append("\n SELECT *                                                              ");
        sql.append("\n    FROM                                                               ");
        sql.append("\n        (SELECT ROW_NUMBER() OVER(ORDER BY tour_reg_d DESC) rnum       ");
        sql.append("\n              , COUNT(*) OVER()                             totalcount ");
        sql.append("\n              , tour_no_n                                              ");
        sql.append("\n              , tour_title_v                                           ");
        sql.append("\n              , tour_type_v                                            ");
        sql.append("\n              , tour_loc_v                                             ");
        sql.append("\n              , tour_reg_d                                             ");
        sql.append("\n              , tour_imgpath_1_v                                             ");
        sql.append("\n           FROM t_tour                                                 ");
        sql.append("\n          WHERE tour_use_yn_v = 'Y'                                    ");
        if(searchOption.equals("title")){
            sql.append("\n            AND tour_title_v like '%"+searchKeyword+"%'            ");
        }
        if(searchOption.equals("loc")){
            sql.append("\n            AND tour_loc_v like '%"+searchKeyword+"%'              ");
        }
        if(searchOption.equals("holiday")){
            sql.append("\n            AND tour_holiday_v like '%"+searchKeyword+"%'          ");
        }
        if(searchOption.equals("tel")){
            sql.append("\n            AND tour_tel_v like '%"+searchKeyword+"%'              ");
        }
        sql.append("\n        )                                                              ");
        //sql.append("\n WHERE rnum BETWEEN 1 AND 10                                         ");

        try {
            pstmt = conn.prepareStatement(sql.toString());

            rs = pstmt.executeQuery();

            while(rs.next()){
                int totalcount = rs.getInt("totalcount");
                int tour_no_n = rs.getInt("tour_no_n");
                String tour_title_v = rs.getString("tour_title_v");
                String tour_type_v = rs.getString("tour_type_v");
                String tour_loc_v = rs.getString("tour_loc_v");
                String tour_reg_d = rs.getString("tour_reg_d");
                String tour_imgpath_1_v = rs.getString("tour_imgpath_1_v");                

                TourVO vo = new TourVO(totalcount, tour_no_n, tour_title_v, tour_type_v,
                        tour_loc_v, tour_reg_d, tour_imgpath_1_v);
                list.add(vo);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


    public TourVO selectOne(int tour_no_n){
        TourVO vo = null;
        sql.setLength(0);
        sql.append("\n SELECT tour_title_v      ");
        sql.append("\n      , tour_content_v    ");
        sql.append("\n      , tour_imgpath_1_v  ");
        sql.append("\n      , tour_imgpath_2_v  ");
        sql.append("\n      , tour_imgpath_3_v  ");
        sql.append("\n      , tour_imgpath_4_v  ");
        sql.append("\n      , tour_imgpath_5_v  ");
        sql.append("\n      , tour_imgpath_6_v  ");
        sql.append("\n      , tour_imgpath_7_v  ");
        sql.append("\n      , tour_imgpath_8_v  ");
        sql.append("\n      , tour_tel_v        ");
        sql.append("\n      , tour_open_time_v  ");
        sql.append("\n      , tour_close_time_v ");
        sql.append("\n      , tour_price_v      ");
        sql.append("\n      , tour_holiday_v    ");
        sql.append("\n      , tour_type_v       ");
        sql.append("\n      , tour_loc_v        ");
        sql.append("\n      , tour_use_yn_v     ");
        sql.append("\n      , tour_reg_d        ");
        sql.append("\n   FROM t_tour            ");
        sql.append("\n  WHERE tour_no_n = ?     ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, tour_no_n);
            rs = pstmt.executeQuery();
            if(rs.next()){
                String tour_title_v = rs.getString("tour_title_v");
                String tour_content_v = rs.getString("tour_content_v");
                String tour_imgpath_1_v = rs.getString("tour_imgpath_1_v");
                String tour_imgpath_2_v = rs.getString("tour_imgpath_2_v");
                String tour_imgpath_3_v = rs.getString("tour_imgpath_3_v");
                String tour_imgpath_4_v = rs.getString("tour_imgpath_4_v");
                String tour_imgpath_5_v = rs.getString("tour_imgpath_5_v");
                String tour_imgpath_6_v = rs.getString("tour_imgpath_6_v");
                String tour_imgpath_7_v = rs.getString("tour_imgpath_7_v");
                String tour_imgpath_8_v = rs.getString("tour_imgpath_8_v");
                String tour_tel_v = rs.getString("tour_tel_v");
                String tour_open_time_v = rs.getString("tour_open_time_v");
                String tour_close_time_v = rs.getString("tour_close_time_v");
                String tour_price_v = rs.getString("tour_price_v");
                String tour_holiday_v = rs.getString("tour_holiday_v");
                String tour_type_v = rs.getString("tour_type_v");
                String tour_loc_v = rs.getString("tour_loc_v");
                String tour_use_yn_c = rs.getString("tour_use_yn_v");
                String tour_reg_d = rs.getString("tour_reg_d");

                vo = new TourVO(tour_no_n,tour_title_v, tour_content_v, tour_tel_v,
                        tour_open_time_v, tour_close_time_v, tour_price_v, tour_holiday_v,
                        tour_type_v, tour_loc_v, tour_use_yn_c, tour_reg_d,
                        tour_imgpath_1_v, tour_imgpath_2_v, tour_imgpath_3_v, tour_imgpath_4_v,
                        tour_imgpath_5_v, tour_imgpath_6_v, tour_imgpath_7_v, tour_imgpath_8_v);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }

        return vo;
    }

    public int updateOne(TourVO vo){
        int result = 0;
        sql.setLength(0);
        sql.append("\n UPDATE t_tour SET            ");
        sql.append("\n        tour_title_v = ?     ");
        sql.append("\n      , tour_content_v = ?    ");
        sql.append("\n      , tour_tel_v = ?        ");
        sql.append("\n      , tour_open_time_v = ?  ");
        sql.append("\n      , tour_close_time_v = ? ");
        sql.append("\n      , tour_price_v = ?      ");
        sql.append("\n      , tour_holiday_v = ?    ");
        sql.append("\n      , tour_type_v = ?       ");
        sql.append("\n      , tour_loc_v = ?        ");
        if(vo.getTour_imgpath_1_v()!=null){
            sql.append("\n      , tour_imgpath_1_v = ?  ");
        }
        if(vo.getTour_imgpath_2_v()!=null){
            sql.append("\n      , tour_imgpath_2_v = ?  ");
        }
        if(vo.getTour_imgpath_3_v()!=null){
            sql.append("\n      , tour_imgpath_3_v = ?  ");
        }
        if(vo.getTour_imgpath_4_v()!=null){
            sql.append("\n      , tour_imgpath_4_v = ?  ");
        }
        if(vo.getTour_imgpath_5_v()!=null){
            sql.append("\n      , tour_imgpath_5_v = ?  ");
        }
        if(vo.getTour_imgpath_6_v()!=null){
            sql.append("\n      , tour_imgpath_6_v = ?  ");
        }
        if(vo.getTour_imgpath_7_v()!=null){
            sql.append("\n      , tour_imgpath_7_v = ?  ");
        }
        if(vo.getTour_imgpath_8_v()!=null){
            sql.append("\n      , tour_imgpath_8_v = ?  ");
        }
        sql.append("\n  WHERE tour_no_n = ?         ");


        try {
            pstmt = conn.prepareStatement(sql.toString());

            int i = 1;
            pstmt.setString(i++, vo.getTour_title_v());
            pstmt.setString(i++, vo.getTour_content_v());
            pstmt.setString(i++, vo.getTour_tel_v());
            pstmt.setString(i++, vo.getTour_open_time_v());
            pstmt.setString(i++, vo.getTour_close_time_v());
            pstmt.setString(i++, vo.getTour_price_v());
            pstmt.setString(i++, vo.getTour_holiday_v());
            pstmt.setString(i++, vo.getTour_type_v());
            pstmt.setString(i++, vo.getTour_loc_v());
            if(vo.getTour_imgpath_1_v()!=null){
                pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_1_v());
            }
            if(vo.getTour_imgpath_2_v()!=null){
                pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_2_v());
            }
            if(vo.getTour_imgpath_3_v()!=null){
                pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_3_v());
            }
            if(vo.getTour_imgpath_4_v()!=null){
                pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_4_v());
            }
            if(vo.getTour_imgpath_5_v()!=null){
                pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_5_v());
            }
            if(vo.getTour_imgpath_6_v()!=null){
                pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_6_v());
            }
            if(vo.getTour_imgpath_7_v()!=null){
                pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_7_v());
            }
            if(vo.getTour_imgpath_8_v()!=null){
                pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_8_v());
            }
            pstmt.setInt(i++, vo.getTour_no_n());

            result = pstmt.executeUpdate();

        } catch(SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public int deleteOne(int tour_no_n){
        int result = 0;
        sql.setLength(0);
        sql.append("\n UPDATE t_tour SET      ");
        sql.append("\n       tour_use_yn_v = 'N'    ");
        sql.append("\n  WHERE tour_no_n = ?         ");

        try {
            pstmt = conn.prepareStatement(sql.toString());

            pstmt.setInt(1, tour_no_n);

            result = pstmt.executeUpdate();

            if(result>0){
                result = 0;
                pstmt.close();
                sql.setLength(0);
                sql.append("\n UPDATE t_tour_price SET      ");
                sql.append("\n       tour_use_yn_v = 'N'    ");
                sql.append("\n  WHERE tour_no_n = ?         ");

                pstmt = conn.prepareStatement(sql.toString());

                pstmt.setInt(1, tour_no_n);

                result = pstmt.executeUpdate();

            }


            if(result>0){
                System.out.println("삭제성공");
            }else{
                System.out.println("삭제실패");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}


/*package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import db.MakeConnection;


	* final update : 2015-04-24
	* writer : 김찬혁


public class TourDAO {
	Connection conn = null;
	StringBuffer sb = new StringBuffer();
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	
	public TourDAO(){
		conn = MakeConnection.getInstance().getConnection();
	}
	
	//selectAll()
	public ArrayList<TourVO> selectAll(){
		ArrayList<TourVO> list = new ArrayList<TourVO>();
		sb.setLength(0);
		sb.append("SELECT * FROM T_TOUR ");
		
		try {
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while(rs.next()){
				int tour_no_n = rs.getInt("tour_no_n");
				String tour_title_v = rs.getString("tour_title_v");
				String tour_content_v = rs.getString("tour_content_v");
				String tour_tel_v = rs.getString("tour_tel_v");
				String tour_open_time_v = rs.getString("tour_open_time_v");
				String tour_close_time_v = rs.getString("tour_close_time_v");
				String tour_price_v = rs.getString("tour_price_v");
				String tour_holiday_v = rs.getString("tour_holiday_v");
				String tour_type_v = rs.getString("tour_type_v");
				String tour_loc_v = rs.getString("tour_loc_v");
				String tour_use_yn_v = rs.getString("tour_use_yn_v");
				String tour_reg_d = rs.getString("tour_reg_d");
				String tour_imgpath_1_v = rs.getString("tour_imgpath_1_v");
				String tour_imgpath_2_v = rs.getString("tour_imgpath_2_v");
				String tour_imgpath_3_v = rs.getString("tour_imgpath_3_v");
				String tour_imgpath_4_v = rs.getString("tour_imgpath_4_v");
				String tour_imgpath_5_v = rs.getString("tour_imgpath_5_v");
				String tour_imgpath_6_v = rs.getString("tour_imgpath_6_v");
				String tour_imgpath_7_v = rs.getString("tour_imgpath_7_v");
				String tour_imgpath_8_v = rs.getString("tour_imgpath_8_v");
				
				TourVO vo = new TourVO(tour_no_n, tour_title_v, tour_content_v,
						tour_tel_v, tour_open_time_v,
						tour_close_time_v, tour_price_v,
						tour_holiday_v, tour_type_v, tour_loc_v,
						tour_use_yn_v, tour_reg_d, tour_imgpath_1_v,
						tour_imgpath_2_v, tour_imgpath_3_v,
						tour_imgpath_4_v, tour_imgpath_5_v,
						tour_imgpath_6_v, tour_imgpath_7_v,
						tour_imgpath_8_v);
				
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
		
	}//selectAll end
	
	
	public TourVO selectOne(int tour_no_n) {
		   TourVO vo = null;
		   sb.setLength(0);
	        sb.append("\n SELECT tour_title_v      ");
	        sb.append("\n      , tour_content_v    ");
	        sb.append("\n      , tour_imgpath_1_v  ");
	        sb.append("\n      , tour_imgpath_2_v  ");
	        sb.append("\n      , tour_imgpath_3_v  ");
	        sb.append("\n      , tour_imgpath_4_v  ");
	        sb.append("\n      , tour_imgpath_5_v  ");
	        sb.append("\n      , tour_imgpath_6_v  ");
	        sb.append("\n      , tour_imgpath_7_v  ");
	        sb.append("\n      , tour_imgpath_8_v  ");
	        sb.append("\n      , tour_tel_v        ");
	        sb.append("\n      , tour_open_time_v  ");
	        sb.append("\n      , tour_close_time_v ");
	        sb.append("\n      , tour_price_v      ");
	        sb.append("\n      , tour_holiday_v    ");
	        sb.append("\n      , tour_type_v       ");
	        sb.append("\n      , tour_loc_v        ");
	        sb.append("\n      , tour_use_yn_v     ");
	        sb.append("\n      , tour_reg_d        ");
	        sb.append("\n   FROM t_tour            ");
	        sb.append("\n  WHERE tour_no_n = ?     ");
	        
	        try {
				pstmt = conn.prepareStatement(sb.toString());
				pstmt.setInt(1, tour_no_n);
				rs = pstmt.executeQuery();
				
				if ( rs.next() ) {
					 String tour_title_v = rs.getString("tour_title_v");
		             String tour_content_v = rs.getString("tour_content_v");
	                 String tour_tel_v = rs.getString("tour_tel_v");
	                 String tour_open_time_v = rs.getString("tour_open_time_v");
	                 String tour_close_time_v = rs.getString("tour_close_time_v");
	                 String tour_price_v = rs.getString("tour_price_v");
	                 String tour_holiday_v = rs.getString("tour_holiday_v");
	                 String tour_type_v = rs.getString("tour_type_v");
	                 String tour_loc_v = rs.getString("tour_loc_v");
	                 String tour_use_yn_c = rs.getString("tour_use_yn_v");
	                 String tour_reg_d = rs.getString("tour_reg_d");
	                 String tour_imgpath_1_v = rs.getString("tour_imgpath_1_v");
	                 String tour_imgpath_2_v = rs.getString("tour_imgpath_2_v");
	                 String tour_imgpath_3_v = rs.getString("tour_imgpath_3_v");
	                 String tour_imgpath_4_v = rs.getString("tour_imgpath_4_v");
	                 String tour_imgpath_5_v = rs.getString("tour_imgpath_5_v");
	                 String tour_imgpath_6_v = rs.getString("tour_imgpath_6_v");
	                 String tour_imgpath_7_v = rs.getString("tour_imgpath_7_v");
	                 String tour_imgpath_8_v = rs.getString("tour_imgpath_8_v");
	                 
	                 vo = new TourVO(tour_no_n,tour_title_v, tour_content_v, 
	                		 tour_tel_v, tour_open_time_v, 
	                		 tour_close_time_v, tour_price_v, 
	                		 tour_holiday_v, tour_type_v, 
	                		 tour_loc_v, tour_use_yn_c, tour_reg_d,
	                		 tour_imgpath_1_v, tour_imgpath_2_v, 
	                		 tour_imgpath_3_v, tour_imgpath_4_v, 
	                		 tour_imgpath_5_v, tour_imgpath_6_v, 
	                		 tour_imgpath_7_v, tour_imgpath_8_v);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
	        return vo;
	   }
	
	
	//검색조회 searchResult()
	public TourVO searchResult(String title, String content){
		TourVO vo = null;
		sb.setLength(0);
		sb.append("SELECT * FROM T_TOUR ");
		sb.append("WHERE TOUR_TITLE_V = ? ");
		sb.append("OR TOUR_CONTENT_V = ? ");
		
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, title);
			pstmt.setString(2, content);
			rs = pstmt.executeQuery();
			while(rs.next()){
				int tour_no_n = rs.getInt("tour_no_n");
				String tour_title_v = rs.getString("tour_title_v");
				String tour_content_v = rs.getString("tour_content_v");
				String tour_tel_v = rs.getString("tour_tel_v");
				String tour_open_time_v = rs.getString("tour_open_time_v");
				String tour_close_time_v = rs.getString("tour_close_time_v");
				String tour_price_v = rs.getString("tour_price_v");
				String tour_holiday_v = rs.getString("tour_holiday_v");
				String tour_type_v = rs.getString("tour_type_v");
				String tour_loc_v = rs.getString("tour_loc_v");
				String tour_use_yn_v = rs.getString("tour_use_yn_v");
				String tour_reg_d = rs.getString("tour_reg_d");
				String tour_imgpath_1_v = rs.getString("tour_imgpath_1_v");
				String tour_imgpath_2_v = rs.getString("tour_imgpath_2_v");
				String tour_imgpath_3_v = rs.getString("tour_imgpath_3_v");
				String tour_imgpath_4_v = rs.getString("tour_imgpath_4_v");
				String tour_imgpath_5_v = rs.getString("tour_imgpath_5_v");
				String tour_imgpath_6_v = rs.getString("tour_imgpath_6_v");
				String tour_imgpath_7_v = rs.getString("tour_imgpath_7_v");
				String tour_imgpath_8_v = rs.getString("tour_imgpath_8_v");
				
				vo = new TourVO(tour_no_n, tour_title_v, tour_content_v,
						tour_tel_v, tour_open_time_v,
						tour_close_time_v, tour_price_v,
						tour_holiday_v, tour_type_v, tour_loc_v,
						tour_use_yn_v, tour_reg_d, tour_imgpath_1_v,
						tour_imgpath_2_v, tour_imgpath_3_v,
						tour_imgpath_4_v, tour_imgpath_5_v,
						tour_imgpath_6_v, tour_imgpath_7_v,
						tour_imgpath_8_v);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vo;
	}//searchResult() end
	
	public int insertOne(TourVO vo) {
    	int result = 0;
    	String tourUploadPath = "/project_datesystem/upload/tour";
    	sb.setLength(0);
    	sb.append("\n INSERT INTO t_tour VALUES(					");
    	sb.append("\n						tour_no_seq.nextval "); 
    	sb.append("\n						, ?							"); 
    	sb.append("\n						, ?							"); 
    	sb.append("\n						, ?							"); 
    	sb.append("\n						, ?							"); 
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");   
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");  
    	sb.append("\n						, SYSDATE				");  
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");  
    	sb.append("\n						, ?							");    	
    	sb.append("\n						)							");
    	
    	 try {
 	    	pstmt = conn.prepareStatement(sb.toString());
 	    	int i = 1;
 	    	pstmt.setString(i++, vo.getTour_title_v());
 	    	pstmt.setString(i++, vo.getTour_content_v());
 	    	pstmt.setString(i++, vo.getTour_tel_v());
 	    	pstmt.setString(i++, vo.getTour_open_time_v());
 	    	pstmt.setString(i++, vo.getTour_close_time_v());
 	    	pstmt.setString(i++, vo.getTour_price_v());
 	    	pstmt.setString(i++, vo.getTour_holiday_v());
 	    	pstmt.setString(i++, vo.getTour_type_v());
 	    	pstmt.setString(i++, vo.getTour_loc_v());
 	    	pstmt.setString(i++, vo.getTour_use_yn_v());
 	    	pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_1_v());
 	    	pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_2_v());
 	    	pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_3_v());
 	    	pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_4_v());
 	    	pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_5_v());
 	    	pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_6_v());
 	    	pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_7_v());
 	    	pstmt.setString(i++, tourUploadPath+vo.getTour_imgpath_8_v());
 	    	
 	    	result = pstmt.executeUpdate();
 	    	
 	    	if ( result > 0 ) {
 	    		System.out.println("등록성공");
 	    	} else {
 	    		System.out.println("등록실패");
 	    	}
 	    } catch ( SQLException e ) {
 	    	e.printStackTrace();
 	    }
    	 return result;
    }
	
	//게시물 수정 updateTour()
	public void updateTour(TourVO vo) {
		sb.setLength(0);
		sb.append("UPDATE T_TOUR ");
		sb.append("SET TOUR_TITLE_V = ?  , TOUR_CONTENT_V = ? ");
		sb.append("WHERE TOUR_NO_N = ? ");

		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, vo.getTour_title_v());
			pstmt.setString(2, vo.getTour_content_v());
			pstmt.setInt(3, vo.getTour_no_n());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}// updateOne end
	
	//게시물 삭제 deleteTour
	public void deleteOne(int tour_no_n) {
		sb.setLength(0);
		sb.append("DELETE T_TOUR ");
		sb.append("WHERE TOUR_NO_N = ? ");
		try {
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, tour_no_n);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}// deleteOne(int no) method end;
	
}
*/