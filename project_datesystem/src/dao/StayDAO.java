package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import db.MakeConnection;

public class StayDAO {
    Connection conn = null;
    PreparedStatement pstmt = null;
    StringBuffer sql = new StringBuffer();
    ResultSet rs = null;

    String stayUploadPath = "/project_datesystem/upload/stay/";

    public StayDAO() {
        conn = MakeConnection.getInstance().getConnection();
    }

    public int insertOne(StayVO vo) {
        int result = 0;
        sql.setLength(0);
        sql.append("\n INSERT INTO t_stay(stay_no_n                  ");
        sql.append("\n                  , stay_title_v               ");
        sql.append("\n                  , stay_info_title_v          ");
        sql.append("\n                  , stay_content_v             ");
        sql.append("\n                  , stay_imgpath_1_v           ");
        sql.append("\n                  , stay_imgpath_2_v           ");
        sql.append("\n                  , stay_imgpath_3_v           ");
        sql.append("\n                  , stay_imgpath_4_v           ");
        sql.append("\n                  , stay_imgpath_5_v           ");
        sql.append("\n                  , stay_imgpath_6_v           ");
        sql.append("\n                  , stay_imgpath_7_v           ");
        sql.append("\n                  , stay_imgpath_8_v           ");
        sql.append("\n                  , stay_tel_v                 ");
        sql.append("\n                  , stay_open_time_v           ");
        sql.append("\n                  , stay_close_time_v          ");
        sql.append("\n                  , stay_price_v               ");
        sql.append("\n                  , stay_holiday_v             ");
        sql.append("\n                  , stay_type_v                ");
        sql.append("\n                  , stay_loc_v                 ");
        sql.append("\n                  , stay_read_n                ");
        sql.append("\n                  , stay_recommend_n           ");
        sql.append("\n                  , stay_use_yn_v              ");
        sql.append("\n                  , stay_reg_d                 ");
        sql.append("\n                   )VALUES(                    ");
        sql.append("\n                           stay_no_seq.nextval "); // stay_no_n
        sql.append("\n                         , ?                   "); // stay_title_v
        sql.append("\n                         , ?                   "); // stay_info_title_v
        sql.append("\n                         , ?                   "); // stay_content_v
        sql.append("\n                         , ?                   "); // stay_imgpath_1_v
        sql.append("\n                         , ?                   "); // stay_imgpath_2_v
        sql.append("\n                         , ?                   "); // stay_imgpath_3_v
        sql.append("\n                         , ?                   "); // stay_imgpath_4_v
        sql.append("\n                         , ?                   "); // stay_imgpath_5_v
        sql.append("\n                         , ?                   "); // stay_imgpath_6_v
        sql.append("\n                         , ?                   "); // stay_imgpath_7_v
        sql.append("\n                         , ?                   "); // stay_imgpath_8_v
        sql.append("\n                         , ?                   "); // stay_tel_v
        sql.append("\n                         , ?                   "); // stay_open_time_v
        sql.append("\n                         , ?                   "); // stay_close_time_v
        sql.append("\n                         , ?                   "); // stay_price_v
        sql.append("\n                         , ?                   "); // stay_holiday_v
        sql.append("\n                         , ?                   "); // stay_type_v
        sql.append("\n                         , ?                   "); // stay_loc_v
        sql.append("\n                         , 0                   "); // stay_read_n
        sql.append("\n                         , 0                   "); // stay_recommend_n
        sql.append("\n                         , 'Y'                 "); // stay_use_yn_v
        sql.append("\n                         , SYSDATE             "); // stay_reg_d
        sql.append("\n                           )                   ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            int i = 1;
            pstmt.setString(i++, vo.getStay_title_v());
            pstmt.setString(i++, vo.getStay_info_title_v());
            pstmt.setString(i++, vo.getStay_content_v());
            pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_1_v());
            pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_2_v());
            pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_3_v());
            pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_4_v());
            pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_5_v());
            pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_6_v());
            pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_7_v());
            pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_8_v());
            pstmt.setString(i++, vo.getStay_tel_v());
            pstmt.setString(i++, vo.getStay_open_time_v());
            pstmt.setString(i++, vo.getStay_close_time_v());
            pstmt.setString(i++, vo.getStay_price_v());
            pstmt.setString(i++, vo.getStay_holiday_v());
            pstmt.setString(i++, vo.getStay_type_v());
            pstmt.setString(i++, vo.getStay_loc_v());
            // pstmt.setString(i++, vo.getStay_use_yn_v());

            result = pstmt.executeUpdate();

            if (result > 0) {
                System.out.println("등록성공");
            } else {
                System.out.println("등록실패");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<StayVO> selectAll(int startNum, int endNum, String searchOption, String searchKeyword,
            String orderType) {
        ArrayList<StayVO> list = new ArrayList<StayVO>();
        sql.setLength(0);
        sql.append("\n SELECT *                                                              ");
        sql.append("\n    FROM                                                               ");
        sql.append("\n        (SELECT ROW_NUMBER() OVER(ORDER BY stay_reg_d DESC) rnum       ");
        sql.append("\n              , COUNT(*) OVER()                             totalcount ");
        sql.append("\n              , stay_no_n                                              ");
        sql.append("\n              , stay_title_v                                           ");
        sql.append("\n              , stay_info_title_v                                      ");
        sql.append("\n              , stay_imgpath_1_v                                       ");
        sql.append("\n              , stay_type_v                                            ");
        sql.append("\n              , stay_tel_v                                             ");
        sql.append("\n              , stay_loc_v                                             ");
        sql.append("\n              , NVL(stay_read_n, 0)                         stay_read_n ");
        sql.append("\n              , NVL(stay_recommend_n, 0)                    stay_recommend_n ");
        sql.append("\n              , stay_reg_d                                             ");
        sql.append("\n           FROM t_stay                                                 ");
        sql.append("\n          WHERE stay_use_yn_v = 'Y'                                    ");
        if (searchOption.equals("title")) {
            sql.append("\n            AND stay_title_v like '%" + searchKeyword + "%'        ");
        }
        if (searchOption.equals("loc")) {
            sql.append("\n            AND stay_loc_v like '%" + searchKeyword + "%'          ");
        }
        if (searchOption.equals("holiday")) {
            sql.append("\n            AND stay_holiday_v like '%" + searchKeyword + "%'      ");
        }
        if (searchOption.equals("tel")) {
            sql.append("\n            AND stay_tel_v like '%" + searchKeyword + "%'          ");
        }
        sql.append("\n        )                                                              ");
        sql.append("\n WHERE rnum BETWEEN " + startNum + " AND " + endNum);
        if (!orderType.equals("")) {
            if (orderType.equals("date")) {
                sql.append("\n ORDER BY stay_reg_d DESC                                      ");
            } else if (orderType.equals("recommend")) {
                sql.append("\n ORDER BY stay_recommend_n DESC                                ");
            } else if (orderType.equals("readn")) {
                sql.append("\n ORDER BY stay_read_n DESC                                     ");
            }
        }

        try {
            pstmt = conn.prepareStatement(sql.toString());

            rs = pstmt.executeQuery();

            while(rs.next()) {
                int totalcount = rs.getInt("totalcount");
                int stay_no_n = rs.getInt("stay_no_n");
                String stay_title_v = rs.getString("stay_title_v");
                String stay_info_title_v = rs.getString("stay_info_title_v");
                String stay_imgpath_1_v = rs.getString("stay_imgpath_1_v");
                String stay_type_v = rs.getString("stay_type_v");
                String stay_tel_v = rs.getString("stay_tel_v");
                String stay_loc_v = rs.getString("stay_loc_v");
                int stay_read_n = rs.getInt("stay_read_n");
                int stay_recommend_n = rs.getInt("stay_recommend_n");
                String stay_reg_d = rs.getString("stay_reg_d");

                StayVO vo = new StayVO(totalcount, stay_no_n, stay_title_v, stay_info_title_v,
                        stay_imgpath_1_v, stay_type_v, stay_tel_v, stay_loc_v, stay_read_n, stay_recommend_n,
                        stay_reg_d);
                list.add(vo);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public StayVO selectOne(int stay_no_n) {
        StayVO vo = null;
        int result = 0;
        try {
            sql.setLength(0);
            sql.append("\n UPDATE T_STAY                                ");
            sql.append("\n    SET STAY_READ_N = NVL(STAY_READ_N, 0) + 1 ");
            sql.append("\n  WHERE STAY_NO_N = ?                         ");

            pstmt = conn.prepareStatement(sql.toString());

            pstmt.setInt(1, stay_no_n);
            result = pstmt.executeUpdate();
            pstmt.close();

            sql.setLength(0);
            sql.append("\n SELECT stay_title_v      ");
            sql.append("\n      , stay_info_title_v ");
            sql.append("\n      , stay_content_v    ");
            sql.append("\n      , stay_imgpath_1_v  ");
            sql.append("\n      , stay_imgpath_2_v  ");
            sql.append("\n      , stay_imgpath_3_v  ");
            sql.append("\n      , stay_imgpath_4_v  ");
            sql.append("\n      , stay_imgpath_5_v  ");
            sql.append("\n      , stay_imgpath_6_v  ");
            sql.append("\n      , stay_imgpath_7_v  ");
            sql.append("\n      , stay_imgpath_8_v  ");
            sql.append("\n      , stay_tel_v        ");
            sql.append("\n      , stay_open_time_v  ");
            sql.append("\n      , stay_close_time_v ");
            sql.append("\n      , stay_price_v      ");
            sql.append("\n      , stay_holiday_v    ");
            sql.append("\n      , stay_type_v       ");
            sql.append("\n      , stay_loc_v        ");
            sql.append("\n      , stay_read_n       ");
            sql.append("\n      , stay_recommend_n  ");
            sql.append("\n      , stay_use_yn_v     ");
            sql.append("\n      , stay_reg_d        ");
            sql.append("\n   FROM t_stay            ");
            sql.append("\n  WHERE stay_no_n = ?     ");

            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, stay_no_n);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                String stay_title_v = rs.getString("stay_title_v");
                String stay_info_title_v = rs.getString("stay_info_title_v");
                String stay_content_v = rs.getString("stay_content_v");
                String stay_imgpath_1_v = rs.getString("stay_imgpath_1_v");
                String stay_imgpath_2_v = rs.getString("stay_imgpath_2_v");
                String stay_imgpath_3_v = rs.getString("stay_imgpath_3_v");
                String stay_imgpath_4_v = rs.getString("stay_imgpath_4_v");
                String stay_imgpath_5_v = rs.getString("stay_imgpath_5_v");
                String stay_imgpath_6_v = rs.getString("stay_imgpath_6_v");
                String stay_imgpath_7_v = rs.getString("stay_imgpath_7_v");
                String stay_imgpath_8_v = rs.getString("stay_imgpath_8_v");
                String stay_tel_v = rs.getString("stay_tel_v");
                String stay_open_time_v = rs.getString("stay_open_time_v");
                String stay_close_time_v = rs.getString("stay_close_time_v");
                String stay_price_v = rs.getString("stay_price_v");
                String stay_holiday_v = rs.getString("stay_holiday_v");
                String stay_type_v = rs.getString("stay_type_v");
                String stay_loc_v = rs.getString("stay_loc_v");
                int stay_read_n = rs.getInt("stay_read_n");
                int stay_recommend_n = rs.getInt("stay_recommend_n");
                String stay_use_yn_v = rs.getString("stay_use_yn_v");
                String stay_reg_d = rs.getString("stay_reg_d");

                vo = new StayVO(stay_no_n, stay_title_v, stay_info_title_v, stay_content_v, stay_tel_v,
                        stay_open_time_v, stay_close_time_v, stay_price_v, stay_holiday_v, stay_type_v,
                        stay_loc_v, stay_read_n, stay_recommend_n, stay_use_yn_v, stay_reg_d,
                        stay_imgpath_1_v, stay_imgpath_2_v, stay_imgpath_3_v, stay_imgpath_4_v,
                        stay_imgpath_5_v, stay_imgpath_6_v, stay_imgpath_7_v, stay_imgpath_8_v);
            }

        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            /*
             * try { if(rs != null){rs.close();} if(pstmt != null){pstmt.close();} //if(conn != null){conn.close();} } catch (SQLException e) {
             * e.printStackTrace(); }
             */
        }
        return vo;
    }

    public int updateReadCnt(int stay_no_n) {
        int result = 0;
        sql.setLength(0);
        sql.append("\n UPDATE T_STAY SET                            ");
        sql.append("\n        STAY_READ_N = NVL(STAY_READ_N, 0) + 1 ");
        sql.append("\n  WHERE STAY_NO_N = ?                         ");

        try {
            pstmt = conn.prepareStatement(sql.toString());

            pstmt.setInt(1, stay_no_n);
            result = pstmt.executeUpdate();

        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public int updateOne(StayVO vo) {
        int result = 0;
        sql.setLength(0);
        sql.append("\n UPDATE t_stay SET            ");
        sql.append("\n        stay_title_v = ?      ");
        sql.append("\n      , stay_info_title_v = ? ");
        sql.append("\n      , stay_content_v = ?    ");
        sql.append("\n      , stay_tel_v = ?        ");
        sql.append("\n      , stay_open_time_v = ?  ");
        sql.append("\n      , stay_close_time_v = ? ");
        sql.append("\n      , stay_price_v = ?      ");
        sql.append("\n      , stay_holiday_v = ?    ");
        sql.append("\n      , stay_type_v = ?       ");
        sql.append("\n      , stay_loc_v = ?        ");
        if (vo.getStay_imgpath_1_v() != null) {
            sql.append("\n      , stay_imgpath_1_v = ?  ");
        }
        if (vo.getStay_imgpath_2_v() != null) {
            sql.append("\n      , stay_imgpath_2_v = ?  ");
        }
        if (vo.getStay_imgpath_3_v() != null) {
            sql.append("\n      , stay_imgpath_3_v = ?  ");
        }
        if (vo.getStay_imgpath_4_v() != null) {
            sql.append("\n      , stay_imgpath_4_v = ?  ");
        }
        if (vo.getStay_imgpath_5_v() != null) {
            sql.append("\n      , stay_imgpath_5_v = ?  ");
        }
        if (vo.getStay_imgpath_6_v() != null) {
            sql.append("\n      , stay_imgpath_6_v = ?  ");
        }
        if (vo.getStay_imgpath_7_v() != null) {
            sql.append("\n      , stay_imgpath_7_v = ?  ");
        }
        if (vo.getStay_imgpath_8_v() != null) {
            sql.append("\n      , stay_imgpath_8_v = ?  ");
        }
        sql.append("\n  WHERE stay_no_n = ?         ");

        try {
            pstmt = conn.prepareStatement(sql.toString());

            int i = 1;
            pstmt.setString(i++, vo.getStay_title_v());
            pstmt.setString(i++, vo.getStay_info_title_v());
            pstmt.setString(i++, vo.getStay_content_v());
            pstmt.setString(i++, vo.getStay_tel_v());
            pstmt.setString(i++, vo.getStay_open_time_v());
            pstmt.setString(i++, vo.getStay_close_time_v());
            pstmt.setString(i++, vo.getStay_price_v());
            pstmt.setString(i++, vo.getStay_holiday_v());
            pstmt.setString(i++, vo.getStay_type_v());
            pstmt.setString(i++, vo.getStay_loc_v());
            if (vo.getStay_imgpath_1_v() != null) {
                pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_1_v());
            }
            if (vo.getStay_imgpath_2_v() != null) {
                pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_2_v());
            }
            if (vo.getStay_imgpath_3_v() != null) {
                pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_3_v());
            }
            if (vo.getStay_imgpath_4_v() != null) {
                pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_4_v());
            }
            if (vo.getStay_imgpath_5_v() != null) {
                pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_5_v());
            }
            if (vo.getStay_imgpath_6_v() != null) {
                pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_6_v());
            }
            if (vo.getStay_imgpath_7_v() != null) {
                pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_7_v());
            }
            if (vo.getStay_imgpath_8_v() != null) {
                pstmt.setString(i++, stayUploadPath + vo.getStay_imgpath_8_v());
            }
            pstmt.setInt(i++, vo.getStay_no_n());

            result = pstmt.executeUpdate();

        } catch(SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public int deleteOne(int stay_no_n) {
        int result = 0;
        sql.setLength(0);
        sql.append("\n UPDATE t_stay SET      ");
        sql.append("\n       stay_use_yn_v = 'N'    ");
        sql.append("\n  WHERE stay_no_n = ?         ");

        try {
            pstmt = conn.prepareStatement(sql.toString());

            pstmt.setInt(1, stay_no_n);

            result = pstmt.executeUpdate();

            if (result > 0) {
                result = 0;
                pstmt.close();
                sql.setLength(0);
                sql.append("\n UPDATE t_stay_price SET      ");
                sql.append("\n       stay_use_yn_v = 'N'    ");
                sql.append("\n  WHERE stay_no_n = ?         ");

                pstmt = conn.prepareStatement(sql.toString());

                pstmt.setInt(1, stay_no_n);

                result = pstmt.executeUpdate();

            }

            if (result > 0) {
                System.out.println("삭제성공");
            } else {
                System.out.println("삭제실패");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
