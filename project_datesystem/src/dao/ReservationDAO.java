/**
 * FileName : ReservationDAO.java
 * Comment  :
 * @version : 1.0
 * @author  : Kim Doyeon
 * @date    : 2015. 4. 30.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import db.MakeConnection;

/**
 * @author user
 *
 */
public class ReservationDAO {
    Connection conn = null;
    PreparedStatement pstmt = null;
    StringBuffer sql = new StringBuffer();
    ResultSet rs = null;

    public ReservationDAO() {
        conn = MakeConnection.getInstance().getConnection();
    }

    public int insertOne(ReservationVO vo) {
        int result = 0;
        sql.setLength(0);
        sql.append("\n INSERT INTO t_reservation(reservation_no_n             ");
        sql.append("\n                         , stay_no_n                    ");
        sql.append("\n                         , stay_price_no_n              ");
        sql.append("\n                         , user_tel_v                   ");
        sql.append("\n                         , reservation_date_v           ");
        sql.append("\n                         , reservation_time_v           ");
        sql.append("\n                         , reservation_hour_v           ");
        sql.append("\n                         , reservation_minute_v         ");
        sql.append("\n                         , reservation_type_v           ");
        sql.append("\n                         , total_pay_n                  ");
        sql.append("\n                         , reservation_regd_d           ");
        sql.append("\n                         , reservation_use_yn_v         ");
        sql.append("\n                         , pay_yn_v                     ");
        sql.append("\n                         , confirm_yn_v                 ");
        sql.append("\n                          )VALUES(                      ");
        sql.append("\n                           reservation_no_n_seq.nextval "); // reservation_no_n
        sql.append("\n                         , ?                            "); // stay_no_n
        sql.append("\n                         , ?                            "); // stay_price_no_n
        sql.append("\n                         , ?                            "); // user_tel_v
        sql.append("\n                         , ?                            "); // reservation_date_v
        sql.append("\n                         , ?                            "); // reservation_time_v
        sql.append("\n                         , ?                            "); // reservation_hour_v
        sql.append("\n                         , ?                            "); // reservation_minute_v
        sql.append("\n                         , ?                            "); // reservation_type_v
        sql.append("\n                         , ?                            "); // total_pay_n
        sql.append("\n                         , SYSDATE                      "); // reservation_regd_d
        sql.append("\n                         , 'Y'                          "); // reservation_use_yn_v
        sql.append("\n                         , ?                            "); // pay_yn_v
        sql.append("\n                         , ?                            "); // confirm_yn_v
        sql.append("\n                           )                            ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            int i = 1;
            pstmt.setInt(i++, vo.getStay_no_n());
            pstmt.setInt(i++, vo.getStay_price_no_n());
            pstmt.setString(i++, vo.getUser_tel_v());
            pstmt.setString(i++, vo.getReservation_date_v());
            pstmt.setString(i++, vo.getReservation_time_v());
            pstmt.setString(i++, vo.getReservation_hour_v());
            pstmt.setString(i++, vo.getReservation_minute_v());
            pstmt.setString(i++, vo.getReservation_type_v());
            pstmt.setInt(i++, vo.getTotal_pay_n());
            pstmt.setString(i++, vo.getPay_yn_v());
            pstmt.setString(i++, vo.getConfirm_yn_v());

            result = pstmt.executeUpdate();

            if (result > 0) {
                System.out.println("등록성공");
            } else {
                System.out.println("등록실패");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<ReservationVO> selectAll(int startNum, int endNum, String startDate, String lastDate,
            String searchOption, String searchKeyword) {
        ArrayList<ReservationVO> list = new ArrayList<ReservationVO>();
        sql.setLength(0);
        sql.append("\n SELECT *                                                               ");
        sql.append("\n   FROM                                                                 ");
        sql.append("\n       (SELECT ROW_NUMBER() OVER(ORDER BY reservation_regd_d DESC) rnum ");
        sql.append("\n             , COUNT(*) OVER()                               totalcount ");
        sql.append("\n             , r.reservation_no_n                                       ");
        sql.append("\n             , r.stay_no_n                                              ");
        sql.append("\n             , r.stay_price_no_n                                        ");
        sql.append("\n             , r.user_tel_v                                             ");
        sql.append("\n             , r.reservation_date_v                                     ");
        sql.append("\n             , r.reservation_time_v                                     ");
        sql.append("\n             , r.reservation_regd_d                                     ");
        sql.append("\n             , r.reservation_type_v                                     ");
        sql.append("\n             , r.pay_yn_v                                               ");
        sql.append("\n             , r.confirm_yn_v                                           ");
        sql.append("\n             , s.stay_title_v                                           ");
        sql.append("\n             , p.stay_price_nm_v                                        ");
        sql.append("\n             , CASE reservation_type_v                                  ");
        sql.append("\n                   WHEN 'sw' THEN p.stay_s_w_v                          ");
        sql.append("\n                   WHEN 'sh' THEN p.stay_s_h_v                          ");
        sql.append("\n                   WHEN 'dw' THEN p.stay_d_w_v                          ");
        sql.append("\n                   WHEN 'dh' THEN p.stay_d_h_v                          ");
        sql.append("\n               END stay_price_n                                         ");
        sql.append("\n          FROM t_reservation r, t_stay s, t_stay_price p                ");
        sql.append("\n         WHERE r.reservation_use_yn_v = 'Y'                             ");
        sql.append("\n           AND r.stay_no_n = s.stay_no_n                                ");
        sql.append("\n           AND r.stay_price_no_n = p.stay_price_no_n                    ");
        if (!startDate.equals("") && !lastDate.equals("")) {
            // sql.append("\n     AND r.reservation_date_v BETWEEN '" + startDate + "' AND '" + lastDate + "' ");
        }
        /*
         * if(searchOption.equals("loc")) { sql.append("\n            AND stay_loc_v like '%" + searchKeyword + "%'          "); }
         * if(searchOption.equals("holiday")) { sql.append("\n            AND stay_holiday_v like '%" + searchKeyword + "%'      "); }
         * if(searchOption.equals("tel")) { sql.append("\n            AND stay_tel_v like '%" + searchKeyword + "%'          "); }
         */
        sql.append("\n        )                                                              ");
        if (startDate.equals("") && lastDate.equals("")) {
            // sql.append("\n WHERE rnum BETWEEN " + startNum + " AND " + endNum);
        }
        if (startNum > 0 && endNum > 0) {
            sql.append("\n WHERE rnum BETWEEN " + startNum + " AND " + endNum);
        }
        sql.append("\n ORDER BY reservation_date_v DESC, reservation_time_v DESC              ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            System.out.println(sql.toString());
            rs = pstmt.executeQuery();

            while(rs.next()) {
                int totalcount = rs.getInt("totalcount");
                int reservation_no_n = rs.getInt("reservation_no_n");
                int stay_no_n = rs.getInt("stay_no_n");
                int stay_price_no_n = rs.getInt("stay_price_no_n");
                String user_tel_v = rs.getString("user_tel_v");
                String reservation_date_v = rs.getString("reservation_date_v");
                String reservation_time_v = rs.getString("reservation_time_v");
                String reservation_regd_d = rs.getString("reservation_regd_d");
                String reservation_type_v = rs.getString("reservation_type_v");
                String pay_yn_v = rs.getString("pay_yn_v");
                String confirm_yn_v = rs.getString("confirm_yn_v");
                String stay_title_v = rs.getString("stay_title_v");
                String stay_price_nm_v = rs.getString("stay_price_nm_v");
                int stay_price_n = rs.getInt("stay_price_n");

                ReservationVO vo = new ReservationVO(totalcount, reservation_no_n, stay_no_n,
                        stay_price_no_n, user_tel_v, reservation_date_v, reservation_time_v,
                        reservation_type_v, reservation_regd_d, pay_yn_v, confirm_yn_v, stay_title_v,
                        stay_price_nm_v, stay_price_n);
                list.add(vo);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ReservationVO selectOne(int reservation_no_n) {
        ReservationVO vo = null;
        sql.setLength(0);
        sql.append("\n SELECT r.stay_no_n                               ");
        sql.append("\n      , r.stay_price_no_n                         ");
        sql.append("\n      , r.user_tel_v                              ");
        sql.append("\n      , r.reservation_date_v                      ");
        sql.append("\n      , r.reservation_time_v                      ");
        sql.append("\n      , r.reservation_hour_v                      ");
        sql.append("\n      , r.reservation_minute_v                    ");
        sql.append("\n      , r.reservation_regd_d                      ");
        sql.append("\n      , r.reservation_type_v                      ");
        sql.append("\n      , r.reservation_use_yn_v                    ");
        sql.append("\n      , r.pay_yn_v                                ");
        sql.append("\n      , r.confirm_yn_v                            ");
        sql.append("\n      , s.stay_title_v                            ");
        sql.append("\n      , p.stay_price_nm_v                         ");
        sql.append("\n      , CASE reservation_type_v                   ");
        sql.append("\n            WHEN 'sw' THEN p.stay_s_w_v           ");
        sql.append("\n            WHEN 'sh' THEN p.stay_s_h_v           ");
        sql.append("\n            WHEN 'dw' THEN p.stay_d_w_v           ");
        sql.append("\n            WHEN 'dh' THEN p.stay_d_h_v           ");
        sql.append("\n         END stay_price_n                         ");
        sql.append("\n   FROM t_reservation r, t_stay s, t_stay_price p ");
        sql.append("\n  WHERE r.stay_no_n = s.stay_no_n                 ");
        sql.append("\n    AND r.stay_price_no_n = p.stay_price_no_n     ");
        sql.append("\n    AND reservation_no_n = ?                      ");
        try {
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, reservation_no_n);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                int stay_no_n = rs.getInt("stay_no_n");
                int stay_price_no_n = rs.getInt("stay_price_no_n");
                String user_tel_v = rs.getString("user_tel_v");
                String reservation_date_v = rs.getString("reservation_date_v");
                String reservation_time_v = rs.getString("reservation_time_v");
                String reservation_hour_v = rs.getString("reservation_hour_v");
                String reservation_minute_v = rs.getString("reservation_minute_v");
                String reservation_type_v = rs.getString("reservation_type_v");
                String reservation_regd_d = rs.getString("reservation_regd_d");
                String reservation_use_yn_v = rs.getString("reservation_use_yn_v");
                String pay_yn_v = rs.getString("pay_yn_v");
                String confirm_yn_v = rs.getString("confirm_yn_v");
                String stay_title_v = rs.getString("stay_title_v");
                String stay_price_nm_v = rs.getString("stay_price_nm_v");
                int stay_price_n = rs.getInt("stay_price_n");

                vo = new ReservationVO(stay_no_n, stay_price_no_n, user_tel_v, reservation_date_v,
                        reservation_time_v, reservation_hour_v, reservation_minute_v, reservation_type_v,
                        reservation_regd_d, reservation_use_yn_v, pay_yn_v, confirm_yn_v, stay_title_v,
                        stay_price_nm_v, stay_price_n);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }

        return vo;
    }
}
