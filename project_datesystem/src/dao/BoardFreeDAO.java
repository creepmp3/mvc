/**
 * FileName : BoardManDAO.java
 * Comment  :
 * @version : 1.0
 * @author  : Kim Doyeon
 * @date    : 2015. 5. 6.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import db.MakeConnection;

public class BoardFreeDAO {
    Connection conn = null;
    PreparedStatement pstmt = null;
    StringBuffer sql = new StringBuffer();
    ResultSet rs = null;

    public BoardFreeDAO() {
        conn = MakeConnection.getInstance().getConnection();
    }

    public int insertOne(BoardFreeVO vo) {
        int result = 0;
        sql.setLength(0);
        sql.append("\n INSERT INTO T_BOARD_FREE(SEQ_N                         ");
        sql.append("\n                     , MEMBER_NO_N                   ");
        sql.append("\n                     , TITLE_V                       ");
        sql.append("\n                     , CONTENT_V                     ");
        sql.append("\n                     , READ_CNT_N                    ");
        sql.append("\n                     , REF_N                         ");
        sql.append("\n                     , USE_YN_V                      ");
        sql.append("\n                     , REG_D                         ");
        sql.append("\n                       )VALUES(                      ");
        sql.append("\n                               T_BOARD_FREE_SEQ.NEXTVAL "); // SEQ_N
        sql.append("\n                             , ?                     "); // MEMBER_NO_N
        sql.append("\n                             , ?                     "); // TITLE_V
        sql.append("\n                             , ?                     "); // CONTENT_V
        sql.append("\n                             , 0                     "); // READ_CNT_N
        sql.append("\n                             , ?                     "); // REF_N
        sql.append("\n                             , 'Y'                   "); // USE_YN_V
        sql.append("\n                             , SYSDATE               "); // USE_YN_V
        sql.append("\n                               )                     ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            int i = 1;
            pstmt.setInt(i++, vo.getMember_no_n());
            pstmt.setString(i++, vo.getTitle_v());
            pstmt.setString(i++, vo.getContent_v());
            pstmt.setInt(i++, vo.getRef_n());

            result = pstmt.executeUpdate();

            if (result > 0) {
                System.out.println("등록성공");
            } else {
                System.out.println("등록실패");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ArrayList<BoardFreeVO> selectAll(int startNum, int endNum, String searchOption, String searchKeyword, String orderType) {
        ArrayList<BoardFreeVO> list = new ArrayList<BoardFreeVO>();
        sql.setLength(0);
        sql.append("\n SELECT COUNT(*)OVER()                       totalcount ");
        sql.append("\n      , LEVEL                                lv         ");
        sql.append("\n      , b.MEMBER_NO_N                                   ");
        sql.append("\n      , m.MEMBER_ID_V                                   ");
        sql.append("\n      , m.MEMBER_NICK_V                                 ");
        sql.append("\n      , b.SEQ_N                                         ");
        sql.append("\n      , b.REF_N                                         ");
        // sql.append("\n      , LPAD(b.TITLE_V, LEVEL+5+LEVEL, '　') title_v    ");
        sql.append("\n      , b.TITLE_V                                         ");
        sql.append("\n      , b.CONTENT_V                                     ");
        sql.append("\n      , b.READ_CNT_N                                    ");
        sql.append("\n      , TO_CHAR(b.REG_D, 'YYYY-MM-DD') REG_D            ");
        sql.append("\n   FROM T_BOARD_FREE b, T_MEMBER m                         ");
        sql.append("\n  WHERE b.MEMBER_NO_N = m.MEMBER_NO_N                   ");
        sql.append("\n    AND b.USE_YN_V = 'Y'                                ");
        sql.append("\n    AND ROWNUM BETWEEN " + startNum + " AND " + endNum);
        sql.append("\n   START WITH REF_N = 0                                 ");
        sql.append("\n CONNECT BY REF_N = PRIOR SEQ_N                         ");
        /*
         * if (searchOption.equals("title")) { sql.append("\n            AND stay_title_v like '%" + searchKeyword + "%'        "); } if
         * (searchOption.equals("loc")) { sql.append("\n            AND stay_loc_v like '%" + searchKeyword + "%'          "); } if
         * (searchOption.equals("holiday")) { sql.append("\n            AND stay_holiday_v like '%" + searchKeyword + "%'      "); } if
         * (searchOption.equals("tel")) { sql.append("\n            AND stay_tel_v like '%" + searchKeyword + "%'          "); }
         */

        if (!orderType.equals("")) {
            if (orderType.equals("date")) {
                sql.append("\n ORDER BY REG_D DESC                                     ");
            } else if (orderType.equals("readcnt")) {
                sql.append("\n ORDER BY READ_CNT_N DESC                                ");
            }
        }

        try {
            pstmt = conn.prepareStatement(sql.toString());
            System.out.println(sql.toString());
            rs = pstmt.executeQuery();

            while(rs.next()) {
                int totalcount = rs.getInt("totalcount");
                int lv = rs.getInt("lv");
                int seq_n = rs.getInt("seq_n");
                int member_no_n = rs.getInt("member_no_n");
                String member_id_v = rs.getString("member_id_v");
                String member_nick_v = rs.getString("member_nick_v");
                String title_v = rs.getString("title_v");
                int read_cnt_n = rs.getInt("read_cnt_n");
                int ref_n = rs.getInt("ref_n");
                String reg_d = rs.getString("reg_d");

                BoardFreeVO vo = new BoardFreeVO(totalcount, lv, seq_n, member_no_n, member_id_v, member_nick_v, title_v, read_cnt_n, ref_n, reg_d);
                list.add(vo);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public BoardFreeVO selectOne(int seq_n) {
        BoardFreeVO vo = null;

        try {
            sql.setLength(0);
            sql.append("\n UPDATE T_BOARD_FREE                          ");
            sql.append("\n    SET READ_CNT_N = NVL(READ_CNT_N, 0) +1 ");
            sql.append("\n  WHERE SEQ_N = ?                          ");

            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, seq_n);
            pstmt.executeUpdate();

            pstmt.close();

            sql.setLength(0);
            sql.append("\n SELECT b.MEMBER_NO_N                 ");
            sql.append("\n      , m.MEMBER_ID_V                 ");
            sql.append("\n      , m.MEMBER_NICK_V               ");
            sql.append("\n      , b.SEQ_N                       ");
            sql.append("\n      , b.TITLE_V                     ");
            sql.append("\n      , b.CONTENT_V                   ");
            sql.append("\n      , b.READ_CNT_N                  ");
            sql.append("\n      , b.REF_N                       ");
            sql.append("\n      , b.REG_D                       ");
            sql.append("\n   FROM T_BOARD_FREE b, T_MEMBER m       ");
            sql.append("\n  WHERE b.MEMBER_NO_N = m.MEMBER_NO_N ");
            sql.append("\n    AND b.SEQ_N = ?                   ");

            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, seq_n);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                int member_no_n = rs.getInt("member_no_n");
                String member_id_v = rs.getString("member_id_v");
                String member_nick_v = rs.getString("member_nick_v");
                String title_v = rs.getString("title_v");
                String content_v = rs.getString("content_v");
                int read_cnt_n = rs.getInt("read_cnt_n");
                int ref_n = rs.getInt("ref_n");
                String reg_d = rs.getString("reg_d");

                vo = new BoardFreeVO(member_no_n, member_id_v, member_nick_v, seq_n, title_v, content_v, read_cnt_n, ref_n, reg_d);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return vo;
    }

    public int updateOne(BoardFreeVO vo) {
        int result = 0;

        try {
            sql.setLength(0);
            sql.append("\n UPDATE T_BOARD_FREE     ");
            sql.append("\n    SET TITLE_V = ?   ");
            sql.append("\n      , CONTENT_V = ? ");
            sql.append("\n  WHERE SEQ_N = ?     ");

            pstmt = conn.prepareStatement(sql.toString());
            int idx = 1;
            pstmt.setString(idx++, vo.getTitle_v());
            pstmt.setString(idx++, vo.getContent_v());
            pstmt.setInt(idx++, vo.getSeq_n());
            result = pstmt.executeUpdate();

        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int deleteOne(int seq_n) {
        int result = 0;

        try {
            sql.setLength(0);
            sql.append("\n UPDATE T_BOARD_FREE      ");
            sql.append("\n    SET USE_YN_V = 'N' ");
            sql.append("\n  WHERE SEQ_N = ?      ");

            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, seq_n);
            result = pstmt.executeUpdate();

        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
