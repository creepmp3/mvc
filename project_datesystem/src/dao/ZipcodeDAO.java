package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import db.MakeConnection;

public class ZipcodeDAO {
	
	int result = 0;
	Connection conn = null;
	PreparedStatement pstmt = null;
	StringBuffer sql = new StringBuffer();
	ResultSet rs = null;
	
	public ZipcodeDAO(){
		conn = MakeConnection.getInstance().getConnection();
	}
	
	public ArrayList<ZipcodeVO> selectAll(int startNum, int endNum, String searchKeyword){
		ArrayList<ZipcodeVO> list = new ArrayList<ZipcodeVO>();
		sql.setLength(0);
		sql.append("\n SELECT *                                                                                                                             ");
		sql.append("\n    FROM                                                                                                                                ");
		sql.append("\n              (SELECT ROW_NUMBER() OVER(ORDER BY zipcode_v ASC)     rnum       ");
		sql.append("\n                             , COUNT(*) OVER()                                                             totalcount ");
		sql.append("\n                             , zipcode_v                                                                                              ");
		sql.append("\n                             , index_v                                                                                                 ");
		sql.append("\n                             , si_v                                                                                                        ");
		sql.append("\n                             , gu_v                                                                                                       ");
		sql.append("\n                             , dong_v                                                                                                   ");
		sql.append("\n                             , ri_v                                                                                                         ");
		sql.append("\n                             , doser_v                                                                                                   ");
		sql.append("\n                             , bunji_v                                                                                                   ");
		sql.append("\n                             , building_v                                                                                             ");
		sql.append("\n                             , update_v                                                                                                ");
		sql.append("\n                             , addr_v                                                                                                    ");
		sql.append("\n                  FROM t_zipcode                                                                                                ");
		sql.append("\n               WHERE addr_v like ?                                                                                            ");
		sql.append("\n                )                                                                                                                               ");
		sql.append("\n WHERE rnum BETWEEN "+startNum+" AND "+ endNum                                            );
		
		try {
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setString(1, "%"+searchKeyword+"%");
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				int totalcount = rs.getInt("totalcount");
				String zipcode_v = rs.getString("zipcode_v");
				String index_v = rs.getString("index_v");
				String si_v = rs.getString("si_v");
				String gu_v = rs.getString("gu_v");
				String dong_v = rs.getString("dong_v");
				String ri_v = rs.getString("ri_v");
				String doser_v = rs.getString("doser_v");
				String bunji_v = rs.getString("bunji_v");
				String building_v = rs.getString("building_v");
				String update_v = rs.getString("update_v");
				String addr_v = rs.getString("addr_v");
				
				ZipcodeVO vo = new ZipcodeVO(totalcount, zipcode_v, index_v, si_v, gu_v,
						dong_v, ri_v, doser_v, bunji_v, building_v, update_v, addr_v);
				list.add(vo);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	
}
