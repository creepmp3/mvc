<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="../css/main/slider_style.css"/>
<link rel="stylesheet" href="../js/main/bxslider-4-development/dist/jquery.bxslider2.css" />
<script src="../js/main/bxslider-4-development/dist/jquery.bxslider2.js"></script>
<script src="../js/main/slide_banner.js"></script>
<script type="text/javascript" src="../js/main/slide.js"></script>
<script type="text/javascript">
$(function(){
	
	/* ********************** 날씨 ********************** */
	expires = new Date();					
	expires.setTime(expires.getTime() + (1000 * 3600 * 24 * 365));	
	set_cookie('cityid', 1835848, expires);	
	$.getJSON("http://api.openweathermap.org/data/2.5/forecast/daily?callback=?&id=1835848&units=metric&cnt=14", showForecastDaily).error(errorHandler);
	/* ********************** 날씨 ********************** */
	
	/* 이미지슬라이드 */
	imageSilide(2000);
	
	
});

</script>
</head>
<body>
	<div id="slide" class="sliderWrapper">
			<!-- <img src = "../images/slide/top/1.jpg"> -->
			<ul class="bxslider">
				<li class="slide"><img src="http://contents.visitseoul.net/file_save/r_mainindex/2015/04/03/20150403104135.jpg" alt="" /></li>
				<li class="slide"><img src="http://contents.visitseoul.net/file_save/r_mainindex/2015/04/16/20150416150454.jpg" alt="" /></li>
				<li class="slide"><img src="http://contents.visitseoul.net/file_save/r_mainindex/2015/04/21/20150421104747.jpg" alt="" /></li>
				<li class="slide"><img src="http://contents.visitseoul.net/file_save/r_mainindex/2015/04/02/20150402113833.jpg" alt="" /></li>
				<li class="slide"><img src="http://contents.visitseoul.net/file_save/r_mainindex/2015/04/14/20150414171939.jpg" alt="" /></li>
			</ul>
		</div>

</body>
</html>