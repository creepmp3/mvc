<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="util.StringUtil" %>
<% 
	String session_member_id_v = StringUtil.nvl((String)session.getAttribute("member_id_v"), "");
	String session_member_nick_v = StringUtil.nvl((String)session.getAttribute("member_nick_v"), "");
	String session_member_type_v = StringUtil.nvl((String)session.getAttribute("member_type_v"), "");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<!-- jQuery  -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<!-- main Layout -->
<link rel="stylesheet" href="/project_datesystem/css/main/main_layout.css" />
<link rel="stylesheet" href="/project_datesystem/css/main/top_menu.css"/>
<!-- 메인 이미지 슬라이드 -->


<!-- 날씨 -->
<script type="text/javascript" src="/project_datesystem/js/main/weather.js"></script>
<link rel="stylesheet" href="/project_datesystem/css/main/weather.css" />

<script type="text/javascript">

function logout(){
	if(confirm("로그아웃 하시겠습니까?")){
		location.href = "../login/logout.jsp";
	}
}
</script>
</head>

<body>
	<div id="content">
		<div id="top">
			
			<div id="member">
				<%
				if(session_member_id_v.equals("")){
				%>
					<a href="../login/login.jsp">로그인</a> | <a href="../user/member/member_insert.jsp">회원가입</a>
				<%}else{ %>
					<a href="#"><%=session_member_nick_v %></a>님 | <a href="#" id="logoutBtn" onclick="logout()">로그아웃</a>
				<%} %>
				<%
				if(session_member_type_v.equals("S")){
				%>
				  | <a href="../admin/admin_list.jsp">Admin</a><br /> <br /> <input type="text" name="search" id="" />
				<%
				}
				%>
			</div>
			<div id="menubar">
				<ul>
					<li class = "menu">주변 먹거리</li>
					<li class = "menu"><a href="../user/tour/tour_list.jsp">데이트장소</a></li>
					<li class = "menu"><a href="../user/stay/stay_list.jsp">주변 숙박시설</a></li>
					<li class = "menu">오늘의 영화</li>
					<li class = "menu">우리들 이야기</li>
					<li class = "menu">고 객 센 터</li>
				</ul>
			</div>

		</div>
		
			<jsp:include page="../contents/slider.jsp"></jsp:include>
		<div id="content_text">
			<div id="text1">오늘 어디갈까?</div>

		</div>
		<div id = "middle">
		<jsp:include page="sub_contents.jsp"></jsp:include>
		</div>
		<jsp:include page="sub_menu.jsp"></jsp:include>

			<div id="footer">회사 주소, 전화번호, 이 메일 등등</div>

		</div>
	
</body>
</html>