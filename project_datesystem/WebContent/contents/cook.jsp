<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="dao.StayDAO, dao.StayVO, util.StringUtil"%>
<%@ include file="../../common/sub_header.jsp"%>
<%
	/* ******************** paging ******************** */
	int totalcount = 0;
	int pageCount = 0;
	int pageNo = Integer.parseInt(StringUtil.nvl(
			request.getParameter("pageNo"), "1"));
	int pageSize = 10;
	int startNum = ((pageNo - 1) * pageSize) + 1;
	int endNum = ((pageNo - 1) * pageSize) + pageSize;

	int searchStart = 0;
	int searchEnd = 0;
	searchStart = (pageNo - 5 <= 0) ? 1 : pageNo - 5;
	searchEnd = (pageNo - 5 <= 0) ? 10 : pageNo + 5;

	String searchOption = StringUtil.nvl(
			request.getParameter("searchOption"), "");
	String searchKeyword = StringUtil.nvl(
			request.getParameter("searchKeyword"), "");
	/* ******************** paging ******************** */
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" href="../css/content_css/tour_content_style.css" />

</head>
<body>

	<div id="tour_content">
		<div id="tour_content_top">추천 맛집</div>
		<div id="tour_content_down">
			<ul>
				<li>신규</li>
				<li>추천</li>
				<li>인기</li>
			</ul>



			<form action="<%=PAGENAME%>">

				<input type="submit" id="search_btn" value="검색" /> <input
					type="text" name="searchKeyword" id="search_text"
					value="<%=searchKeyword%>" /> <select name="searchOption"
					class="searchOption">
					<option value="titlev"
						<%=searchOption.equals("titlev") ? "selected='selected'"
					: ""%>>이름</option>
					<option value="loc"
						<%=searchOption.equals("loc") ? "selected='selected'" : ""%>>지역</option>
					<option value="type"
						<%=searchOption.equals("type") ? "selected='selected'" : ""%>>타입</option>
					<option value="holiday"
						<%=searchOption.equals("holiday") ? "selected='selected'"
					: ""%>>휴일</option>
					<option value="tel"
						<%=searchOption.equals("tel") ? "selected='selected'" : ""%>>전화번호</option>
				</select>
			</form>
		</div>
	</div>


	<form id="cook">
		<div class="food1">
			<ul>
				<%
					StayDAO dao = new StayDAO();
					ArrayList<StayVO> list = dao.selectAll(startNum, endNum,
							searchOption, searchKeyword);

					if (list.size() > 0) {
						for (StayVO vo : list) {
							totalcount = vo.getTotalcount();

							if (totalcount % pageSize == 0) {
								pageCount = totalcount / pageSize;
							} else {
								pageCount = totalcount / pageSize + 1;
							}

							String type = "";
							if (vo.getStay_type_v().equals("H")) {
								type = "호텔";
							} else if (vo.getStay_type_v().equals("M")) {
								type = "모텔";
							} else if (vo.getStay_type_v().equals("G")) {
								type = "게스트하우스";
							}
				%>
				<li>
					<dl>
						<dt>
							<a href="stay_view.jsp?stay_no_n=<%=vo.getStay_no_n()%>"><img
								src="<%=vo.getStay_imgpath_1_v()%>"></a>
						</dt>
						<dd class="title"><%=vo.getStay_title_v()%></dd>
						<dd><%=vo.getStay_loc_v()%></dd>

					</dl>
				</li>
				<%
					}
					} else {
				%>
				<li>데이터가 없습니다</li>
				<%
					}
				%>
			</ul>
		</div>
	</form>

</body>
</html>