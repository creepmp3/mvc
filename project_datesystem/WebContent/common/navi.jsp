<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<link href="style.css" rel="stylesheet" type="text/css" />
<title>test</title>
<script src="jquery-1.6.3.js"></script>
<script src="jquery-1.6.3.min.js"></script>
<script>
 $(document).ready(function(){
 $(".top ul").css({display:"none"});
  
 $(".top>li").hover(
  function(){ //마우스 오버
   $(this).find("ul").css({display:"block",height:"0%"})
   .stop()
   .animate({height:"100%"},"fast");
   $(this).find("a:first").addClass("on");
  },
  function(){ //마우스 리브(아웃)
   $(this).find("ul").stop()
   .animate({height:"0%"},"fast", function(){
    $(this).css({display:"none"});
   });
   $(this).find("a:first").removeClass("on");
  }
 );
 $(".top>li>a").on("focus",function(){
  console.log($(this).parent().index());
  $(".top ul").css({display:"none"});
  $(".top>li>a").removeClass("on");
  $(this).next("ul").css({display:"block"});
  $(this).addClass("on");
 });
 $(".menum_ns>li>a").on("focus", function(){
  $(".menum_ns>li>a").removeClass("on");
  $(this).addClass("on");
 });
 //$(".release").on("focus",function(){ //release를 안만들고 처리할려면.
 $(".menu a:last").on("focus",function(){
  $(".top ul").css({display:"none"});
  $(".top>li>a").removeClass("on");
  $(".menum_ns>li>a").removeClass("on");
 });
 });
</script>
</head>
<body>
<div id="wrap">
  <div id="header">
    <div class="topmn">
      <div class="logo"></div>
      <div class="menu">
        <ul class="top">
          <li class="link">
           <a href="#">네비게이션1</a>
            <ul class="menum_ns">
                <li class='link1'><a href="#">네비게이션 조회</a></li>
                <li class='link1'><a href="#">네비게이션 입력</a></li>
            </ul>
    </li>
          <li class="link">
           <a href="#">네비게이션2</a>
            <ul class="menum_ns">
                <li class='link1'><a href="#">네비게이션 조회</a></li>
                <li class='link1'><a href="#">네비게이션 입력</a></li>
            </ul>
          </li>
          <li class="link"><a href="#">네비게이션3</a></li>
          <li class="link"><a href="#">네비게이션4</a></li>
        </ul>
      </div>
      <div class="admin">홍길동님 반갑습니다.</div>
    </div>
  </div>
  <!-- // header -->
  <div id="main_wrap">
    <div id="center_area01">
      <div class="graph">
        <h2><img src="images/main_bullet01.png" alt="그래프 표시" align="absmiddle" />그래프 표시</h2>
        <div class="bg">
          <div class="num">
            <table width="20" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr class="tt">
                <td height="31" valign="top" class="ft_11 ft_bold">100</td>
              </tr>
              <!-- //그래프 이미지 -->
              <tr valign="top" class="tt">
                <td height="31" class="ft_11 ft_bold">80</td>
              </tr>
              <tr valign="top" class="tt">
                <td height="31" class="ft_11 ft_bold">60</td>
              </tr>
              <tr valign="top" class="tt">
                <td height="31" class="ft_11 ft_bold">40</td>
              </tr>
              <tr valign="top" class="tt">
                <td height="31" class="ft_11 ft_bold">20</td>
              </tr>
              <tr valign="top" class="tt">
                <td height="31" class="ft_11 ft_bold">0</td>
              </tr>
            </table>
            <!-- //그래프 숫자 -->
          </div>
          <div class="graphview">
            <table width="337" border="0" cellpadding="0" cellspacing="0">
              <colgroup>
              <col width="15%" />
              <col width="15%" />
              <col width="15%" />
              <col width="15%" />
              <col width="15%" />
              <col width="15%" />
              </colgroup>
              <tr class="tt">
                <td height="144" valign="bottom" class="line"><img src="images/graph_line01.png" alt="그래프1" width="21" height="100%" /></td>
                <td valign="bottom" class="line"><img src="images/graph_line02.png" alt="그래프2" width="21" height="80%" /></td>
                <td valign="bottom" class="line"><img src="images/graph_line03.png" alt="그래프3" width="21" height="60%" /></td>
                <td valign="bottom" class="line"><img src="images/graph_line04.png" alt="그래프4" width="21" height="40%" /></td>
                <td valign="bottom" class="line"><img src="images/graph_line05.png" alt="그래프5" width="21" height="20%" /></td>
                <td valign="bottom" class="line"><img src="images/graph_line06.png" alt="그래프6" width="21" height="10%" /></td>
              </tr>
              <!-- //그래프 이미지 -->
              <tr class="tt">
                <td height="30">그래프<br />
                  1</td>
                <td>그래프<br />
                  2</td>
                <td>그래프<br />
                  3</td>
                <td>그래프<br />
                  4</td>
                <td>그래프<br />
                  5</td>
                <td>그래프<br />
                  6</td>
              </tr>
              <!-- //그래프 텍스트 -->
            </table>
          </div>
        </div>
      </div>
      <div class="img"></div>
    </div>
    <div id="center_area02">
      <div class="research">
        <div>
          <h2>과제 표시</h2>
          <div class="bg01">
            <table width="280" border="0" cellspacing="0" cellpadding="0">
              <colgroup>
              <col width="" />
              <col width="28%" />
              <col width="15%" />
              <col width="22%" />
              </colgroup>
              <tr>
                <td class="tt"><img src="images/bullet01.png" alt="" align="absmiddle" /> 진행중인 과제</td>
                <td class="orangt ft_bold txtR pdr10">100건</td>
                <td class="orangt ft_bold txtR pdr10">&nbsp;</td>
                <td class="orangt ft_bold txtR pdr10">&nbsp;</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- //과제 현황 -->
        <div class="research">
          <h2>과제 현황</h2>
          <div class="bg02">
            <table width="280" border="0" cellpadding="0" cellspacing="0">
              <colgroup>
              <col width="" />
              <col width="28%" />
              <col width="15%" />
              <col width="22%" />
              </colgroup>
              <tr>
                <td class="tt"><img src="images/bullet01.png" alt="" align="absmiddle" /> 등록</td>
                <td class="orangt ft_bold txtR pdr10">100건</td>
                <td class="tt"><img src="images/bullet01.png" alt="" align="absmiddle" /> 등록 </td>
                <td class="orangt ft_bold txtR">100건</td>
              </tr>
              <tr>
                <td class="tt"><img src="images/bullet01.png" alt="" align="absmiddle" />  등록</td>
                <td class="orangt ft_bold txtR pdr10">100건</td>
                <td class="tt"><img src="images/bullet01.png" alt="" align="absmiddle" /> 등록 </td>
                <td class="orangt ft_bold txtR">100건</td>
              </tr>
              <tr>
                <td class="tt"><img src="images/bullet01.png" alt="" align="absmiddle" /> 등록</td>
                <td class="orangt ft_bold txtR pdr10">100건</td>
                <td class="tt"><img src="images/bullet01.png" alt="" align="absmiddle" /> 등록 </td>
                <td class="orangt ft_bold txtR">100건</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- //과제 현황 -->
      </div>
      <div class="notice">
        <div id="main_contents">
          <div class="left">
            <div class="l-cont">
              <div class="notice01">
                <div class="list" style="display:block;">
                  <ul>
                    <li><a href="#" title="">공지사항입니다공지사항입...</a><em class="date">2014.10.13</em></li>
                    <li><a href="#" title="">공지사항입니다공지사항입...</a><em class="date">2014.10.13</em></li>
                    <li><a href="#" title="">공지사항입니다공지사항입...</a><em class="date">2014.10.13</em></li>
                    <li><a href="#" title="">공지사항입니다공지사항입...</a><em class="date">2014.10.13</em></li>
                    <li><a href="#" title="">공지사항입니다공지사항입...</a><em class="date">2014.10.13</em></li>
                  </ul>
                  <a href="#" class="more"><img src="images/btn_more.png" alt="새소식 더보기" border="0" /></a> </div>
              </div>
              <span class="notice_title">공지사항</span> </div>
          </div>
        </div>
        <!-- //공지사항 -->
      </div>
    </div>
  </div>
  <div id="footer">
    <div class="area"></div>
  </div>
</div>
</body>
</html>