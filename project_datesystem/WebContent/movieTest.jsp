<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="./css/main/movie_style.css" />
<link rel="stylesheet" href="./css/main/main_layout.css" />
<link rel="stylesheet" href="./css/main/top_menu.css" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" src="./js/main/movie_js_style.js"></script>
<title>Insert title here</title>

</head>

<body>
	<!-- 첫 번째 영화 정보 -->
	<div id="content">
		<div id="movie_body">
			<jsp:include page="/main/menubar.jsp"></jsp:include>

			<div id="title_movie">
				<div id="title_text">이달의 개봉작</div>
				<ul>
					<li id="image1"></li>
					<li id="image2"></li>
					<li id="image3"></li>
					<li id="image4"></li>
					<li id="image5"></li>
				</ul>
			</div>

		
			<table id="detail_movie">
				<tr height="50px">
					<td>
						<h2>영화 상세</h2>
					</td>
				</tr>
				<tr height="auto">
					<td align="center">
						<table id="inner_detail_movie" >
							<tr>
								<td style="padding: 0px; margin: 0px;" rowspan="6" width="200px" height="300px"><div id="image1_1" align="center" ></div></td>
								<td><div id="desc1_1"></div></td>
							</tr>
							<tr>
								<td><div id="desc1_2"></div></td>
							</tr>
							<tr>
								<td><div id="desc1_3"></div></td>
							</tr>
							<tr>
								<td><div id="desc1_4"></div></td>
							</tr>
							<tr>
								<td><div id="desc1_5"></div></td>
							</tr>
							<tr>
								<td><div id="desc1_6"></div></td>
							</tr>
							<tr>
								<td colspan="2"><div class = "st" id = "story1"></div></td>
							</tr>
						</table>
					
					
					
					
					
					</td>
				</tr>
			</table>
		
			<!-- div id="detail_movie">
				<div id="movie_title">
					<h1>영화 상세</h1>
				</div>
				<div id="image1_1"></div>
				<div id="movie_content">
					<div id="desc1_1"></div>
					<div id="desc1_2"></div>
					<div id="desc1_3"></div>
					<div id="desc1_4"></div>
					<div id="desc1_5"></div>
					<div id="desc1_6"></div>
					<div id = "s_text"><h3>줄거리</h3></div>
					<div class = "st" id = "story1"></div>
				</div>
			</div> -->
		</div>
	</div>



</body>
</html>