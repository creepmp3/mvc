<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.StayDAO, dao.StayVO, dao.StayPriceDAO, dao.StayPriceVO" %>
<%
	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"),"0"));
	String stayPath = "file:///C:/eclipse/web_workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/project_datesystem/upload/stay/";

%>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(function(){
	$("#listBtn").on("click", function(){
		location.href = "stay_list.jsp";
	});
	
	$("#deleteBtn").on("click", function(){
		if(confirm("삭제하시겠습니까?")){
			$("#frm").attr("action", "stay_deleteOk.jsp").submit();
		}
	});
	
	$("#priceAddBtn").on("click", function(){
		$("#frm").attr("action", "stay_price_insert.jsp").submit();
	});
	
	$("#modifyBtn").on("click", function(){
		$("#frm").attr("action", "stay_modify.jsp").submit();
	});
	
	$("a[name='reservationBtn']").each(function(i, elem){
		$(elem).on("click", function(){
			var stay_title_v = $(".titlev").text();
			var stay_price_nm_v = $(elem).parent().children("a")[0].text;
			var stay_no_n = $(elem).attr("stay");
			var stay_price_no_n = $(elem).attr("price");
			var imgpath1v = $(".imgPath_1v").attr("src");
			var stay_type_v = $(".stay_type_v").text();
			var stayType = $(":radio[name='stayType']:checked").parent().children("span").text();
			var price = $(":radio[name='reservationType']:checked").parent().children("span").text();
			var priceType = $(":radio[name='reservationType']:checked").attr("class");
			if(priceType=="w"){
				priceType="평일"
			}else if(priceType=="h"){
				priceType = "주말";
			}
			
			opener.document.getElementById("stay_title_v").value = stay_title_v;
			opener.document.getElementById("stay_no_n").value = stay_no_n;
			opener.document.getElementById("stay_price_no_n").value = stay_price_no_n;
			opener.document.getElementById("stay_price_v").value = price;
			opener.document.getElementById("stay_title_v").value = stay_title_v;
			opener.document.getElementById("stay_price_nm_v").value = stay_price_nm_v;
			opener.document.getElementById("stay_type_v").value = stay_type_v;
			opener.document.getElementById("stay_price_type_v").value = stayType;
			opener.document.getElementById("stay_price_type2_v").value = priceType;
			opener.document.getElementById("stay_imgpath_1_v").src = imgpath1v;
			window.close();
		});
		
	});
	
});
</script>
<style type="text/css">
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.btnArea {
	clear:both;
	margin-top:10px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<div class="view">
		<h2>관리자페이지</h2>
			<%
				StayDAO dao = new StayDAO();
				StayVO vo = dao.selectOne(stay_no_n);
				
				if(vo != null){
			%>
			<form id="frm">
				<input type="hidden" name="stay_no_n" id="stay_no_n" value="<%=stay_no_n %>" />
				<table>
					<tbody>
						<tr>
							<th>제목</th>
							<td><span class="titlev"><%=vo.getStay_title_v() %></span></td>
						</tr>
						<tr>
							<th>부제목</th>
							<td><%=vo.getStay_info_title_v() %></td>
						</tr>
						<tr>
							<th>내용</th>
							<td><%=vo.getStay_content_v() %></td>
						</tr>
						<tr>
							<th>전화번호</th>
							<td><%=vo.getStay_tel_v() %></td>
						</tr>
						<tr>
							<th>OPEN TIME</th>
							<td><%=vo.getStay_open_time_v() %></td>
						</tr>
						<tr>
							<th>CLOSE TIME</th>
							<td><%=vo.getStay_close_time_v() %></td>
						</tr>
						<tr>
							<th>가격</th>
							<td>
								<div>
									<table>
										<tr>
											<th rowspan="2">구분</th>
											<th colspan="2"><span>대실</span> <input type="radio" name="stayType" /></th>
											<th colspan="2"><span>숙박</span> <input type="radio" name="stayType" /></th>
										</tr>
										<tr>
											<td>월~금</td>
											<td>토~일</td>
											<td>월~금</td>
											<td>토~일</td>
										</tr>
										<%
										
										StayPriceDAO p_dao = new StayPriceDAO();
										ArrayList<StayPriceVO> p_list = p_dao.selectAll(stay_no_n);
										
										for(StayPriceVO pvo : p_list){
										%>
										<tr>
											<th><a href="stay_price_modify.jsp?stay_no_n=<%=stay_no_n%>&stay_price_no_n=<%=pvo.getStay_price_no_n() %>"><%=pvo.getStay_price_nm_v() %></a> <a href="#" class="btn" name="reservationBtn" price="<%=pvo.getStay_price_no_n() %>" stay="<%=stay_no_n%>">예약선택</a></th>
											<td><span><%=pvo.getStay_d_w_v() %></span> <input type="radio" class="w" name="reservationType" /></td>
											<td><span><%=pvo.getStay_d_h_v() %></span> <input type="radio" class="h" name="reservationType" /></td>
											<td><span><%=pvo.getStay_s_w_v() %></span> <input type="radio" class="w" name="reservationType" /></td>
											<td><span><%=pvo.getStay_s_h_v() %></span> <input type="radio" class="h" name="reservationType" /></td>
										</tr>
										<%
										}
										%>
									</table>
									<input type="button" class="btnPink" style="float:right;margin-top:5px"id="priceAddBtn" value="요금등록" />
								</div>
							</td>
						</tr>
						<tr>
							<th>휴일</th>
							<td><%=vo.getStay_holiday_v() %></td> 
						</tr>
						<tr>
							<th>종류</th>
							<td><span class="stay_type_v"><%=vo.getStay_type_v()%></span></td>
						</tr>
						<tr>
							<th>지역</th>
							<td><%=vo.getStay_loc_v() %></td>
						</tr>
						<tr>
							<th>등록일</th>
							<td><%=vo.getStay_reg_d()%></td>
						</tr>
						<tr>
							<th>이미지1</th>
							<td><img class="imgPath_1v" src="<%=vo.getStay_imgpath_1_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지2</th>
							<td><img src="<%=vo.getStay_imgpath_2_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지3</th>
							<td><img src="<%=vo.getStay_imgpath_3_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지4</th>
							<td><img src="<%=vo.getStay_imgpath_4_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지5</th>
							<td><img src="<%=vo.getStay_imgpath_5_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지6</th>
							<td><img src="<%=vo.getStay_imgpath_6_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지7</th>
							<td><img src="<%=vo.getStay_imgpath_7_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
						<tr>
							<th>이미지8</th>
							<td><img src="<%=vo.getStay_imgpath_8_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
						</tr>
					</tbody>
				</table>
				<%
						}
				%>
				<div class="btnArea">
					<a href="#" class="btnPink" id="deleteBtn">삭제</a>
					<a href="#" class="btnPink" id="modifyBtn">수정</a>
					<a href="#" class="btnPink" id="listBtn">목록</a>
				</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>