<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.StayDAO, dao.StayVO, dao.StayPriceDAO, dao.StayPriceVO" %>
<%
	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"),"0"));
	String stayPath = "file:///C:/eclipse/web_workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/project_datesystem/upload/stay/";

%>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(function(){
	$("#listBtn").on("click", function(){
		$("#moveForm").submit();
	});
	
	$("#deleteBtn").on("click", function(){
		if(confirm("삭제하시겠습니까?")){
			$("#frm").attr("action", "stay_deleteOk.jsp").submit();
		}
	});
	
	$("#priceAddBtn").on("click", function(){
		$("#frm").attr("action", "stay_price_insert.jsp").submit();
	});
	
	$("#modifyBtn").on("click", function(){
		if(confirm("수정하시겠습니까?")){
			$("#frm").attr("action", "stay_modifyOk.jsp").submit();
		}
	});
	
});
</script>
<style type="text/css">
img {
	width:300px;
	height:200px;
}

.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.btnArea {
	clear:both;
	margin-top:10px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<form action="stay_view.jsp" id="moveForm">
			<input type="hidden" name="stay_no_n" value="<%=stay_no_n %>" />
		</form>
		<div class="view">
			<h2>관리자페이지</h2>
			<%
				StayDAO dao = new StayDAO();
				StayVO vo = dao.selectOne(stay_no_n);
				
				if(vo != null){
			%>
			<form id="frm" enctype="multipart/form-data" method="post">
				<input type="hidden" name="stay_no_n" id="stay_no_n" value="<%=stay_no_n %>" />
				<table>
					<tbody>
						<tr>
							<th><label for="stay_title_v">제목</label></th>
							<td><input type="text" name="stay_title_v" id="stay_title_v" value="<%=vo.getStay_title_v() %>" /></td>
						</tr>
						<tr>
							<th><label for="stay_info_title_v">부제목</label></th>
							<td><input type="text" name="stay_info_title_v" id="stay_info_title_v" value="<%=vo.getStay_info_title_v() %>" /></td>
						</tr>
						<tr>
							<th><label for="stay_content_v">내용</label></th>
							<td><textarea name="stay_content_v" id="stay_content_v" cols="70" rows="10"><%=vo.getStay_content_v() %></textarea></td>
						</tr>
						<tr>
							<th><label for="stay_tel_v">전화번호</label></th>
							<td><input type="text" name="stay_tel_v" id="stay_tel_v" value="<%=vo.getStay_tel_v() %>" /></td>
						</tr>
						<tr>
							<th><label for="stay_open_time_v">OPEN TIME</label></th>
							<td><input type="text" name="stay_open_time_v" id="stay_open_time_v" value="<%=vo.getStay_open_time_v() %>" /></td>
						</tr>
						<tr>
							<th><label for="stay_close_time_v">CLOSE TIME</label></th>
							<td><input type="text" name="stay_close_time_v" id="stay_close_time_v" value="<%=vo.getStay_close_time_v() %>" /></td>
						</tr>
						<tr>
							<th>가격</th>
							<td>
								<div>
									<table>
										<tr>
											<th rowspan="2">구분</th>
											<th colspan="2">대실</th>
											<th colspan="2">숙박</th>
										</tr>
										<tr>
											<td>월~금</td>
											<td>토~일</td>
											<td>월~금</td>
											<td>토~일</td>
										</tr>
										<%
										
										StayPriceDAO p_dao = new StayPriceDAO();
										ArrayList<StayPriceVO> p_list = p_dao.selectAll(stay_no_n);
										
										for(StayPriceVO pvo : p_list){
										%>
										<tr>
											<th><a href="stay_price_modify.jsp?stay_no_n=<%=stay_no_n%>&stay_price_no_n=<%=pvo.getStay_price_no_n() %>"><%=pvo.getStay_price_nm_v() %></a></th>
											<td><%=pvo.getStay_d_w_v() %></td>
											<td><%=pvo.getStay_d_h_v() %></td>
											<td><%=pvo.getStay_s_w_v() %></td>
											<td><%=pvo.getStay_s_h_v() %></td>
										</tr>
										<%
										}
										%>
									</table>
									<input type="button" value="요금등록" id="priceAddBtn"/>
								</div>
							</td>
						</tr>
						<tr>
							<th><label for="stay_holiday_v">휴일</label></th>
							<td><input type="text" name="stay_holiday_v" id="stay_holiday_v" value="<%=vo.getStay_holiday_v() %>" /></td> 
						</tr>
						<tr>
							<th><label for="stay_type_v">종류</label></th>
							<td>
								<select id="stay_type_v" name="stay_type_v">
									<option value="M" <%=vo.getStay_type_v().equals("M")?"'selected=selected'":"" %>>모텔
									<option value="H" <%=vo.getStay_type_v().equals("H")?"'selected=selected'":"" %>>호텔
									<option value="G" <%=vo.getStay_type_v().equals("G")?"'selected=selected'":"" %>>게스트하우스
								</select>	
							</td>
						</tr>
						<tr>
							<th><label for="stay_loc_v">지역</label></th>
							<td><input type="text" name="stay_loc_v" id="stay_loc_v" value="<%=vo.getStay_loc_v() %>" /></td>
						</tr>
						<tr>
							<th>등록일</th>
							<td><%=vo.getStay_reg_d()%></td>
						</tr>
						<tr>
							<th>이미지1</th>
							<td>
								<img src="<%=vo.getStay_imgpath_1_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt=""/><br/>
								<input type="file" name="stay_imgpath_1_v" id="stay_imgpath_1_v" />
							</td>
						</tr>
						<tr>
							<th>이미지2</th>
							<td>
								<img src="<%=vo.getStay_imgpath_2_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="stay_imgpath_2_v" id="stay_imgpath_2_v" />
							</td>
						</tr>
						<tr>
							<th>이미지3</th>
							<td>
								<img src="<%=vo.getStay_imgpath_3_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="stay_imgpath_3_v" id="stay_imgpath_3_v" />
							</td>
						</tr>
						<tr>
							<th>이미지4</th>
							<td>
								<img src="<%=vo.getStay_imgpath_4_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="stay_imgpath_4_v" id="stay_imgpath_4_v" />
							</td>
						</tr>
						<tr>
							<th>이미지5</th>
							<td>
								<img src="<%=vo.getStay_imgpath_5_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="stay_imgpath_5_v" id="stay_imgpath_5_v" />
							</td>
						</tr>
						<tr>
							<th>이미지6</th>
							<td>
								<img src="<%=vo.getStay_imgpath_6_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="stay_imgpath_6_v" id="stay_imgpath_6_v" />
							</td>
						</tr>
						<tr>
							<th>이미지7</th>
							<td>
								<img src="<%=vo.getStay_imgpath_7_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="stay_imgpath_7_v" id="stay_imgpath_7_v" />
							</td>
						</tr>
						<tr>
							<th>이미지8</th>
							<td>
								<img src="<%=vo.getStay_imgpath_8_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="stay_imgpath_8_v" id="stay_imgpath_8_v" />
							</td>
						</tr>
					</tbody>
				</table>
				<%
						}
				%>
				<div class="btnArea">
					<!-- <a href="#" class="btn"  id="deleteBtn">삭제</a> -->
					<a href="#" class="btnPink" id="modifyBtn">수정하기</a>
					<a href="#" class="btnPink" id="listBtn">상위페이지</a>
				</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>