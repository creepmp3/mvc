<%@page import="dao.TourVO"%>
<%@page import="dao.TourDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%
	int tour_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("tour_no_n"),"0"));
	String tourPath = "file:///C:/eclipse/web_workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/project_datesystem/upload/tour/";

%>
<html>
<head>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(function(){
	$("#listBtn").on("click", function(){
		location.href = "tour_list.jsp";
	});
	
	$("#deleteBtn").on("click", function(){
		if(confirm("삭제하시겠습니까?")){
			$("#frm").attr("action", "tour_deleteOk.jsp").submit();
		}
	});
	
	$("#priceAddBtn").on("click", function(){
		$("#frm").attr("action", "tour_price_insert.jsp").submit();
	});
	
	$("#modifyBtn").on("click", function(){
		$("#frm").attr("action", "tour_modify.jsp").submit();
	});
	
});
</script>
<style type="text/css">
img{
	width:400px;
	height:300px;
}
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<div class="view">
			<%
				TourDAO dao = new TourDAO();
				TourVO vo = dao.selectOne(tour_no_n);
				
				if(vo != null){
			%>
			<form id="frm">
				<input type="hidden" name="tour_no_n" id="tour_no_n" value="<%=tour_no_n %>" />
				<table class="tableView">
					<colgroup>
						<col width="25%"/>
						<col width="75%"/>
					</colgroup>
					<tr>
						<th>번호</th>
						<td><%=vo.getTour_no_n() %></td>
					</tr>
					<tr>
						<th>이름</th>
						<td><%=vo.getTour_title_v() %></td>
					</tr>
					<tr>
						<th>내용</th>
						<td><%=vo.getTour_content_v() %></td>
					</tr>
					<tr>
						<th>전화번호</th>
						<td><%=vo.getTour_tel_v() %></td>
					</tr>
					<tr>
						<th>OPEN TIME</th>
						<td><%=vo.getTour_open_time_v() %></td>
					</tr>
					<tr>
						<th>CLOSE TIME</th>
						<td><%=vo.getTour_close_time_v() %></td>
					</tr>
					<tr>
						<th>가격</th>
						<td><%=vo.getTour_price_v() %></td>
					</tr>
					<tr>
						<th>휴일</th>
						<td><%=vo.getTour_holiday_v() %></td> 
					</tr>
					<tr>
						<th>종류</th>
						<td><%=vo.getTour_type_v()%></td>
					</tr>
					<tr>
						<th>지역</th>
						<td><%=vo.getTour_loc_v() %></td>
					</tr>
					<tr>
						<th>등록일</th>
						<td><%=vo.getTour_reg_d()%></td>
					</tr>
					<tr>
						<th>이미지1</th>
						<td><img src="<%=vo.getTour_imgpath_1_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
					</tr>
					<tr>
						<th>이미지2</th>
						<td><img src="<%=vo.getTour_imgpath_2_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
					</tr>
					<tr>
						<th>이미지3</th>
						<td><img src="<%=vo.getTour_imgpath_3_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
					</tr>
					<tr>
						<th>이미지4</th>
						<td><img src="<%=vo.getTour_imgpath_4_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
					</tr>
					<tr>
						<th>이미지5</th>
						<td><img src="<%=vo.getTour_imgpath_5_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
					</tr>
					<tr>
						<th>이미지6</th>
						<td><img src="<%=vo.getTour_imgpath_6_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
					</tr>
					<tr>
						<th>이미지7</th>
						<td><img src="<%=vo.getTour_imgpath_7_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
					</tr>
					<tr>
						<th>이미지8</th>
						<td><img src="<%=vo.getTour_imgpath_8_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></td>
					</tr>
				</table>
				<%
						}
				%>
				<div id="btnArea">
					<input type="button" class="btnPink" id="deleteBtn" value="삭제"/>
					<input type="button" class="btnPink" id="modifyBtn" value="수정"/>
					<input type="button" class="btnPink" id="listBtn" value="목록"/>
				</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>