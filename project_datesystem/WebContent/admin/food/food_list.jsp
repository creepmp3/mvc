<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp"%>
<%@ page import="dao.FoodDAO, dao.FoodVO"%>
<%
	/* ******************** paging ******************** */
	int totalcount = 0;
	int pageCount = 0;
	int pageNo = Integer.parseInt(StringUtil.nvl(request.getParameter("pageNo"), "1"));
	int pageSize = 10;
	int startNum = ((pageNo - 1) * pageSize) + 1;
	int endNum = ((pageNo - 1) * pageSize) + pageSize;
	
	int searchStart = 0;
	int searchEnd = 0;
	searchStart = (pageNo-5 <= 0)?1:pageNo-5;
	searchEnd = (pageNo-5 <=0)?10:pageNo+5;

	String searchOption = StringUtil.nvl(request.getParameter("searchOption"), "");
	String searchKeyword = StringUtil.nvl(request.getParameter("searchKeyword"), "");
	String orderType = StringUtil.nvl(request.getParameter("orderType"), "");
	/* ******************** paging ******************** */
	
	FoodDAO dao = new FoodDAO();
	ArrayList<FoodVO> list = dao.selectAll(startNum, endNum, searchOption, searchKeyword, orderType);
%>

<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
	$(function() {
		$("#writeBtn").on("click", function() {
			location.href = "food_insert.jsp";
		});

		$("a[name='titlev']").each(function(i, elem) {
			$(this).on("click", function() {
				var no = $(this).parent().prev().text();
				$("#food_no_n").val(no);
				$("#moveForm").attr("action", "food_view.jsp").submit();
			});
		});

		$("#reservationBtn").on("click", function() {
			location.href = "./reservation/reservation_insert.jsp";
		});

	});
</script>
<style type="text/css">
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<div class="list">

			<form id="moveForm">
				<input type="hidden" name="food_no_n" id="food_no_n" />
			</form>
			<div class="searchWrap">
				<form action="<%=PAGENAME%>">
					<select name="searchOption">
						<option value="titlev" <%=searchOption.equals("titlev") ? "selected='selected'": ""%>>이름</option>
						<option value="loc"	<%=searchOption.equals("loc") ? "selected='selected'" : ""%>>지역</option>
						<option value="type" <%=searchOption.equals("type") ? "selected='selected'" : ""%>>타입</option>
						<option value="holiday"	<%=searchOption.equals("holiday") ? "selected='selected'": ""%>>휴일</option>
						<option value="tel"	<%=searchOption.equals("tel") ? "selected='selected'" : ""%>>전화번호</option>
					</select> <input type="text" name="searchKeyword"
						value="<%=searchKeyword%>" /> <input type="submit" value="검색" />
				</form>
			</div>
			<table>
				<thead>
					<tr>
						<th><input type="checkbox" id="chkAll" /></th>
						<th>번호</th>
						<th>이름</th>
						<th>지역</th>
						<th>타입</th>
						<th>등록일</th>
					</tr>
				</thead>
				<tbody>
					<%
						if (list.size() > 0) {
							for (FoodVO vo : list) {
								totalcount = vo.getTotalcount();

								if (totalcount % pageSize == 0) {
									pageCount = totalcount / pageSize;
								} else {
									pageCount = totalcount / pageSize + 1;
								}

								String type = "";
								if (vo.getFood_type_v().equals("K")) {
									type = "한식";
								} else if (vo.getFood_type_v().equals("C")) {
									type = "중식";
								} else if (vo.getFood_type_v().equals("J")) {
									type = "일식";
								}
					%>
					<tr>
						<td><input type="checkbox" name="chk" /></td>
						<td><%=vo.getFood_no_n()%></td>
						<td><a href="#" name="titlev"><%=vo.getFood_title_v()%></a></td>
						<td><%=vo.getFood_loc_v()%></td>
						<td><%=type%></td>
						<td><%=vo.getFood_reg_d()%></td>
					</tr>
					<%
							}
						} else {
					%>
					<tr>
						<td colspan="8">데이터가 없습니다</td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
			<div class="paging">
				<div class="pagenum">
					<%
						for (int i = searchStart; i <= pageCount; i++) {
							if (i < 0) {
								continue;
							} else if (i > searchEnd || totalcount < 10) {
								break;
							} else if (i == pageNo) {
								out.println("<strong>" + i + "</strong>");
							} else {
								out.println("<a class='pagingBtn' href='food_list.jsp?pageNo="
										+ i
										+ "&searchKeyword="
										+ searchKeyword
										+ " '>"
										+ i
										+ "</a>");
							}
						}
						out.println("<span>(" + pageNo + "/" + pageCount + " 총"	+ totalcount + "개)</span>");
					%>
				</div>
				<div class="btnArea">
					<a href="#" class="btnPink" id="deleteBtn">삭제</a> 
					<!-- <a href="#"	class="btn" id="reservationBtn">예약하기</a>  -->
					<a href="#" class="btnPink"	id="writeBtn">등록하기</a>
				</div>
			</div>

		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp"%>