<%@page import="java.io.File"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		//실제경로는 WebContent부터 시작
		String uploadPath = request.getRealPath("/uploadImg");
		int maxSize = 1024 * 1024 * 10; // 10MB제한
		
		//multipartRequest객체를 생성하여
		//request,경로,사이즈,encoding,DefaultFileRenamePolicy를 넘겨준다.
		MultipartRequest mr = new MultipartRequest(request, uploadPath, maxSize, "UTF-8", new DefaultFileRenamePolicy());
		
		String f_name = mr.getParameter("f_name");
		//파일명은 MultipartRequest아래 getOriginalFileName으로 받아온다.
		String f_fileImg = mr.getOriginalFileName("f_fileImg");
		
		//실제 경로를 찾아
		File f= new File(uploadPath);
		//디렉토리가 있으면
		if(f.isDirectory()){
			//배열에 요소를 꺼내
			String[] flist = f.list();
			//향상된for문을 이용하여 String변수 x에 담고
			for(String x : flist){
				//모든 이미지를 출력
				out.println("<img src='../../uploadImg/"+x+ "'/> ");
			}
		}
	%>
</body>
</html>