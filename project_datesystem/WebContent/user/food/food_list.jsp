<%@page import="dao.FoodVO"%>
<%@page import="dao.FoodDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="util.StringUtil"%>
<%@ include file="../../common/sub_header.jsp"%>
<%
	/* ******************** paging ******************** */
	int totalcount = 0;
	int pageCount = 0;
	int pageNo = Integer.parseInt(StringUtil.nvl(
	request.getParameter("pageNo"), "1"));
	int pageSize = 10;
	int startNum = ((pageNo - 1) * pageSize) + 1;
	int endNum = ((pageNo - 1) * pageSize) + pageSize;

	int searchStart = 0;
	int searchEnd = 0;
	searchStart = (pageNo - 5 <= 0) ? 1 : pageNo - 5;
	searchEnd = (pageNo - 5 <= 0) ? 10 : pageNo + 5;

	String searchOption = StringUtil.nvl(request.getParameter("searchOption"), "");
	String searchKeyword = StringUtil.nvl(request.getParameter("searchKeyword"), "");
	String orderType = StringUtil.nvl(request.getParameter("orderType"), "");
	/* ******************** paging ******************** */
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript"
	src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="../../css/content_css/main_contents_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/menu_content.css" />
<link rel="stylesheet" href="../../css/content_css/tour_content_style.css" />
<script type="text/javascript" src="../../js/main/weather.js"></script>
<link rel="stylesheet" href="../../css/main/weather.css" />
<link rel="stylesheet" href="../css/content_css/tour_content_style.css" />

<script type="text/javascript">
	$(document).ready(function() {
		$("#option_content_local").hide();
		$("#option_content_tema").hide();
		$("#local").on('click', function() {
			$("#option_content_local").toggle();
		});
		$("#tema").on('click', function() {
			$("#option_content_tema").toggle();
		});

	});
</script>
</head>
<body>
	<div id="main">
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		<div id="btn">
			<input type="button" name="" id="local" value="지역" /> <input
				type="button" name="" id="tema" value="태마" />
		</div>

		<div id="option_content_local">
			<div id="option_text">지역 세부사항</div>
			<div id="option_check">
				<ul>
					<li><label><input type="checkbox" name=""
							id="local_ck" />전체</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>

				</ul>
			</div>
		</div>
		<div id="option_content_tema">
			<div id="option_text">태마 세부사항</div>
			<div id="option_check">
				<ul>

					<li><label><input type="checkbox" name=""
							id="local_ck" />전체</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>
					<li><label><input type="checkbox" name=""
							id="local_ck" />강남/서초</label></li>


				</ul>
			</div>

		</div>

		<div id="list_search">
			<div id="tour_content_top">추천 맛집</div>
			<div id="tour_content_down">
				<ul>
					<li><a href="<%=PAGENAME %>?orderType=date">신규</a></li>
					<li><a href="<%=PAGENAME %>?orderType=recommend">추천</a></li>
					<li>인기</li>
				</ul>
			
			<form action="<%=PAGENAME%>">
				<input type="submit" id="search_btn" value="검색" /> <input
					type="text" name="searchKeyword" id="search_text"
					value="<%=searchKeyword%>" /> <select name="searchOption"
					class="searchOption">
					<option value="titlev"
						<%=searchOption.equals("titlev") ? "selected='selected'"
					: ""%>>이름</option>
					<option value="loc"
						<%=searchOption.equals("loc") ? "selected='selected'" : ""%>>지역</option>
					<option value="type"
						<%=searchOption.equals("type") ? "selected='selected'" : ""%>>타입</option>
					<option value="holiday"
						<%=searchOption.equals("holiday") ? "selected='selected'"
					: ""%>>휴일</option>
					<option value="tel"
						<%=searchOption.equals("tel") ? "selected='selected'" : ""%>>전화번호</option>
				</select>
			</form>
		</div>
</div>

		<form id="cook">
			<div class="food1">
				<ul>
					<%
						FoodDAO dao = new FoodDAO();
						ArrayList<FoodVO> list = dao.selectAll(startNum, endNum,
								searchOption, searchKeyword, orderType);

						if (list.size() > 0) {
							for (FoodVO vo : list) {
								totalcount = vo.getTotalcount();

								if (totalcount % pageSize == 0) {
									pageCount = totalcount / pageSize;
								} else {
									pageCount = totalcount / pageSize + 1;
								}

								String type = "";
								if (vo.getFood_type_v().equals("H")) {
									type = "호텔";
								} else if (vo.getFood_type_v().equals("M")) {
									type = "모텔";
								} else if (vo.getFood_type_v().equals("G")) {
									type = "게스트하우스";
								}
					%>
					<li>
						<dl>
							<dt>
								<a href="food_view.jsp?food_no_n=<%=vo.getFood_no_n()%>"><img
									src="<%=vo.getFood_imgpath_1_v()%>"></a>
							</dt>
							<dd class="title"><%=vo.getFood_title_v()%></dd>
							<dd><%=vo.getFood_loc_v()%></dd>

						</dl>
					</li>
					<%
						}
						} else {
					%>
					<li>데이터가 없습니다</li>
					<%
						}
					%>
				</ul>
			</div>
		</form>
	</div>
</body>
</html>