<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.BoardManDAO, dao.BoardManVO, util.StringUtil"%>
<%
	int seq_n = Integer.parseInt(StringUtil.nvl(request.getParameter("seq_n"), "0"));
	BoardManDAO dao = new BoardManDAO();
	int result = dao.deleteOne(seq_n);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
var result = "<%=result%>";
var msg = "삭제가 실패하였습니다";
if(result>0){
	msg = "삭제되셨습니다";
}
alert(msg);
location.href = "./boardM_list.jsp";
</script>
<title></title>
</head>
<body>

</body>
</html>