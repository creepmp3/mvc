<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.BoardWorryDAO, dao.BoardWorryVO, util.StringUtil"%>
<%
	int seq_n = Integer.parseInt(StringUtil.nvl(request.getParameter("seq_n"), "0"));

	BoardWorryDAO dao = new BoardWorryDAO();
	BoardWorryVO vo = dao.selectOne(seq_n);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/community_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/main_contents_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/main/weather.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="/project_datesystem/js/se2/js/HuskyEZCreator.js" charset="utf-8"></script>
<script type="text/javascript" src="./quick_photo_uploader/plugin/hp_SE2M_AttachQuickPhoto.js" charset="utf-8"> </script>
<script type="text/javascript" src="/project_datesystem/js/main/movie_js_style.js" /></script>

<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<title>Insert title here</title>
<script type="text/javascript">
$(function(){

	var oEditors = [];
	nhn.husky.EZCreator.createInIFrame({
	    oAppRef: oEditors,
	    elPlaceHolder: "content_v",
	    sSkinURI: "/project_datesystem/js/se2/SmartEditor2Skin.html",
	    fCreator: "createSEditor2"
	});
	
	$("#listBtn").on("click", function(){
		location.href = "./boardWorry_list.jsp";
	});
	
	$("#deleteBtn").on("click", function(){
		if(confirm("삭제하시겠습니까?")){
			$("#updateForm").attr("action", "boardWorry_deleteOk.jsp").submit();
		}
		return false;
	});
	
	$("#modifyBtn").on("click", function(){
		if(confirm("수정하시겠습니까?")){
			 // 에디터의 내용이 textarea에 적용된다.
		    oEditors.getById["content_v"].exec("UPDATE_CONTENTS_FIELD", []);
		 
		    // 에디터의 내용에 대한 값 검증은 이곳에서
		    // document.getElementById("ir1").value를 이용해서 처리한다.
			$("#updateForm").attr("action", "boardWorry_updateOk.jsp").submit();
		}
		return false;
	});
	
});
</script>
<style type="text/css">
</style>
</head>
<body>
	<div id = "main">
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		<div id = "sub_menu">
			<div id = "profile">
				xx님 <br/>
				상태 상태 <br/>
				뭐야 뭐야 <br/>
			</div>
			<ul>
				<li>사이드 메뉴1</li>
				<li>사이드 메뉴2</li>
				<li>사이드 메뉴3</li>
				<li>사이드 메뉴4</li>
				<li>사이드 메뉴5</li>
				<li>사이드 메뉴6</li>
				<li>사이드 메뉴7</li>
			</ul>
		</div>
		<div id = "sign_area">광고 영역</div>
		<div id = "board_content">
			<div class="view">
				<div style="border:0px solid black;width:735px;height:655px;position:absolute;top:-227px">
				<form id="updateForm">
					<input type="hidden" name="seq_n" id="seq_n" value="<%=seq_n %>"/>
					<table style="border:1px solid black;width:735px;height:auto">
						<tbody>
							<tr>
								<th>제목</th>
								<td><input type="text" id="title_v" name="title_v" style="width:99%" value="<%=vo.getTitle_v() %>"/></td>
							</tr>
							<tr>
								<th>내용</th>
								<td><textarea name="content_v" id="content_v" cols="80" rows="15"><%=vo.getContent_v() %></textarea></td>
							</tr>
						</tbody>
					</table>
				</form>
				<div class="btnArea">
					<a href="#" class="btnBlue" id="deleteBtn">삭제</a>
					<a href="#" class="btnBlue" id="modifyBtn">수정</a>
					<a href="#" class="btnBlue" id="listBtn">목록</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>