<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.BoardWoManDAO, dao.BoardWoManVO"%>
<%
	/* ******************** paging ******************** */
	int totalcount = 0;
	int pageCount = 0;
	int pageNo = Integer.parseInt(StringUtil.nvl(request.getParameter("pageNo"), "1"));
	int pageSize = 10;
	int startNum = ((pageNo - 1) * pageSize) + 1;
	int endNum = ((pageNo - 1) * pageSize) + pageSize;
	
	int searchStart = 0;
	int searchEnd = 0;
	searchStart = (pageNo-5 <= 0)?1:pageNo-5;
	searchEnd = (pageNo-5 <=0)?10:pageNo+5;

	String searchOption = StringUtil.nvl(request.getParameter("searchOption"), "");
	String searchKeyword = StringUtil.nvl(request.getParameter("searchKeyword"), "");
	String orderType = StringUtil.nvl(request.getParameter("orderType"), "");
	/* ******************** paging ******************** */
	
	BoardWoManDAO dao = new BoardWoManDAO();
	ArrayList<BoardWoManVO> list = dao.selectAll(startNum, endNum, searchOption, searchKeyword, orderType);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/community_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/main_contents_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/main/weather.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="/project_datesystem/js/main/movie_js_style.js" /></script>

<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<title>Insert title here</title>
<script type="text/javascript">
$(function(){
	$("#insertBtn").on("click", function(){
		location.href = "./boardW_insert.jsp";
	});
});
</script>
<style type="text/css">
.ar {
	text-align:left;
}
</style>
</head>
<body>
	<div id = "main">
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		<div id = "sub_menu">
			<div id = "profile">
				<%=session_member_nick_v %>님 <br/>
				상태 상태 <br/>
				뭐야 뭐야 <br/>
			</div>
			<ul>
				<li>사이드 메뉴1</li>
				<li>사이드 메뉴2</li>
				<li>사이드 메뉴3</li>
				<li>사이드 메뉴4</li>
				<li>사이드 메뉴5</li>
				<li>사이드 메뉴6</li>
				<li>사이드 메뉴7</li>
			</ul>
		</div>
		<div id = "sign_area">광고 영역</div>
		<div id = "board_content">
			<div class="list">
				<form id="listForm">
					<table class="pink">
						<colgroup>
							<col width="10%"/>
							<col width="45%"/>
							<col width="20%"/>
							<col width="10%"/>
							<col width="15%"/>
						</colgroup>
						<thead>
							<tr>
								<th>번호</th>
								<th>제목</th>
								<th>글쓴이</th>
								<th>조회수</th>
								<th>등록일</th>
							</tr>
						</thead>
						<tbody>
							<%
							for(BoardWoManVO vo : list){
							    totalcount = vo.getTotalcount();
		
								if (totalcount % pageSize == 0) {
									pageCount = totalcount / pageSize;
								} else {
									pageCount = totalcount / pageSize + 1;
								}
							%>
							<tr>
								<td><%=vo.getSeq_n() %></td>
								<td class="titlevTd"><a href="boardW_detail.jsp?seq_n=<%=vo.getSeq_n()%>" >
								<%
								String reStr = "";
								String reSp = "";
								if(vo.getRef_n()>0){
								    reStr = "└";
								    for(int i=1; i<vo.getLv()-1; i++){
								        reStr+="-";
								        reSp+="　";
								    }
								    reStr+="> ";
								}
								%>
								<%=reSp%><%=reStr%><%=vo.getTitle_v() %></a></td>
								<td><%=vo.getMember_nick_v() %>(<%=vo.getMember_id_v() %>)</td>
								<td><%=vo.getRead_cnt_n() %></td>
								<td><%=vo.getReg_d() %></td>
							</tr>
							<%
							}
							%>
						</tbody>
					</table>
				</form>
				<div class="btnArea">
					<a href="#" class="btnPink" id="insertBtn">글쓰기</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>