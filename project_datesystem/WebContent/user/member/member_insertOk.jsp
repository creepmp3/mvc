<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.MemberDAO, dao.MemberVO, util.StringUtil" %>
<%
	String member_id_v = StringUtil.nvl(request.getParameter("member_id_v"), "");
	String member_pw_v = StringUtil.nvl(request.getParameter("member_pw_v"), "");
	String member_nick_v = StringUtil.nvl(request.getParameter("member_nick_v"), "");
	String member_nm_v = StringUtil.nvl(request.getParameter("member_nm_v"), "");
	String member_gender_v = StringUtil.nvl(request.getParameter("member_gender_v"), "");
	String member_bday_v = StringUtil.nvl(request.getParameter("member_bday_v"), "");
	String member_email_v = StringUtil.nvl(request.getParameter("member_email_v"), "");
	String member_tel_v = StringUtil.nvl(request.getParameter("member_tel_v"), "");
	String member_cell_v = StringUtil.nvl(request.getParameter("member_cell_v"), "");
	String member_addr_v = StringUtil.nvl(request.getParameter("member_addr_v"), "");
	String ip = request.getRemoteAddr();
	String member_type_v = StringUtil.nvl(request.getParameter("member_type_v"), "N");
	
	
	MemberDAO dao = new MemberDAO();
	MemberVO vo = new MemberVO(member_id_v, member_pw_v, member_nick_v, 
			member_nm_v, member_gender_v, member_bday_v, member_email_v, member_tel_v, 
			member_cell_v, member_addr_v, ip, member_type_v);
	int result = dao.insertOne(vo);
	if(result>0){
		session.setAttribute("member_id_v", member_id_v);
		session.setAttribute("member_pw_v", member_pw_v);
		session.setAttribute("member_nick_v", member_nick_v);
		session.setAttribute("member_type_v", member_type_v);
		session.setAttribute("member_gender_v", member_gender_v);
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title> 오후 4:59:32</title>
<script type="text/javascript">
var result = "<%=result%>";
if(result>0){
	alert("등록이 완료되었습니다");
}else{
	alert("등록에 실패하였습니다");
}
location.href = "../../contents/main_content.jsp";
</script>
</head>
<body>

</body>
</html>